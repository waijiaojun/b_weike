<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>[NativeTalk] New Appointment</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0;padding: 0">
<table cellpadding="0" cellspacing="0" width="600px" align="center">
    <tr>
        <td>
            <img src="http://file.waijiaojun.com/waijiaojun/app/image/email/logo.png">
            <div id="NA1"
                 style="background: #00BB9C; color: white; height: 60px; line-height: 60px; border-radius: 15px 0px 0px 0px">
                <p style="font-family: PingFang SC Medium; font-size: 24px; padding-left: 20px; margin: 0px; padding-top: 0px">
                    New Appointment
                </p>
            </div>
            <div id="NA2" style="background: #ECECEC; height: 18px">

            </div>
            <div id="NA3" style="font-family: PingFang SC Medium; margin-top: 10px; margin-bottom: 10px">
                <p style="font-family: PingFang SC Medium; font-size: 24px; padding-left: 20px">Dear ${tea_name}</p>
            </div>
            <div id="NA4" style="font-family: PingFang SC Medium; font-size: 16px; padding-left: 20px; line-height: 25px">
                <p>You have a new appointment, please check your appointment details under "Appointments" tab on your homepage. Please remember to start your class on time. Thank you!</p>
            </div>
            <div id="NA5" style="margin-top: 10px; margin-bottom: 10px">
                <table cellpadding="8"
                       style="font-family:PingFang SC Medium;font-size: 16px; margin: auto;background-color: #EFFFFC">
                    <tr>
                        <td align="center" width="10%" style="border-right: dashed; border-width: 1px">student name</td>
                        <td align="center" width="10%" style="border-right: dashed; border-width: 1px">course name</td>
                        <td align="center" width="10%" style="border-right: dashed; border-width: 1px">starting
                            time(UTC/GMT+8)
                        </td>
                        <td align="center">expected payment</td>
                    </tr>
                    <tr>
                        <td align="center" width="10%" style="border-right: dashed; border-width: 1px">
                            <B>${memb_name}</B></td>
                        <td align="center" width="10%" style="border-right: dashed; border-width: 1px">
                            <B>${course_name}</B></td>
                        <td align="center" width="10%" style="border-right: dashed; border-width: 1px">
                            <B>${start_time}</B></td>
                        <td align="center" width="10%"><B>$${money}</B></td>
                    </tr>
                </table>
            </div>
            <div id="NA6"
                 style="font-family: PingFang SC Medium; font-size: 16px; padding-left: 20px; line-height: 25px">
                <p>
                    Best Regards!
                    <br/>
                    Nativetalk Team
                </p>
            </div>
            <div id="NA7"
                 style="font-family: PingFang SC Medium; font-size: 10px;background: #ECECEC; padding-left: 5px; padding-right: 5px;padding-top: 1px; padding-bottom: 5px; line-height: 18px;margin-top: 10px">
                <p>
                    *This is an auto email reply, please do not write back. Contact us through our official email
                    accountor Skype if you have any question :)
                    <br/>
                    Skype: nativetalk@waijiaojun.com
                    &nbsp&nbsp&nbsp
                    Email: nativetalk@waijiaojun.com
                </p>
            </div>
        </td>
    </tr>
</table>
</body>
</html>