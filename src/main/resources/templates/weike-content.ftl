<!doctype html>
<#import "/spring.ftl" as spring />
<html>
<head>
    <title>外教君-微课</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link rel="stylesheet" type="text/css" href="<@spring.url '/static/weike-share/css/libs/CSSRESET.css'/>">
    <link rel="stylesheet" type="text/css" href="<@spring.url '/static/weike-share/css/libs/swiper.min.css'/>">
    <link rel="stylesheet" type="text/css" href="<@spring.url '/static/weike-share/css/common.css'/>">
    <link rel="stylesheet" type="text/css" href="<@spring.url '/static/weike-share/css/course_content.css?v=15'/>">
    <script src="<@spring.url '/static/weike-share/js/libs/jquery-2.1.3.min.js'/>"></script>
    <script src="<@spring.url '/static/weike-share/js/libs/swiper.jquery.min.js'/>"></script>
    <script>
        $(function(){

            var _top=$("#blur_2").data("top");
            $("#blur_2").css("top",_top);

            //音频播放暂停效果
            $(".play i").on("click",function(){
                var $play=$(this).parent(".play");
                var audio=$play.find("audio")[0]
                if($play.hasClass("active")){   //暂停
                    $play.removeClass("active");
                    audio.pause();
                }else{                          //播放
                    $(".play").find("audio").each(function(){
                        $(this)[0].pause();
                        $(this).parents(".play").removeClass("active");
                    });
                    $play.find("span").remove();
                    $play.addClass("active");
                    audio.currentTime = 0;
                    audio.play();
                }
            });

        });
    </script>
</head>
<body>
<input type="hidden" id="computer_download_url" value="${computer_download_url}"/>
<input type="hidden" id="iOS_download_url" value="${iOS_download_url}"/>
<input type="hidden" id="Android_download_url" value="${Android_download_url}"/>
<article class="wrapper">
    <div class="main">
        <div class="pbanner">
        <#list weikeContent.images as image>
            <#if image_index == 0 >
                <img style="max-height: 200px;" src="http://file.waijiaojun.com/${image.url}" alt="" />
                <div style="width:100%; height: 100%; max-height: 200px;position: absolute; left:0; top:0; -webkit-filter: blur(5px); filter: blur(5px);
                        background: url(http://file.waijiaojun.com/${image.url!''}) no-repeat center center; background-size: cover;"></div>
            </#if>
        </#list>

        </div>
        <!-- pbanner -->

        <!-- part -->
        <#list weikeContent.parts>
                <#items as part>
                <div class="part">
                    <h3><p>${part.title}</p></h3>
                    <ul class="ul_part">
                        <#list part.contents as content>
                            <#if content.type == 'audio'>
                                <li>
                                    <div><img src="${weikeContent.teacher_avatar_url}" alt="" /></div>
                                    <div class="play">
                                        <i class="voice">${content.durationFromat}<span class="point"></span></i>
                                        <audio src="http://file.waijiaojun.com/${content.content}"></audio>
                                    </div>
                                </li>
                            <#else>
                                <li>
                                    <div><img src="${weikeContent.teacher_avatar_url}" alt="" /></div>
                                    <div>
                                        <div class="cnt_wrapper">
                                            <div class="em">${content.content}</div>
                                        </div>
                                    </div>
                                </li>
                            </#if>
                        </#list>
                    </ul>
                    <#if part_index gte 1 >
                        <div class="blur"></div>
                    </#if>

                </div>
                </#items>
        </#list>

    </div>
</article>

<footer class="footer">
    <a href="javascript:;" onclick="phone_system()"> 立即收听 </a>
</footer>
<script>
    function phone_system(){
        var ua = navigator.userAgent.toLowerCase();
        if (/iphone|ipad|ipod/.test(ua)) {

            location.href=document.getElementById("iOS_download_url").value;iOS_download_url
        } else if (/android/.test(ua)) {

            window.location=document.getElementById("Android_download_url").value;
        } else {
            window.location.href=document.getElementById("iOS_download_url").value;
        }

    }
</script>
</body>
</html>
