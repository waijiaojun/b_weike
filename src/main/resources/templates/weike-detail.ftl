<!doctype html>
<#import "/spring.ftl" as spring />
<html>
<head>
    <title>外教君-微课</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link rel="stylesheet" type="text/css" href="<@spring.url '/static/weike-share/css/libs/CSSRESET.css'/> ">
    <link rel="stylesheet" type="text/css" href="<@spring.url '/static/weike-share/css/common.css'/>">
    <link rel="stylesheet" type="text/css" href="<@spring.url '/static/weike-share/css/course_detail.css'/>">
    <script src="<@spring.url '/static/weike-share/js/libs/jquery-2.1.3.min.js'/>"></script>
    <script>
        $(function(){

            //显示全部 内容
            $(".a_show").on("click",function(){
                $(".b_content").css({"height":"auto","overflow":"auto"});
                $(this).fadeOut();
            });

            //评分
            $(".p_mark").each(function(index, element) {
                var _index=parseInt($(this).attr("data-value"));
                $(this).find("span:lt("+(_index)+")").addClass("active");
            });


        });
    </script>
</head>
<body>
<input type="hidden" id="computer_download_url" value="${computer_download_url}"/>
<input type="hidden" id="iOS_download_url" value="${iOS_download_url}"/>
<input type="hidden" id="Android_download_url" value="${Android_download_url}"/>
<article class="wrapper">
    <div class="main">
        <div class="open">
            <div class="o_wrap">
                <div><img src="<@spring.url '/static/weike-share/img/icon_cface.png'/>" alt="" /></div>
                <div>
                    <h5>外教君青少儿英语</h5>
                    <p>承包你的外语和未来</p>
                </div>
                <div><a href="javascript:;" onclick="phone_system()">立即打开</a></div>
            </div>
        </div>
        <!-- open -->
    <#attempt>

        <div class="cbanner">
            <img style="max-height: 200px;" src="http://file.waijiaojun.com/${weikeDetail.cover_url}" alt="" />
            <p>${weikeDetail.title}</p>
        </div>
        <!-- cbanner -->

        <div class="tinfo">
            <div>
                <img src="${weikeDetail.teacher.teacher_avatar_url}" alt="" />
                <span></span>
            </div>

            <div data-value="3">
                <h5>Peny</h5>
                <p>${weikeDetail.duration}分钟    <span>|</span>${weikeDetail.viewer_count}人听过</p>
            </div>

            <div>
                <h5>${weikeDetail.score?string["0.#"]}</h5>
                综合评分
            </div>
        </div>
        <!-- tinfo -->

        <div class="brief">
            <h3 class="cm_h3">简介</h3>
            <div class="b_content">
            ${weikeDetail.intro}
            </div>
            <a href="javascript:;" class="a_show"><span>显示全部</span></a>
        </div>
        <!-- brief -->

        <div class="ctable">
            <h3 class="cm_h3">课表</h3>
            <ul class="ul_clist">
            <#list weikeDetail.parts as part>
                <li>
                    <a href="javascript:;">${part.title} </a>
                    <span>${part.durationFromat}</</span>
                </li>
            </#list>
            </ul>
        </div>
        <!-- ctable -->

        <div class="mark">
            <h3 class="cm_h3">学生评价</h3>

            <ul class="ul_mark">
            <#list comments.data as comment>
                <li>
                    <div class="div_mark">
                        <div><img src="${comment.memb_head_portrait}" alt="" /></div>
                        <div>
                            <h5>${comment.memb_name}</h5>
                            <p>${comment.com_time}</p>
                        </div>
                        <div data-value="3">
                            <p class="p_mark" data-value="5"><#list 1..comment.com_grade as i><span></span></#list></p>
                        </div>
                    </div>
                    <p></p>
                </li>
            </#list>
            </ul>
        </div>
        <!-- mark -->
    </div>

        <#recover>
    <h1 style="margin: 100px auto;text-align: center">没有内容！</h1>
    </#attempt>
</article>

<footer class="footer">
<#attempt>
    <a href="<@spring.url '/html/share/content/${weikeDetail.id}'/>"><span>试听</span></a>
<#recover>
    <a href="javascript:;"><span>试听</span></a>
</#attempt>

    <a href="javascript:;" onclick="phone_system()"> 立即收听 </a>
</footer>
<script>
    function phone_system(){
        var ua = navigator.userAgent.toLowerCase();
        if (/iphone|ipad|ipod/.test(ua)) {

            location.href=document.getElementById("iOS_download_url").value;iOS_download_url
        } else if (/android/.test(ua)) {

            window.location=document.getElementById("Android_download_url").value;
        } else {
            window.location.href=document.getElementById("iOS_download_url").value;
        }

    }
</script>
</body>
</html>
