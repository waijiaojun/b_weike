package com.nativetalk.bean.member;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * TdNtMember entity. @author MyEclipse Persistence Tools
 */

public class TdNtMember implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	private Integer memb_id;
	private String memb_phone_area;
	private String memb_phone;
	private String memb_name;
	private String memb_password;
	private Timestamp memb_register_time;
	private String memb_register_month;
	private String memb_islock;
	private BigDecimal memb_balance;
	private String memb_head_portrait;
	private String memb_sex;
	private Timestamp memb_birthday;
	private String memb_synopsis;
	private Integer memb_ages;
	private String memb_invite_code;
	private String token;
	private String equ_type;//1android 2ios
	private String equ_client_id;//设备唯一标示  Android:clientId ios:devicetoken
	private String video_switch;//视频教学开关0视频1语音默认语音
	private BigDecimal memb_purchase_balance;//用户钻石余额

	public BigDecimal getMemb_purchase_balance() {
		return memb_purchase_balance;
	}

	public void setMemb_purchase_balance(BigDecimal memb_purchase_balance) {
		this.memb_purchase_balance = memb_purchase_balance;
	}
	public String getVideo_switch() {
		return video_switch;
	}

	public void setVideo_switch(String video_switch) {
		this.video_switch = video_switch;
	}

	public void setEqu_type(String equ_type) {
		this.equ_type = equ_type;
	}

	public void setEqu_client_id(String equ_client_id) {
		this.equ_client_id = equ_client_id;
	}

	public String getEqu_type() {
		return equ_type;
	}

	public String getEqu_client_id() {
		return equ_client_id;
	}
	public String getToken(){ return  token;}
	public void  setToken(String token){ this.token = token;}
	public Integer getMemb_id() {
		return memb_id;
	}
	public void setMemb_id(Integer memb_id) {
		this.memb_id = memb_id;
	}
	public String getMemb_phone_area() {
		return memb_phone_area;
	}
	public void setMemb_phone_area(String memb_phone_area) {
		this.memb_phone_area = memb_phone_area;
	}
	public String getMemb_phone() {
		return memb_phone;
	}
	public void setMemb_phone(String memb_phone) {
		this.memb_phone = memb_phone;
	}
	public String getMemb_name() {
		return memb_name;
	}
	public void setMemb_name(String memb_name) {
		this.memb_name = memb_name;
	}
	public String getMemb_password() {
		return memb_password;
	}
	public void setMemb_password(String memb_password) {
		this.memb_password = memb_password;
	}
	public Timestamp getMemb_register_time() {
		return memb_register_time;
	}
	public void setMemb_register_time(Timestamp memb_register_time) {
		this.memb_register_time = memb_register_time;
	}
	public String getMemb_register_month() {
		return memb_register_month;
	}
	public void setMemb_register_month(String memb_register_month) {
		this.memb_register_month = memb_register_month;
	}
	public String getMemb_islock() {
		return memb_islock;
	}
	public void setMemb_islock(String memb_islock) {
		this.memb_islock = memb_islock;
	}
	public BigDecimal getMemb_balance() {
		return memb_balance;
	}
	public void setMemb_balance(BigDecimal memb_balance) {
		this.memb_balance = memb_balance;
	}
	public String getMemb_head_portrait() {
		return memb_head_portrait;
	}
	public void setMemb_head_portrait(String memb_head_portrait) {
		this.memb_head_portrait = memb_head_portrait;
	}
	public String getMemb_sex() {
		return memb_sex;
	}
	public void setMemb_sex(String memb_sex) {
		this.memb_sex = memb_sex;
	}
	public Timestamp getMemb_birthday() {
		return memb_birthday;
	}
	public void setMemb_birthday(Timestamp memb_birthday) {
		this.memb_birthday = memb_birthday;
	}
	public String getMemb_synopsis() {
		return memb_synopsis;
	}
	public void setMemb_synopsis(String memb_synopsis) {
		this.memb_synopsis = memb_synopsis;
	}
	public Integer getMemb_ages() {
		return memb_ages;
	}
	public void setMemb_ages(Integer memb_ages) {
		this.memb_ages = memb_ages;
	}
	public String getMemb_invite_code() {
		return memb_invite_code;
	}
	public void setMemb_invite_code(String memb_invite_code) {
		this.memb_invite_code = memb_invite_code;
	}

	@Override
	public String toString() {
		return "TdNtMember{" +
				"memb_id=" + memb_id +
				", memb_phone_area='" + memb_phone_area + '\'' +
				", memb_phone='" + memb_phone + '\'' +
				", memb_name='" + memb_name + '\'' +
				", memb_password='" + memb_password + '\'' +
				", memb_register_time=" + memb_register_time +
				", memb_register_month='" + memb_register_month + '\'' +
				", memb_islock='" + memb_islock + '\'' +
				", memb_balance=" + memb_balance +
				", memb_head_portrait='" + memb_head_portrait + '\'' +
				", memb_sex='" + memb_sex + '\'' +
				", memb_birthday=" + memb_birthday +
				", memb_synopsis='" + memb_synopsis + '\'' +
				", memb_ages=" + memb_ages +
				", memb_invite_code='" + memb_invite_code + '\'' +
				", token='" + token + '\'' +
				", equ_type='" + equ_type + '\'' +
				", equ_client_id='" + equ_client_id + '\'' +
				", video_switch='" + video_switch + '\'' +
				", memb_purchase_balance=" + memb_purchase_balance +
				'}';
	}
}