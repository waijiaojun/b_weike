package com.nativetalk.bean.member;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * TdNtMembFoundChange entity. @author MyEclipse Persistence Tools
 * Modified by wangzheng on 17/03/04
 */

public class TdNtMembFoundChange implements java.io.Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	private Integer mfchg_id;
	private Integer memb_id;
	private BigDecimal mfchg_change_amount;
	private BigDecimal mfchg_front_amount;
	private BigDecimal mfchg_back_amount;
	private String mfchg_change_type;//充值（进入余额：+）:1；消费（余额支出：-）:2
	private Timestamp mfchg_change_time;
	private String mfchg_change_month;

	private Integer type;//类别 0充值 1购买课程 2取消预约 3呼叫老师(闲聊) 4其它(优惠码、周排行奖励、直播打赏(安卓才有) 系统调整) 5购买公开课(安卓才有) 6购买微课
	private String mfchg_change_name;//类型的名称(对应type：购买课程、取消预约)

	private String state;//1转账中 2已付款 3付款失败
	private String mfchg_number;//编号(orderNo) 支付用
	private Integer money_change_type=0;//0余额变动 1支付宝支付 2微信支付  (历史记录中只显示“余额变动”)

	public Integer getMoney_change_type() {
		return money_change_type;
	}

	public void setMoney_change_type(Integer money_change_type) {
		this.money_change_type = money_change_type;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public void setMfchg_number(String mfchg_number) {
		this.mfchg_number = mfchg_number;
	}

	public String getMfchg_number() {
		return mfchg_number;
	}
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	public Integer getMfchg_id() {
		return mfchg_id;
	}
	public void setMfchg_id(Integer mfchg_id) {
		this.mfchg_id = mfchg_id;
	}
	public Integer getMemb_id() {
		return memb_id;
	}
	public void setMemb_id(Integer memb_id) {
		this.memb_id = memb_id;
	}
	public BigDecimal getMfchg_change_amount() {
		return mfchg_change_amount;
	}
	public void setMfchg_change_amount(BigDecimal mfchg_change_amount) {
		this.mfchg_change_amount = mfchg_change_amount;
	}
	public BigDecimal getMfchg_front_amount() {
		return mfchg_front_amount;
	}
	public void setMfchg_front_amount(BigDecimal mfchg_front_amount) {
		this.mfchg_front_amount = mfchg_front_amount;
	}
	public BigDecimal getMfchg_back_amount() {
		return mfchg_back_amount;
	}
	public void setMfchg_back_amount(BigDecimal mfchg_back_amount) {
		this.mfchg_back_amount = mfchg_back_amount;
	}
	public String getMfchg_change_type() {
		return mfchg_change_type;
	}
	public void setMfchg_change_type(String mfchg_change_type) {
		this.mfchg_change_type = mfchg_change_type;
	}
	public Timestamp getMfchg_change_time() {
		return mfchg_change_time;
	}
	public void setMfchg_change_time(Timestamp mfchg_change_time) {
		this.mfchg_change_time = mfchg_change_time;
	}
	public String getMfchg_change_month() {
		return mfchg_change_month;
	}
	public void setMfchg_change_month(String mfchg_change_month) {
		this.mfchg_change_month = mfchg_change_month;
	}
	public String getMfchg_change_name() {
		return mfchg_change_name;
	}
	public void setMfchg_change_name(String mfchg_change_name) {
		this.mfchg_change_name = mfchg_change_name;
	}


}