package com.nativetalk.bean.live;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by Micoo on 2017/5/10.
 */
public class LiveCourse {
    public static class LiveState{
        /**
         * 3 直播结束
         */
        public static final Integer end = 3;
    }

    private Integer id;
    private String title;
    private String intro;
    private String cover_url;

    private String title_student;//  课程标题，直播课名称，微课标题     varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
    private String intro_student;
    private String cover_url_student;//课程封面图片URL

    private Double student_price;
    private Integer buy_number;
    private Integer likes_size;
    private Integer live_cou_state;//状态 1待直播2直播中3直播结束4已取消

    private String teacher_name;
    private String teacher_avatar_url;
    private Integer teacher_valid;//是否验证，0:未验证，1:已验证

    private Integer category_id; //分类id
    private String category_name; //分类名称

    ////标签 0无 1new 2V
    private Integer tea_label;

    public Integer getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Integer category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public Integer getTeacher_valid() {
        if(tea_label == null){
            return  0;
        }
        if(tea_label.intValue() == 0){
            teacher_valid = 0;
        }
        if(tea_label.intValue() == 1){
            teacher_valid = 0;
        }
        if(tea_label.intValue() == 2){
            teacher_valid = 1;
        }
        return teacher_valid;
    }

    public void setTeacher_valid(Integer teacher_valid) {
        this.teacher_valid = teacher_valid;
    }

    public Integer getLikes_size() {
        return likes_size;
    }

    public void setLikes_size(Integer likes_size) {
        this.likes_size = likes_size;
    }

    public Integer getBuy_number() {
        return buy_number;
    }

    public void setBuy_number(Integer buy_number) {
        this.buy_number = buy_number;
    }

    public Double getStudent_price() {
        return student_price;
    }

    public void setStudent_price(Double student_price) {
        this.student_price = student_price;
    }

    public Integer getLive_cou_state() {
        return live_cou_state;
    }

    public void setLive_cou_state(Integer live_cou_state) {
        this.live_cou_state = live_cou_state;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getCover_url() {
        return cover_url;
    }

    public void setCover_url(String cover_url) {
        this.cover_url = cover_url;
    }

    public String getTeacher_name() {
        return teacher_name;
    }

    public void setTeacher_name(String teacher_name) {
        this.teacher_name = teacher_name;
    }

    public String getTeacher_avatar_url() {
        return teacher_avatar_url;
    }

    public void setTeacher_avatar_url(String teacher_avatar_url) {
        this.teacher_avatar_url = teacher_avatar_url;
    }


    public String getTitle_student() {
        if (StringUtils.isBlank(title_student)) {
            this.title_student = title;
        }
        return title_student;
    }

    public void setTitle_student(String title_student) {
        this.title_student = title_student;
    }

    public String getIntro_student() {
        if (StringUtils.isBlank(intro_student)) {
            this.intro_student = intro;
        }
        return intro_student;
    }

    public void setIntro_student(String intro_student) {
        this.intro_student = intro_student;
    }

    public String getCover_url_student() {
        if (StringUtils.isBlank(cover_url_student)) {
            this.cover_url_student = cover_url;
        }
        return cover_url_student;
    }

    public void setCover_url_student(String cover_url_student) {
        this.cover_url_student = cover_url_student;
    }
}
