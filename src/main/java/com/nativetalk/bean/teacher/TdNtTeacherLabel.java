package com.nativetalk.bean.teacher;

/**
 * Created by lizhun on 2016/10/28.
 * 老师标签
 */

public class TdNtTeacherLabel {
    private int tea_lab_id;//id
    private int tea_id;//老师id
    private String lab_cn_name;//标签中文
    private String lab_en_name;//标签英文
    private int lab_type;//标签类别1系统2自定义
    private int state;//是否使用：0使用，1不使用，删除

    public int getTea_lab_id() {
        return tea_lab_id;
    }

    public void setTea_lab_id(int tea_lab_id) {
        this.tea_lab_id = tea_lab_id;
    }

    public int getTea_id() {
        return tea_id;
    }

    public void setTea_id(int tea_id) {
        this.tea_id = tea_id;
    }

    public String getLab_cn_name() {
        return lab_cn_name;
    }

    public void setLab_cn_name(String lab_cn_name) {
        this.lab_cn_name = lab_cn_name;
    }

    public String getLab_en_name() {
        return lab_en_name;
    }

    public void setLab_en_name(String lab_en_name) {
        this.lab_en_name = lab_en_name;
    }

    public int getLab_type() {
        return lab_type;
    }

    public void setLab_type(int lab_type) {
        this.lab_type = lab_type;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
}
