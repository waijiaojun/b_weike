package com.nativetalk.bean.teacher;

/**
 * Created by Micoo on 2017/5/10.
 */
public class TeacherLabel {
    private Integer tea_lab_id;
    private Integer tea_id;
    private String lab_cn_name;

    public Integer getTea_lab_id() {
        return tea_lab_id;
    }

    public void setTea_lab_id(Integer tea_lab_id) {
        this.tea_lab_id = tea_lab_id;
    }

    public Integer getTea_id() {
        return tea_id;
    }

    public void setTea_id(Integer tea_id) {
        this.tea_id = tea_id;
    }

    public String getLab_cn_name() {
        return lab_cn_name;
    }

    public void setLab_cn_name(String lab_cn_name) {
        this.lab_cn_name = lab_cn_name;
    }
}
