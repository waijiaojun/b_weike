package com.nativetalk.bean.teacher;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * TdNtTeacher entity. @author MyEclipse Persistence Tools
 */

public class TdNtTeacherResult implements java.io.Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	private Integer tea_id;//教师id
	private String tea_name;//教师名称
	private String tea_email;//教师Email
	private String tea_password;//密码
	private Timestamp tea_register_time;//注册时间
	private String tea_register_month;//注册月份
	private BigDecimal tea_balance;//余额
    /**
     * 1：正常使用；2：认证中；3：审核不通过；4：未认证（新注册用户还未申请认证）
     */
    private String tea_islock;//
    private String tea_head_portrait;//头像
	private String tea_sex;//性别1男2女3其他
	private Timestamp tea_birthday;//生日
	private String tea_synopsis;//简介
	private String tea_sound_synopsis;//声音简介
	private Integer tea_ages;//年龄
	private BigDecimal tea_score;//评分
	private String tea_invite_code;//邀请码
	private String token;//token
	private String state;//0在线 1忙碌 2离线
	private BigDecimal tea_total_balance;//总收入
	private Integer tea_times;//教学分钟数
	private String equ_type;//1android 2ios
	private String equ_client_id;//设备唯一标示  Android:clientId ios:devicetoken
	private String dict_id;//学校id
	private String payPal;//paypal账号
	private int test;//
	private int tea_total_times;


	/**
	 * 新增
	 */
	private int popularity;//人气值
	private int time_zone;//时区1北京（默认）2纽约3伦敦
	private int pay_type;//支付类别1,paypal2.alipay 默认1
	private BigDecimal min_amount;//自由话题每分钟价格 默认0.2
	/**
	 * 3.2新增
	 */
	private int nationality;//国籍 默认为0  0时前端显示美国
	private int tea_label;//标签 0无 1new 2V
	private String tea_cn_synopsis;//中文简介
	/**
	 * 是否删除
	 */
	public static class Role{
		/**
		 *  0 无
		 */
		public static final Integer NONE = 0;

		/**
		 * 1 新用户
		 */
		public static final Integer NEW = 1;
		/**
		 * 1 VIP
		 */
		public static final Integer VIP = 2;
	}



	public String getTea_cn_synopsis() {
		return tea_cn_synopsis;
	}

	public void setTea_cn_synopsis(String tea_cn_synopsis) {
		this.tea_cn_synopsis = tea_cn_synopsis;
	}

	public int getNationality() {
		return nationality;
	}

	public void setNationality(int nationality) {
		this.nationality = nationality;
	}

	public int getTea_label() {
		return tea_label;
	}

	public void setTea_label(int tea_label) {
		this.tea_label = tea_label;
	}

	public int getTest() {
		return test;
	}

	public void setTest(int test) {
		this.test = test;
	}

	public int getTea_total_times() {
		return tea_total_times;
	}

	public void setTea_total_times(int tea_total_times) {
		this.tea_total_times = tea_total_times;
	}
	public int getPay_type() {
		return pay_type;
	}

	public void setPay_type(int pay_type) {
		this.pay_type = pay_type;
	}

	public BigDecimal getMin_amount() {
		return min_amount;
	}

	public void setMin_amount(BigDecimal min_amount) {
		this.min_amount = min_amount;
	}
	public int getPopularity() {
		return popularity;
	}

	public void setPopularity(int popularity) {
		this.popularity = popularity;
	}

	public int getTime_zone() {
		return time_zone;
	}

	public void setTime_zone(int time_zone) {
		this.time_zone = time_zone;
	}

	public void setPayPal(String payPal) {
		this.payPal = payPal;
	}

	public String getPayPal() {
		return payPal;
	}
	public String getDict_id() {
		return dict_id;
	}

	public void setDict_id(String dict_id) {
		this.dict_id = dict_id;
	}

	public void setEqu_type(String equ_type) {
		this.equ_type = equ_type;
	}

	public void setEqu_client_id(String equ_client_id) {
		this.equ_client_id = equ_client_id;
	}

	public String getEqu_type() {
		return equ_type;
	}

	public String getEqu_client_id() {
		return equ_client_id;
	}
	public BigDecimal getTea_total_balance() {
		return tea_total_balance;
	}

	public Integer getTea_times() {
		return tea_times;
	}

	public void setTea_times(Integer tea_times) {
		this.tea_times = tea_times;
	}

	public void setTea_total_balance(BigDecimal tea_total_balance) {
		this.tea_total_balance = tea_total_balance;
	}
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	public String getToken(){ return  token;}
	public void  setToken(String token){ this.token = token;}
	public Integer getTea_id() {
		return tea_id;
	}
	public void setTea_id(Integer tea_id) {
		this.tea_id = tea_id;
	}
	public String getTea_name() {
		return tea_name;
	}
	public void setTea_name(String tea_name) {
		this.tea_name = tea_name;
	}
	public String getTea_email() {
		return tea_email;
	}
	public void setTea_email(String tea_email) {
		this.tea_email = tea_email;
	}
	public String getTea_password() {
		return tea_password;
	}
	public void setTea_password(String tea_password) {
		this.tea_password = tea_password;
	}
	public Timestamp getTea_register_time() {
		return tea_register_time;
	}
	public void setTea_register_time(Timestamp tea_register_time) {
		this.tea_register_time = tea_register_time;
	}
	public String getTea_register_month() {
		return tea_register_month;
	}
	public void setTea_register_month(String tea_register_month) {
		this.tea_register_month = tea_register_month;
	}
	public BigDecimal getTea_balance() {
		return tea_balance;
	}
	public void setTea_balance(BigDecimal tea_balance) {
		this.tea_balance = tea_balance;
	}
	public String getTea_islock() {
		return tea_islock;
	}
	public void setTea_islock(String tea_islock) {
		this.tea_islock = tea_islock;
	}
	public String getTea_head_portrait() {
		return tea_head_portrait;
	}
	public void setTea_head_portrait(String tea_head_portrait) {
		this.tea_head_portrait = tea_head_portrait;
	}
	public String getTea_sex() {
		return tea_sex;
	}
	public void setTea_sex(String tea_sex) {
		this.tea_sex = tea_sex;
	}
	public Timestamp getTea_birthday() {
		return tea_birthday;
	}
	public void setTea_birthday(Timestamp tea_birthday) {
		this.tea_birthday = tea_birthday;
	}
	public String getTea_synopsis() {
		return tea_synopsis;
	}
	public void setTea_synopsis(String tea_synopsis) {
		this.tea_synopsis = tea_synopsis;
	}
	public String getTea_sound_synopsis() {
		return tea_sound_synopsis;
	}
	public void setTea_sound_synopsis(String tea_sound_synopsis) {
		this.tea_sound_synopsis = tea_sound_synopsis;
	}
	public Integer getTea_ages() {
		return tea_ages;
	}
	public void setTea_ages(Integer tea_ages) {
		this.tea_ages = tea_ages;
	}
	public BigDecimal getTea_score() {
		return tea_score;
	}
	public void setTea_score(BigDecimal tea_score) {
		this.tea_score = tea_score;
	}
	public String getTea_invite_code() {
		return tea_invite_code;
	}
	public void setTea_invite_code(String tea_invite_code) {
		this.tea_invite_code = tea_invite_code;
	}

	@Override
	public String toString() {
		return "TdNtTeacherResult{" +
				"tea_id=" + tea_id +
				", tea_name='" + tea_name + '\'' +
				", tea_email='" + tea_email + '\'' +
				", tea_password='" + tea_password + '\'' +
				", tea_register_time=" + tea_register_time +
				", tea_register_month='" + tea_register_month + '\'' +
				", tea_balance=" + tea_balance +
				", tea_islock='" + tea_islock + '\'' +
				", tea_head_portrait='" + tea_head_portrait + '\'' +
				", tea_sex='" + tea_sex + '\'' +
				", tea_birthday=" + tea_birthday +
				", tea_synopsis='" + tea_synopsis + '\'' +
				", tea_sound_synopsis='" + tea_sound_synopsis + '\'' +
				", tea_ages=" + tea_ages +
				", tea_score=" + tea_score +
				", tea_invite_code='" + tea_invite_code + '\'' +
				", token='" + token + '\'' +
				", state='" + state + '\'' +
				", tea_total_balance=" + tea_total_balance +
				", tea_times=" + tea_times +
				", equ_type='" + equ_type + '\'' +
				", equ_client_id='" + equ_client_id + '\'' +
				", dict_id='" + dict_id + '\'' +
				", payPal='" + payPal + '\'' +
				", tea_total_times=" + tea_total_times +
				", popularity=" + popularity +
				", time_zone=" + time_zone +
				", pay_type=" + pay_type +
				", min_amount=" + min_amount +
				", nationality=" + nationality +
				", tea_label=" + tea_label +
				", tea_cn_synopsis='" + tea_cn_synopsis + '\'' +
				'}';
	}
}