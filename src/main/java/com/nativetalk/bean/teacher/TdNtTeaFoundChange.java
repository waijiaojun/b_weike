package com.nativetalk.bean.teacher;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * TdNtTeaFoundChange2 entity. @author MyEclipse Persistence Tools
 */

public class TdNtTeaFoundChange implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Fields

	private Integer tfchg_id;
	private Integer tea_id;
	private BigDecimal tfchg_change_amount;//变化金额
	private BigDecimal tfchg_front_amount;//变化之前的余额
	private BigDecimal tfchg_back_amount;//变化之后的余额
	private String tfchg_change_type;//充值（进入余额：+）:1；消费（余额支出：-）:2
	private Timestamp tfchg_change_time;//资金变动时间
	private String tfchg_change_month;//资金变动月份
	private String tfchg_change_name;//资金变化名称
	private BigDecimal tfchg_account;//目前不使用（不知道什么作用）
	private BigDecimal tfchg_counter_fee;//目前不使用（不知道什么作用）
	private String state;//1转账中2已付款
	private String tfchg_number;//订单编号
	private int type;//资金变动类型 0Freetalk 1Appointment 2Withdraw deposit 3Others

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getState() {
		return state;
	}

	public String getTfchg_number() {
		return tfchg_number;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setTfchg_number(String tfchg_number) {
		this.tfchg_number = tfchg_number;
	}
	public Integer getTfchg_id() {
		return tfchg_id;
	}
	public void setTfchg_id(Integer tfchg_id) {
		this.tfchg_id = tfchg_id;
	}
	public Integer getTea_id() {
		return tea_id;
	}
	public void setTea_id(Integer tea_id) {
		this.tea_id = tea_id;
	}
	public BigDecimal getTfchg_change_amount() {
		return tfchg_change_amount;
	}
	public void setTfchg_change_amount(BigDecimal tfchg_change_amount) {
		this.tfchg_change_amount = tfchg_change_amount;
	}
	public BigDecimal getTfchg_front_amount() {
		return tfchg_front_amount;
	}
	public void setTfchg_front_amount(BigDecimal tfchg_front_amount) {
		this.tfchg_front_amount = tfchg_front_amount;
	}
	public BigDecimal getTfchg_back_amount() {
		return tfchg_back_amount;
	}
	public void setTfchg_back_amount(BigDecimal tfchg_back_amount) {
		this.tfchg_back_amount = tfchg_back_amount;
	}
	public String getTfchg_change_type() {
		return tfchg_change_type;
	}
	public void setTfchg_change_type(String tfchg_change_type) {
		this.tfchg_change_type = tfchg_change_type;
	}
	public Timestamp getTfchg_change_time() {
		return tfchg_change_time;
	}
	public void setTfchg_change_time(Timestamp tfchg_change_time) {
		this.tfchg_change_time = tfchg_change_time;
	}
	public String getTfchg_change_month() {
		return tfchg_change_month;
	}
	public void setTfchg_change_month(String tfchg_change_month) {
		this.tfchg_change_month = tfchg_change_month;
	}
	public String getTfchg_change_name() {
		return tfchg_change_name;
	}
	public void setTfchg_change_name(String tfchg_change_name) {
		this.tfchg_change_name = tfchg_change_name;
	}
	public BigDecimal getTfchg_account() {
		return tfchg_account;
	}
	public void setTfchg_account(BigDecimal tfchg_account) {
		this.tfchg_account = tfchg_account;
	}
	public BigDecimal getTfchg_counter_fee() {
		return tfchg_counter_fee;
	}
	public void setTfchg_counter_fee(BigDecimal tfchg_counter_fee) {
		this.tfchg_counter_fee = tfchg_counter_fee;
	}

	

}