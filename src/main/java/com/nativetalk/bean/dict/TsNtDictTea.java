package com.nativetalk.bean.dict;

/**
 * Created by Micoo on 2017/4/15.
 */
public class TsNtDictTea implements java.io.Serializable{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private Integer dict_id;//id
    private String code_eng_name;//英文名字

    public String getCode_eng_name() {
        return code_eng_name;
    }

    public Integer getDict_id() {
        return dict_id;
    }

    public void setDict_id(Integer dict_id) {
        this.dict_id = dict_id;
    }

    public void setCode_eng_name(String code_eng_name) {
        this.code_eng_name = code_eng_name;
    }
}
