package com.nativetalk.base.course;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * TsNtCourseInfo entity. @author MyEclipse Persistence Tools
 * 课程信息表
 */

public class TwoTsNtCourseInfoDetail implements java.io.Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    // Fields

    private Integer cou_id;//id
    private String cou_name;//名称
    private Integer cate_id;//类别id
    private String cou_synopsis;//简介
    private Integer cou_sort;//排序
    private Integer is_valid;//是否显示 0是1否
    private String creater;//创建者
    private Timestamp cret_date;//创建日期
    private String modifier;//最后修改者
    private Timestamp mod_date;//修改日期
    private String stand;//封面图片
    private String remark;//状态 1无2new3hot
    private Integer study_size;//已学习人数
    private String cou_grade;//学习难度 1初级2中级3高级
    private Integer study_time;//预计学习时间  单位分钟
    private String video_url;//视频URL
    private String cou_english_name;//英文名称
    private String cou_arrangement;//课程安排
    private String cou_suitable_object;//适合对象
    private String cou_teaching_objectives;//教学目标
    private String cou_introduce;//介绍
    private String cou_video_picture;//视频封面
    private BigDecimal cou_hour_amount;//课程每小时价格

    public BigDecimal getCou_hour_amount() {
        return cou_hour_amount;
    }

    public void setCou_hour_amount(BigDecimal cou_hour_amount) {
        this.cou_hour_amount = cou_hour_amount;
    }
    public String getCou_video_picture() {
        return cou_video_picture;
    }

    public void setCou_video_picture(String cou_video_picture) {
        this.cou_video_picture = cou_video_picture;
    }

    public String getCou_arrangement() {
        return cou_arrangement;
    }

    public void setCou_arrangement(String cou_arrangement) {
        this.cou_arrangement = cou_arrangement;
    }

    public String getCou_suitable_object() {
        return cou_suitable_object;
    }

    public void setCou_suitable_object(String cou_suitable_object) {
        this.cou_suitable_object = cou_suitable_object;
    }

    public String getCou_teaching_objectives() {
        return cou_teaching_objectives;
    }

    public void setCou_teaching_objectives(String cou_teaching_objectives) {
        this.cou_teaching_objectives = cou_teaching_objectives;
    }

    public String getCou_introduce() {
        return cou_introduce;
    }

    public void setCou_introduce(String cou_introduce) {
        this.cou_introduce = cou_introduce;
    }

    public void setCou_english_name(String cou_english_name) {
        this.cou_english_name = cou_english_name;
    }

    public String getCou_english_name() {
        return cou_english_name;
    }
    public Integer getCou_id() {
        return cou_id;
    }
    public void setCou_id(Integer cou_id) {
        this.cou_id = cou_id;
    }
    public String getCou_name() {
        return cou_name;
    }
    public void setCou_name(String cou_name) {
        this.cou_name = cou_name;
    }
    public Integer getCate_id() {
        return cate_id;
    }
    public void setCate_id(Integer cate_id) {
        this.cate_id = cate_id;
    }
    public String getCou_synopsis() {
        return cou_synopsis;
    }
    public void setCou_synopsis(String cou_synopsis) {
        this.cou_synopsis = cou_synopsis;
    }
    public Integer getCou_sort() {
        return cou_sort;
    }
    public void setCou_sort(Integer cou_sort) {
        this.cou_sort = cou_sort;
    }
    public Integer getIs_valid() {
        return is_valid;
    }
    public void setIs_valid(Integer is_valid) {
        this.is_valid = is_valid;
    }
    public String getCreater() {
        return creater;
    }
    public void setCreater(String creater) {
        this.creater = creater;
    }
    public Timestamp getCret_date() {
        return cret_date;
    }
    public void setCret_date(Timestamp cret_date) {
        this.cret_date = cret_date;
    }
    public String getModifier() {
        return modifier;
    }
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }
    public Timestamp getMod_date() {
        return mod_date;
    }
    public void setMod_date(Timestamp mod_date) {
        this.mod_date = mod_date;
    }
    public String getStand() {
        return stand;
    }
    public void setStand(String stand) {
        this.stand = stand;
    }
    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }
    public Integer getStudy_size() {
        return study_size;
    }
    public void setStudy_size(Integer study_size) {
        this.study_size = study_size;
    }
    public String getCou_grade() {
        return cou_grade;
    }
    public void setCou_grade(String cou_grade) {
        this.cou_grade = cou_grade;
    }
    public Integer getStudy_time() {
        return study_time;
    }
    public void setStudy_time(Integer study_time) {
        this.study_time = study_time;
    }
    public String getVideo_url() {
        return video_url;
    }
    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }
}