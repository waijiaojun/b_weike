package com.benmei.weike.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Peter on 2017/6/12.
 */
@Configuration
public class RabbitMQConfig {

    @Bean
    public Queue InitQueue() {
        String queueName = "teenager-weike-published";
        return new Queue(queueName);
    }
}
