package com.benmei.weike.config;

import net.rubyeye.xmemcached.MemcachedClient;
import net.rubyeye.xmemcached.utils.XMemcachedClientFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * Created by Peter on 2017/6/12.
 */
@Configuration
public class MemCacheConfig {
    @Autowired
    private Environment environment;

    @Bean
    public MemcachedClient InitMemcachedClient() {
        final String servers = environment.getProperty("memcached.servers");

        XMemcachedClientFactoryBean factoryBean = new XMemcachedClientFactoryBean();
        factoryBean.setServers(servers);
        MemcachedClient client = null;
        try {
            client = (MemcachedClient) factoryBean.getObject();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return client;
    }
}
