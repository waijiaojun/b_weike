package com.benmei.weike.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by waijiaojun on 2017/11/3.
 */
@Component
public class SmallProgramParameter {

    @Value("${wechat.small.program.appid}")
    private String appId;

    @Value("${wechat.small.program.secret}")
    private String secret;

    @Value("${wechat.payment.mch_id}")
    private String mch_id;//微信支付分配的商户号1316213501

    @Value("${wechat.payment.api_key}")
    private String api_key;//API密钥 调用api对参数签名时需要使用该秘钥，网站支付也使用了该秘钥
    @Value("${wechat.payment.notify_url}")
    private String notify_url;

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getMch_id() {
        return mch_id;
    }

    public void setMch_id(String mch_id) {
        this.mch_id = mch_id;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
