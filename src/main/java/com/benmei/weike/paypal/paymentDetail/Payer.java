package com.benmei.weike.paypal.paymentDetail;

public class Payer {
    private String payment_method;
    private String status;
    private String phone;
    private String country_code;
    private PayInfo payer_info;
    public void setPayment_method(String payment_method){
        this.payment_method = payment_method;
    }
    public String getPayment_method(){
        return this.payment_method;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public PayInfo getPayer_info() {
        return payer_info;
    }

    public void setPayer_info(PayInfo payer_info) {
        this.payer_info = payer_info;
    }
}
