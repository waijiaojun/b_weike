package com.benmei.weike.paypal.paymentDetail;

public class ShippingAddress {
    private String recipient_name;

    private String line1;

    private String line2;

    private String city;

    private String state;

    private String phone;

    private String postal_code;

    private String country_code;

    public void setRecipient_name(String recipient_name){
        this.recipient_name = recipient_name;
    }
    public String getRecipient_name(){
        return this.recipient_name;
    }
    public void setLine1(String line1){
        this.line1 = line1;
    }
    public String getLine1(){
        return this.line1;
    }
    public void setLine2(String line2){
        this.line2 = line2;
    }
    public String getLine2(){
        return this.line2;
    }
    public void setCity(String city){
        this.city = city;
    }
    public String getCity(){
        return this.city;
    }
    public void setState(String state){
        this.state = state;
    }
    public String getState(){
        return this.state;
    }
    public void setPhone(String phone){
        this.phone = phone;
    }
    public String getPhone(){
        return this.phone;
    }
    public void setPostal_code(String postal_code){
        this.postal_code = postal_code;
    }
    public String getPostal_code(){
        return this.postal_code;
    }
    public void setCountry_code(String country_code){
        this.country_code = country_code;
    }
    public String getCountry_code(){
        return this.country_code;
    }

}
