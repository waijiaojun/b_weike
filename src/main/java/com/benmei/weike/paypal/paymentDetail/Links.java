package com.benmei.weike.paypal.paymentDetail;

public class Links {
    private String href;

    private String rel;

    private String method;

    public void setHref(String href){
        this.href = href;
    }
    public String getHref(){
        return this.href;
    }
    public void setRel(String rel){
        this.rel = rel;
    }
    public String getRel(){
        return this.rel;
    }
    public void setMethod(String method){
        this.method = method;
    }
    public String getMethod(){
        return this.method;
    }

}
