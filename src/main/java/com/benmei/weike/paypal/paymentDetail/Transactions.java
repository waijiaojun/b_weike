package com.benmei.weike.paypal.paymentDetail;

import com.benmei.weike.paypal.paymentDetail.Amount;
import com.benmei.weike.paypal.paymentDetail.ItemList;
import com.benmei.weike.paypal.paymentDetail.RelatedResources;

import java.util.List;

public class Transactions {
    private Amount amount;

    private String description;

    private ItemList item_list;

    private String custom;

    private Payee payee;

    private List<RelatedResources> related_resources ;

    public String getCustom() {
        return custom;
    }

    public void setCustom(String custom) {
        this.custom = custom;
    }

    public void setAmount(Amount amount){
        this.amount = amount;
    }
    public Amount getAmount(){
        return this.amount;
    }
    public void setDescription(String description){
        this.description = description;
    }
    public String getDescription(){
        return this.description;
    }
    public void setItem_list(ItemList item_list){
        this.item_list = item_list;
    }
    public ItemList getItem_list(){
        return this.item_list;
    }
    public void setRelated_resources(List<RelatedResources> related_resources){
        this.related_resources = related_resources;
    }
    public List<RelatedResources> getRelated_resources(){
        return this.related_resources;
    }

    public Payee getPayee() {
        return payee;
    }

    public void setPayee(Payee payee) {
        this.payee = payee;
    }
}
