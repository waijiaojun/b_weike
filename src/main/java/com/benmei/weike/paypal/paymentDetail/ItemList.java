package com.benmei.weike.paypal.paymentDetail;

import java.util.List;

public class ItemList {
    private List<Items> items ;

    private ShippingAddress shipping_address;

    public void setItems(List<Items> items){
        this.items = items;
    }
    public List<Items> getItems(){
        return this.items;
    }
    public void setShipping_address(ShippingAddress shipping_address){
        this.shipping_address = shipping_address;
    }
    public ShippingAddress getShipping_address(){
        return this.shipping_address;
    }
}
