package com.benmei.weike.paypal.paymentDetail;

public class Amount {
    private String total;

    private String currency;

    private Details details;

    public void setTotal(String total){
        this.total = total;
    }
    public String getTotal(){
        return this.total;
    }
    public void setCurrency(String currency){
        this.currency = currency;
    }
    public String getCurrency(){
        return this.currency;
    }
    public void setDetails(Details details){
        this.details = details;
    }
    public Details getDetails(){
        return this.details;
    }
}
