package com.benmei.weike.paypal.paymentDetail;

public class Details {
    private String subtotal;

    private String tax;

    private String shipping;

    public void setSubtotal(String subtotal){
        this.subtotal = subtotal;
    }
    public String getSubtotal(){
        return this.subtotal;
    }
    public void setTax(String tax){
        this.tax = tax;
    }
    public String getTax(){
        return this.tax;
    }
    public void setShipping(String shipping){
        this.shipping = shipping;
    }
    public String getShipping(){
        return this.shipping;
    }
}
