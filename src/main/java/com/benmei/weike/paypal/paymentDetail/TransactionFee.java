package com.benmei.weike.paypal.paymentDetail;

import java.math.BigDecimal;

public class TransactionFee {
    private String value;
    private String currency;

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
