package com.benmei.weike.paypal;

import com.benmei.weike.paypal.paymentDetail.Links;
import com.benmei.weike.paypal.paymentDetail.Payer;
import com.benmei.weike.paypal.paymentDetail.Transactions;

import java.util.Date;
import java.util.List;

public class GetPaymentDetail {
    private String id;

    private Date create_time;

    private Date update_time;

    private String state;

    private String intent;

    private String cart;

    private Payer payer;

    private List<Transactions> transactions ;

    private List<Links> links ;

    public void setId(String id){
        this.id = id;
    }
    public String getId(){
        return this.id;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public Date getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }

    public void setState(String state){
        this.state = state;
    }
    public String getState(){
        return this.state;
    }
    public void setIntent(String intent){
        this.intent = intent;
    }
    public String getIntent(){
        return this.intent;
    }
    public void setPayer(Payer payer){
        this.payer = payer;
    }
    public Payer getPayer(){
        return this.payer;
    }
    public void setTransactions(List<Transactions> transactions){
        this.transactions = transactions;
    }
    public List<Transactions> getTransactions(){
        return this.transactions;
    }
    public void setLinks(List<Links> links){
        this.links = links;
    }
    public List<Links> getLinks(){
        return this.links;
    }

    public String getCart() {
        return cart;
    }

    public void setCart(String cart) {
        this.cart = cart;
    }
}
