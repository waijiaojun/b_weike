package com.benmei.weike.paypal;

public class GetAccessToken {
    private String scope;

    private String nonce;

    private String access_token;

    private String token_type;

    private String app_id;

    private int expires_in;

    public void setScope(String scope){
        this.scope = scope;
    }
    public String getScope(){
        return this.scope;
    }
    public void setNonce(String nonce){
        this.nonce = nonce;
    }
    public String getNonce(){
        return this.nonce;
    }
    public void setAccess_token(String access_token){
        this.access_token = access_token;
    }
    public String getAccess_token(){
        return this.access_token;
    }
    public void setToken_type(String token_type){
        this.token_type = token_type;
    }
    public String getToken_type(){
        return this.token_type;
    }
    public void setApp_id(String app_id){
        this.app_id = app_id;
    }
    public String getApp_id(){
        return this.app_id;
    }
    public void setExpires_in(int expires_in){
        this.expires_in = expires_in;
    }
    public int getExpires_in(){
        return this.expires_in;
    }
}
