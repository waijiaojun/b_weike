package com.benmei.weike.dto;

/**
 * Created by Peter on 2017/6/12.
 */
public class MemberContentRecommendDto {
    private Integer recommend_id; //推荐ID
    private Long content_id;//内容对应的ID
    private String title;//内容标题
    private String cover_url;// 内容封面图片
    private String from;// 内容来源：公开课、微课、课程
    private String type;// 内容类型：live、weike、course

    public Integer getRecommend_id() {
        return recommend_id;
    }

    public void setRecommend_id(Integer recommend_id) {
        this.recommend_id = recommend_id;
    }

    public Long getContent_id() {
        return content_id;
    }

    public void setContent_id(Long content_id) {
        this.content_id = content_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCover_url() {
        return cover_url;
    }

    public void setCover_url(String cover_url) {
        this.cover_url = cover_url;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
