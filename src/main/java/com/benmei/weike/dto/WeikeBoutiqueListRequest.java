package com.benmei.weike.dto;

/**
 * Created by Micoo on 2017/5/3.
 */
public class WeikeBoutiqueListRequest {
    private Long currentPage = 1L;
    private Long pageSize = 10L;

    public Long getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Long currentPage) {
        this.currentPage = currentPage;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }
}
