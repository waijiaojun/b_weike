package com.benmei.weike.dto;

/**
 * Created by Micoo on 2017/5/2.
 */
public class WeikeFindContentRequest {
    private String weike_id;

    public String getWeike_id() {
        return weike_id;
    }

    public void setWeike_id(String weike_id) {
        this.weike_id = weike_id;
    }
}
