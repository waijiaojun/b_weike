package com.benmei.weike.dto;

/**
 * Created by Micoo on 2017/5/12.
 */
public class StudentIsPurchasedDto {

    public static final Integer Yes = 1;

    private Integer isPurchased = 0;

    public Integer getIsPurchased() {
        return isPurchased;
    }

    public void setIsPurchased(Integer isPurchased) {
        this.isPurchased = isPurchased;
    }
}
