package com.benmei.weike.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;

/**
 * Created by Peter on 2017/8/29.
 */
public class SeriesLiveDetailIntroResponse {
    private Long id;
    private String title;
    private String intro;
    private String cover;
    private Integer status;// 1 正在上课，2 学期已开始，3 学期未开始，4 学期已结束
    private Integer planSaleNumber;// 限量
    private Integer actualSaleNumber;// 实际销售数量
    private Date saleDeadline;//销售截止日期
    private Integer isLive;  //类型：0录播，1直播
    private Integer seriesLiveInstanceId;
    private Double price;// 价格：单位元
    private Date startTime;// 课程开始时间
    private Date endTime;// 课程结束时间
    private Integer isBuy = 0;// 是否购买过了：1 已购买，0 未购买
    private Integer courseSize;// 课时数
    private String remark;//课程说明
    private Integer commentCount;//评价数量

    @JsonIgnore
    private String title_en;
    @JsonIgnore
    private String title_cn;
    @JsonIgnore
    private String intro_en;
    @JsonIgnore
    private String intro_cn;

    public Integer getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    public String getTitle_en() {
        return title_en;
    }

    public void setTitle_en(String title_en) {
        this.title_en = title_en;
    }

    public String getTitle_cn() {
        return title_cn;
    }

    public void setTitle_cn(String title_cn) {
        this.title_cn = title_cn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    // 优先显示中文标题，如果没有中文标题就显示英文标题
    public String getTitle() {
        if (StringUtils.isNotBlank(this.title_cn)) {
            this.title = this.title_cn;
        } else {
            this.title = this.title_en;
        }
        return title;
    }

    // 优先显示英文介绍
    public String getIntro() {
        if (StringUtils.isNotBlank(this.intro_cn)) {
            this.intro = this.intro_cn;
        } else {
            this.intro = this.intro_en;
        }
        return intro;
    }

    public String getIntro_en() {
        return intro_en;
    }

    public void setIntro_en(String intro_en) {
        this.intro_en = intro_en;
    }

    public String getIntro_cn() {
        return intro_cn;
    }

    public void setIntro_cn(String intro_cn) {
        this.intro_cn = intro_cn;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPlanSaleNumber() {
        return planSaleNumber;
    }

    public void setPlanSaleNumber(Integer planSaleNumber) {
        this.planSaleNumber = planSaleNumber;
    }

    public Integer getActualSaleNumber() {
        return actualSaleNumber;
    }

    public void setActualSaleNumber(Integer actualSaleNumber) {
        this.actualSaleNumber = actualSaleNumber;
    }

    public Date getSaleDeadline() {
        return saleDeadline;
    }

    public void setSaleDeadline(Date saleDeadline) {
        this.saleDeadline = saleDeadline;
    }

    public Integer getIsLive() {
        return isLive;
    }

    public void setIsLive(Integer isLive) {
        this.isLive = isLive;
    }

    public Integer getSeriesLiveInstanceId() {
        return seriesLiveInstanceId;
    }

    public void setSeriesLiveInstanceId(Integer seriesLiveInstanceId) {
        this.seriesLiveInstanceId = seriesLiveInstanceId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getIsBuy() {
        return isBuy;
    }

    public void setIsBuy(Integer isBuy) {
        this.isBuy = isBuy;
    }

    public Integer getCourseSize() {
        return courseSize;
    }

    public void setCourseSize(Integer courseSize) {
        this.courseSize = courseSize;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
