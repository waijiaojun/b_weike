package com.benmei.weike.dto;

/**
 * Created by Micoo on 2017/5/4.
 */
public class FindCommentsPageRequest extends PageRequest{
    private Long weike_id;

    public Long getWeike_id() {
        return weike_id;
    }

    public void setWeike_id(Long weike_id) {
        this.weike_id = weike_id;
    }
}
