package com.benmei.weike.dto;

/**
 * Created by Peter on 2017/8/29.
 */
public class SeriesLiveIndexPageRequest extends PageRequest {
    private Integer categoryId;// 分类id,0、Null会查询所有分类

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }
}
