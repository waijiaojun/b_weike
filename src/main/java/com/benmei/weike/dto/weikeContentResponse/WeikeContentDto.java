package com.benmei.weike.dto.weikeContentResponse;

/**
 * <pre>
 *     {"id":1,"sort":1,"type":"audio","content":"xxx/sss.wav","duration":320,"play_number":0},
 * </pre>
 * Created by Peter on 2017/4/25.
 */
public class WeikeContentDto implements Comparable<WeikeContentDto> {
    private Long id;
    private Integer part_sort;
    private Integer sort;
    private String type;
    private String content;
    private Long duration;
    private Long play_number;

    public Long getPlay_number() {
        return play_number;
    }

    public void setPlay_number(Long play_number) {
        this.play_number = play_number;
    }

    public WeikeContentDto() {
    }

    public Integer getPart_sort() {
        return part_sort;
    }

    public void setPart_sort(Integer part_sort) {
        this.part_sort = part_sort;
    }

    //格式化秒为 00′.00″的格式
    public String getDurationFromat() {
        long hours = 0;//小时
        long total_minute = 0;//分钟 ′
        long minute = 0;//分钟 ′
        long second = 0;//秒 ″
        if (duration != null && duration > 0) {
            second = duration % 60; // 剩余的秒

            total_minute = duration / 60; //总计 分钟 可以大于60
            if (total_minute <= 60) {
                minute = total_minute;
            } else {
                minute = total_minute % 60;
                hours = total_minute / 60;
            }
        }

        String result = "";
        if (hours > 0) {
            result = hours + ":";
        }
        if (minute > 0) {
            result = result + minute + "′";
        }
        if (second > 0) {
            result = result + second + "″";
        }
        return result;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    @Override
    public int compareTo(WeikeContentDto o) {
        return this.sort.compareTo(o.getSort());
    }
}
