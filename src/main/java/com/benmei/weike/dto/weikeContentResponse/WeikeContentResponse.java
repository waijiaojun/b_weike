package com.benmei.weike.dto.weikeContentResponse;

import com.benmei.weike.common.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 *{
    "images":[
         {"sort":1,"part":1,"url":"xxx/xxx.jpg"}
     ],
    "parts":[{
         "sort":1,
         "title":"段落标题",
         "contents":[
             {"id":1,"sort":1,"type":"audio","content":"xxx/sss.wav","duration":320,"play_number":0},
             {"id":2,"sort":2,"type":"text","content":"This content very good.","duration":0,"play_number":0
             }
         ]
    }]
 }
 * </pre>
 * Created by Peter on 2017/4/18.
 */
public class WeikeContentResponse {
    private String teacher_avatar_url="";  //老师头像
    public Long weike_id;
    public List<WeikeImageDto> images = new ArrayList<>();
    public List<WeikePartDto> parts = new ArrayList<>();
    public String weike_video;  //微课视频
    public String weike_video_img;  //微课视频封面图

    public String getWeike_video_img() {
        return weike_video_img;
    }

    public void setWeike_video_img(String weike_video_img) {
        this.weike_video_img = weike_video_img;
    }

    public String getWeike_video() {
        return weike_video;
    }

    public void setWeike_video(String weike_video) {
        this.weike_video = weike_video;
    }

    public Long getWeike_id() {
        return weike_id;
    }

    public void setWeike_id(Long weike_id) {
        this.weike_id = weike_id;
    }

    public List<WeikeImageDto> getImages() {
        return images;
    }

    public void setImages(List<WeikeImageDto> images) {
        this.images = images;
    }

    public List<WeikePartDto> getParts() {
        return parts;
    }

    public void setParts(List<WeikePartDto> parts) {
        this.parts = parts;
    }

    public String getTeacher_avatar_url() {
        if (teacher_avatar_url != null && !teacher_avatar_url.startsWith("http://") && !teacher_avatar_url.startsWith("https://")) {
            teacher_avatar_url = Constants.Oss_Url_Prefix +"/" + teacher_avatar_url;
        }
        return teacher_avatar_url;
    }

    public void setTeacher_avatar_url(String teacher_avatar_url) {
        this.teacher_avatar_url = teacher_avatar_url;
    }
}
