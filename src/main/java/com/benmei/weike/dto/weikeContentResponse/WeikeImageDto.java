package com.benmei.weike.dto.weikeContentResponse;

/**
 * <pre>
 *     {"sort":1,"part":1,"url":"xxx/xxx.jpg"}
 * </pre>
 * Created by Peter on 2017/4/25.
 */
public class WeikeImageDto implements Comparable<WeikeImageDto>{
    private Long id;
    private Integer sort;
    private Integer part;
    private String url;

    public WeikeImageDto(Long id,Integer sort, Integer part, String url) {
        this.id=id;
        this.sort = sort;
        this.part = part;
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPart() {
        return part;
    }

    public void setPart(Integer part) {
        this.part = part;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Override
    public int compareTo(WeikeImageDto o) {
        return o.getSort().compareTo(this.sort);
    }
}
