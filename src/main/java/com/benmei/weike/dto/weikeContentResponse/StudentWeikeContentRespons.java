package com.benmei.weike.dto.weikeContentResponse;

import java.math.BigDecimal;

/**
 * Created by Micoo on 2017/5/3.
 */
public class StudentWeikeContentRespons extends WeikeContentResponse {
    /**
     * 是否购买了该课程
     */
    private Integer bought = 0; //是否购买过
    private BigDecimal price; //价格
    private Integer studentScore;//单个学生的评价

    public Integer getStudentScore() {
        return studentScore;
    }

    public void setStudentScore(Integer studentScore) {
        this.studentScore = studentScore;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getBought() {
        return bought;
    }

    public void setBought(Integer bought) {
        this.bought = bought;
    }
}
