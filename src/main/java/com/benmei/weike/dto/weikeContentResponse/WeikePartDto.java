package com.benmei.weike.dto.weikeContentResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 {
    "sort":1,
    "title":"段落标题",
    "contents":[
        {"id":1,"sort":1,"type":"audio","content":"xxx/sss.wav","duration":320,"play_number":0},
        {"id":2,"sort":2,"type":"text","content":"This content very good.","duration":0,"play_number":0}
    ]
 }
 * </pre>
 * Created by Peter on 2017/4/25.
 */
public class WeikePartDto implements Comparable<WeikePartDto>{
    private Long id;
    private Integer sort;
    private String title;
    public List<WeikeContentDto> contents = new ArrayList<>();

    public WeikePartDto(Long id,Integer sort, String title) {
        this.id=id;
        this.sort = sort;
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<WeikeContentDto> getContents() {
        return contents;
    }

    public void setContents(List<WeikeContentDto> contents) {
        this.contents = contents;
    }

    @Override
    public int compareTo(WeikePartDto o) {
        return this.sort.compareTo(o.getSort());
    }
}
