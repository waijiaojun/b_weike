package com.benmei.weike.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Peter on 2017/4/18.
 */
public class TeacherWeikeCreateRequest {

    private Long id;
    private String title;
    private String intro;
    private MultipartFile cover;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public MultipartFile getCover() {
        return cover;
    }

    public void setCover(MultipartFile cover) {
        this.cover = cover;
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
