package com.benmei.weike.dto;

/**
 * Created by Peter on 2017/5/9.
 */
public class HotLiveDto {
    private Integer live_cou_his_id;
    private String live_cou_his_name;
    private String live_cou_his_picture;
    private Integer live_cou_size;
    private String tea_name;

    public Integer getLive_cou_his_id() {
        return live_cou_his_id;
    }

    public void setLive_cou_his_id(Integer live_cou_his_id) {
        this.live_cou_his_id = live_cou_his_id;
    }

    public String getLive_cou_his_name() {
        return live_cou_his_name;
    }

    public void setLive_cou_his_name(String live_cou_his_name) {
        this.live_cou_his_name = live_cou_his_name;
    }

    public String getLive_cou_his_picture() {
        return live_cou_his_picture;
    }

    public void setLive_cou_his_picture(String live_cou_his_picture) {
        this.live_cou_his_picture = live_cou_his_picture;
    }

    public Integer getLive_cou_size() {
        return live_cou_size;
    }

    public void setLive_cou_size(Integer live_cou_size) {
        this.live_cou_size = live_cou_size;
    }

    public String getTea_name() {
        return tea_name;
    }

    public void setTea_name(String tea_name) {
        this.tea_name = tea_name;
    }
}
