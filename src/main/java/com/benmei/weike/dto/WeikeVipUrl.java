package com.benmei.weike.dto;

public class WeikeVipUrl {
    private String vip_url;
    private String vip_link_url;

    public String getVip_url() {
        return vip_url;
    }

    public void setVip_url(String vip_url) {
        this.vip_url = vip_url;
    }

    public String getVip_link_url() {
        return vip_link_url;
    }

    public void setVip_link_url(String vip_link_url) {
        this.vip_link_url = vip_link_url;
    }
}
