package com.benmei.weike.dto.wechatSmallProgram.payment;

import com.benmei.weike.util.WeixinPaySignUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Peter on 2017/11/22.
 */
public class CreateWeixinPayOrderResponse {
    private String appId;//	小程序ID	是	String	wxd678efh567hg6787	微信分配的小程序ID
    private String timeStamp;//	时间戳	是	String	1490840662	时间戳从1970年1月1日00:00:00至今的秒数,即当前的时间
    private String nonceStr;//	 随机串 是	String	5K8264ILTKCH16CQ2502SI8ZNMTM67VS	随机字符串，不长于32位。推荐随机数生成算法
    private String data;//	数据包是	String	prepay_id=wx2017033010242291fcfe0db70013231072	统一下单接口返回的 prepay_id 参数值，提交格式如：prepay_id=wx2017033010242291fcfe0db70013231072
    private String signType;//	签名方式是	String	MD5	签名类型，默认为MD5，支持HMAC-SHA256和MD5。注意此处需与统一下单的签名类型一致

    //paySign = MD5(appId=wxd678efh567hg6787&nonceStr=5K8264ILTKCH16CQ2502SI8ZNMTM67VS&package=prepay_id=wx2017033010242291fcfe0db70013231072&signType=MD5&timeStamp=1490840662&key=qazwsxedcrfvtgbyhnujmikolp111111)
    //        = 22D9B4E54AB1950F51E0649E8810ACD6
    private String paySign;

    public String getPaySign() {
        return paySign;
    }

    public void setPaySign(String paySign) {
        this.paySign = paySign;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getNonceStr() {
        return nonceStr;
    }

    public void setNonceStr(String nonceStr) {
        this.nonceStr = nonceStr;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getSignType() {
        return signType;
    }

    public void setSignType(String signType) {
        this.signType = signType;
    }
}
