package com.benmei.weike.dto.wechatSmallProgram;

public class CheckoutUserState {
    private String memb_phone;//用户注册手机号码
    private String code;//手机验证码
    private String memb_phone_area;//手机号区号
    private String js_code; //微信端的code

    public String getMemb_phone() {
        return memb_phone;
    }

    public void setMemb_phone(String memb_phone) {
        this.memb_phone = memb_phone;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMemb_phone_area() {
        return memb_phone_area;
    }

    public void setMemb_phone_area(String memb_phone_area) {
        this.memb_phone_area = memb_phone_area;
    }

    public String getJs_code() {
        return js_code;
    }

    public void setJs_code(String js_code) {
        this.js_code = js_code;
    }
}
