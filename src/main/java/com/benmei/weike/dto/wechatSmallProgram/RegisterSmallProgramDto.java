package com.benmei.weike.dto.wechatSmallProgram;

/**
 * Created by waijiaojun on 2017/11/8.
 */
public class RegisterSmallProgramDto {
    private String memberPhoneArea; //手机号地区
    private String memberPhone;      //手机号
    private String memberName;       //用户微信名
    private String sex;               //用户性别
    private String memberPassword;   //用户密码
    private String memberHeadPortrait;  //用户头像
    private String code;            //获取openID的jscode

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMemberPhoneArea() {
        return memberPhoneArea;
    }

    public void setMemberPhoneArea(String memberPhoneArea) {
        this.memberPhoneArea = memberPhoneArea;
    }

    public String getMemberPhone() {
        return memberPhone;
    }

    public void setMemberPhone(String memberPhone) {
        this.memberPhone = memberPhone;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberPassword() {
        return memberPassword;
    }

    public void setMemberPassword(String memberPassword) {
        this.memberPassword = memberPassword;
    }

    public String getMemberHeadPortrait() {
        return memberHeadPortrait;
    }

    public void setMemberHeadPortrait(String memberHeadPortrait) {
        this.memberHeadPortrait = memberHeadPortrait;
    }
}
