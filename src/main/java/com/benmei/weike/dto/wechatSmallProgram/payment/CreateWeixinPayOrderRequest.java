package com.benmei.weike.dto.wechatSmallProgram.payment;

import com.benmei.weike.dto.payment.CreateOrderRequest;

/**
 * Created by Peter on 2017/11/22.
 */
public class CreateWeixinPayOrderRequest extends CreateOrderRequest {
    private String code; // 微信小程序中获取的用户code，后台根据code向微信服务器换取用户的openId

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
