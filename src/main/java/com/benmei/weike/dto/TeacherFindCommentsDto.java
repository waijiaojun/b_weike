package com.benmei.weike.dto;

import java.util.Date;

/**
 * Created by Micoo on 2017/5/11.
 */
public class TeacherFindCommentsDto {
    private Long com_id;//评论id
    private String com_content;//评价内容
    private String com_reply_content;//老师回复内容
    private Date com_time;//评价日期
    private Integer com_grade;//学生评分
    private String memb_name;//学生名称
    private String memb_head_portrait;//学生头像url

    private Integer isWekeType = 1;//是否是微课评价类型

    public Long getCom_id() {
        return com_id;
    }

    public void setCom_id(Long com_id) {
        this.com_id = com_id;
    }

    public String getCom_content() {
        return com_content;
    }

    public void setCom_content(String com_content) {
        this.com_content = com_content;
    }

    public String getCom_reply_content() {
        return com_reply_content;
    }

    public void setCom_reply_content(String com_reply_content) {
        this.com_reply_content = com_reply_content;
    }

    public Date getCom_time() {
        return com_time;
    }

    public void setCom_time(Date com_time) {
        this.com_time = com_time;
    }

    public Integer getCom_grade() {
        return com_grade;
    }

    public void setCom_grade(Integer com_grade) {
        this.com_grade = com_grade;
    }

    public String getMemb_name() {
        return memb_name;
    }

    public void setMemb_name(String memb_name) {
        this.memb_name = memb_name;
    }

    public String getMemb_head_portrait() {
        return memb_head_portrait;
    }

    public void setMemb_head_portrait(String memb_head_portrait) {
        this.memb_head_portrait = memb_head_portrait;
    }

    public Integer getIsWekeType() {
        return isWekeType;
    }

    public void setIsWekeType(Integer isWekeType) {
        this.isWekeType = isWekeType;
    }
}
