package com.benmei.weike.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by Micoo on 2017/5/5.
 */
public class StudentFindWeikeListForTeacherDto{
    private Long id;//                   weike id
    private String title;//                varchar(128) comment '标题',
    private String cover_url;//            varchar(512) comment '封面图片',

    private Integer course_count;   //课程更新节数
    @JsonIgnore
    private String title_student;//                varchar(128) comment '标题',
    @JsonIgnore
    private String cover_url_student;//            varchar(512) comment '封面图片',

    private Integer buy_number;//int comment '购买人数',//界面上和人数相关的都用此字段：学习人数，收听人数，购买人数等
    private Long duration;//持续时间

    public String getTitle_student() {
        return title_student;
    }

    public void setTitle_student(String title_student) {
        this.title_student = title_student;
    }

    public String getCover_url_student() {
        return cover_url_student;
    }

    public void setCover_url_student(String cover_url_student) {
        this.cover_url_student = cover_url_student;
    }

    public StudentFindWeikeListForTeacherDto() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        if(StringUtils.isNotBlank(this.title_student)){
            return this.title_student;
        }
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCover_url() {
        if(StringUtils.isNotBlank(this.cover_url_student)){
            return this.cover_url_student;
        }
        return cover_url;
    }

    public void setCover_url(String cover_url) {
        this.cover_url = cover_url;
    }

    public Integer getBuy_number() {
        return buy_number;
    }

    public void setBuy_number(Integer buy_number) {
        this.buy_number = buy_number;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Integer getCourse_count() {
        return course_count;
    }

    public void setCourse_count(Integer course_count) {
        this.course_count = course_count;
    }
}
