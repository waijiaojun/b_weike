package com.benmei.weike.dto;

import com.benmei.weike.entity.WeikeSubscription;

/**
 * Created by Peter on 2017/9/26.
 */
public class MyWeikePageResponse extends PageResponse {
    private WeikeSubscription weikeSubscription;

    public MyWeikePageResponse(Long pageSize, Long currentPage) {
        super(pageSize, currentPage);
    }


    public WeikeSubscription getWeikeSubscription() {
        return weikeSubscription;
    }

    public void setWeikeSubscription(WeikeSubscription weikeSubscription) {
        this.weikeSubscription = weikeSubscription;
    }
}
