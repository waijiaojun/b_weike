package com.benmei.weike.dto.weikeDetailResponse;

import com.benmei.weike.dto.TeacherDto;
import com.benmei.weike.entity.MemberSalesPromotion;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Peter on 2017/5/3.
 */
public class WeikeDetailResponse {
    private Long id;//                   bigint not null auto_increment,
    private String title;//                varchar(128) comment '标题',
    private String intro;//                varchar(255) comment '介绍',
    private String cover_url;//            varchar(512) comment '封面图片',
    private String intro_video;// 介绍视频
    private Double score;//  float comment '综合评分',
    private Long duration;               // 该微课所有语音总计持续时长
    private Integer commented = 0;  //是否评论过,0:否，1:是
    private Integer viewer_count;           // 微课购买人数
    private Integer buy_count;         //微课购买次数
    private Integer update_course;    //更新节数
    private Integer bought = 0; //是否购买过 0未购买，1已购买
    private Integer subscribed = 0;// 是否在包月有效期内  0否，1是
    private BigDecimal price;  // 学生购买课程的价格
    private TeacherDto teacher = new TeacherDto();             // 创建微课的老师信息：姓名，头像，认证标识，tags
    private List<Part> parts = new ArrayList<>();            // 微课课表（标题，part，以及每段语音的时长）

    private String share_name; //分享标题
    private String share_content; //分享介绍
    private String share_picture; //分享封面图片url
    private String share_url; //h5页面地址

    private Integer my_score;//我的评分，学员自己对该微课的评分

    private MemberSalesPromotion memberSalesPromotion;// 促销信息

    private String subscriptionDesc;//订阅说明

    private Integer is_video; //是否视频课  1：是   0：否

    public Integer getIs_video() {
        return is_video;
    }

    public void setIs_video(Integer is_video) {
        this.is_video = is_video;
    }

    public String getSubscriptionDesc() {
        return subscriptionDesc;
    }

    public void setSubscriptionDesc(String subscriptionDesc) {
        this.subscriptionDesc = subscriptionDesc;
    }

    public MemberSalesPromotion getMemberSalesPromotion() {
        return memberSalesPromotion;
    }

    public void setMemberSalesPromotion(MemberSalesPromotion memberSalesPromotion) {
        this.memberSalesPromotion = memberSalesPromotion;
    }

    public Integer getUpdate_course() {
        return update_course;
    }

    public void setUpdate_course(Integer update_course) {
        this.update_course = update_course;
    }

    public Integer getBuy_count() {
        return buy_count;
    }

    public void setBuy_count(Integer buy_count) {
        this.buy_count = buy_count;
    }

    public String getIntro_video() {
        return intro_video;
    }

    public void setIntro_video(String intro_video) {
        this.intro_video = intro_video;
    }

    public Integer getSubscribed() {
        return subscribed;
    }

    public void setSubscribed(Integer subscribed) {
        this.subscribed = subscribed;
    }

    public Integer getMy_score() {
        return my_score;
    }

    public void setMy_score(Integer my_score) {
        this.my_score = my_score;
    }

    public String getShare_name() {
        if (StringUtils.isBlank(this.share_name)) {
            this.share_name = title;
        }
        return share_name;
    }

    public void setShare_name(String share_name) {
        this.share_name = share_name;
    }

    public String getShare_content() {
        if (StringUtils.isBlank(this.share_content)) {
            this.share_content = this.intro;
        }
        return share_content;
    }

    public void setShare_content(String share_content) {
        this.share_content = share_content;
    }

    public String getShare_picture() {
        if (StringUtils.isBlank(this.share_picture)) {
            this.share_picture = this.cover_url;
        }
        return share_picture;
    }

    public void setShare_picture(String share_picture) {
        this.share_picture = share_picture;
    }

    public String getShare_url() {
        return share_url;
    }

    public void setShare_url(String share_url) {
        this.share_url = share_url;
    }

    public Integer getCommented() {
        return commented;
    }

    public void setCommented(Integer commented) {
        this.commented = commented;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getCover_url() {
        return cover_url;
    }

    public void setCover_url(String cover_url) {
        this.cover_url = cover_url;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Integer getViewer_count() {
        return viewer_count;
    }

    public void setViewer_count(Integer viewer_count) {
        this.viewer_count = viewer_count;
    }

    public TeacherDto getTeacher() {
        return teacher;
    }

    public void setTeacher(TeacherDto teacher) {
        this.teacher = teacher;
    }

    public List<Part> getParts() {
        return parts;
    }

    public void setParts(List<Part> parts) {
        this.parts = parts;
    }

    public Integer getBought() {
        return bought;
    }

    public void setBought(Integer bought) {
        this.bought = bought;
    }
}

