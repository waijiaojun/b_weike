package com.benmei.weike.dto.weikeDetailResponse;

import java.util.Date;

/**
 * Created by Micoo on 2017/5/3.
 */
public class Part {
    private String id;
    private String title;
    private Long duration;               // 该段落语音总计持续时长
    private Date update_date;           //更新时间

    private Integer isStudy;//是否学习；1已学习，0未学习


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getIsStudy() {
        return isStudy == null ? 0 : isStudy;
    }

    public void setIsStudy(Integer isStudy) {
        this.isStudy = isStudy;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getDuration() {
        return duration == null ? 0 : duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    //格式化秒为 00:00的格式
    public String getDurationFromat() {
        String minute = "00:";//分钟
        String second = "00";//秒
        if (duration != null) {
            minute = duration / 60 + ":";
            second = duration % 60 + "";
        }
        return minute + second;
    }
}
