package com.benmei.weike.dto;

/**
 * Created by waijiaojun on 2017/10/10.
 */
public class AddWeikePartTextDto {
    private String weike_id;
    private String weike_part_sort;
    private String content;

    public String getWeike_id() {
        return weike_id;
    }

    public void setWeike_id(String weike_id) {
        this.weike_id = weike_id;
    }

    public String getWeike_part_sort() {
        return weike_part_sort;
    }

    public void setWeike_part_sort(String weike_part_sort) {
        this.weike_part_sort = weike_part_sort;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
