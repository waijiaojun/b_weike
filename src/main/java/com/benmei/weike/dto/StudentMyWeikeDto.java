package com.benmei.weike.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by Micoo on 2017/5/11.
 */
public class StudentMyWeikeDto {
    private Long id;//微课id
    private String title;//微课标题
    private String cover_url;//微课封面图片

    @JsonIgnore
    private String title_student;//微课标题
    @JsonIgnore
    private String cover_url_student;//微课封面图片

    private String teacher_name;//老师名称
    private String teacher_avatar_url;//老师头像url
    private Integer commented;//是否评论过：0否，1已评论
    private String is_video;//是否是视频课  0：否  1：是

    public String getIs_video() {
        return is_video;
    }

    public void setIs_video(String is_video) {
        this.is_video = is_video;
    }

    public Integer getCommented() {
        return commented;
    }

    public void setCommented(Integer commented) {
        this.commented = commented;
    }

    public String getTitle_student() {
        return title_student;
    }

    public void setTitle_student(String title_student) {
        this.title_student = title_student;
    }

    public String getCover_url_student() {
        return cover_url_student;
    }

    public void setCover_url_student(String cover_url_student) {
        this.cover_url_student = cover_url_student;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        if(StringUtils.isNotBlank(title_student)){
            return title_student;
        }
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCover_url() {
        if(StringUtils.isNotBlank(cover_url_student)){
            return cover_url_student;
        }
        return cover_url;
    }

    public void setCover_url(String cover_url) {
        this.cover_url = cover_url;
    }

    public String getTeacher_name() {
        return teacher_name;
    }

    public void setTeacher_name(String teacher_name) {
        this.teacher_name = teacher_name;
    }

    public String getTeacher_avatar_url() {
        return teacher_avatar_url;
    }

    public void setTeacher_avatar_url(String teacher_avatar_url) {
        this.teacher_avatar_url = teacher_avatar_url;
    }
}
