package com.benmei.weike.dto;

import com.benmei.weike.common.Constants;

import java.sql.Timestamp;

/**
 * Created by Micoo on 2017/5/4.
 */
public class WeikeCommentsDto {
    private String memb_name;
    private String memb_head_portrait;
    private String com_content;
    private Timestamp com_time;
    private Integer com_grade;//学生评分
    private String com_reply_content;//老师回复，如果是null或者空字符，则表示没有回复

    public String getMemb_name() {
        return memb_name;
    }

    public void setMemb_name(String memb_name) {
        this.memb_name = memb_name;
    }

    public String getMemb_head_portrait() {
        if (memb_head_portrait != null && !memb_head_portrait.startsWith("http://") && !memb_head_portrait.startsWith("https://")) {
            memb_head_portrait = Constants.Oss_Url_Prefix +"/" + memb_head_portrait;
        }
        return memb_head_portrait;
    }

    public void setMemb_head_portrait(String memb_head_portrait) {
        this.memb_head_portrait = memb_head_portrait;
    }

    public String getCom_content() {
        return com_content;
    }

    public void setCom_content(String com_content) {
        this.com_content = com_content;
    }

    public Timestamp getCom_time() {
        return com_time;
    }

    public void setCom_time(Timestamp com_time) {
        this.com_time = com_time;
    }

    public Integer getCom_grade() {
        return com_grade;
    }

    public void setCom_grade(Integer com_grade) {
        this.com_grade = com_grade;
    }

    public String getCom_reply_content() {
        return com_reply_content;
    }

    public void setCom_reply_content(String com_reply_content) {
        this.com_reply_content = com_reply_content;
    }


}
