package com.benmei.weike.dto;

import com.benmei.weike.common.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Micoo on 2017/5/3.
 */
public class TeacherDto {
    private Integer teacher_id;
    private String teacher_name;
    private String teacher_avatar_url;
    private Integer teacher_valid = 0;
    private List<String> labels = new ArrayList<String>();

    public String getTeacher_name() {
        return teacher_name;
    }

    public void setTeacher_name(String teacher_name) {
        this.teacher_name = teacher_name;
    }

    public String getTeacher_avatar_url() {
        if (teacher_avatar_url != null && !teacher_avatar_url.startsWith("http://") && !teacher_avatar_url.startsWith("https://")) {
            teacher_avatar_url = Constants.Oss_Url_Prefix +"/" + teacher_avatar_url;
        }
        return teacher_avatar_url;
    }

    public void setTeacher_avatar_url(String teacher_avatar_url) {
        this.teacher_avatar_url = teacher_avatar_url;
    }

    public Integer getTeacher_valid() {
        return teacher_valid;
    }

    public void setTeacher_valid(Integer teacher_valid) {
        this.teacher_valid = teacher_valid;
    }

    public List<String> getLabels() {
        return labels;
    }

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    public Integer getTeacher_id() {
        return teacher_id;
    }

    public void setTeacher_id(Integer teacher_id) {
        this.teacher_id = teacher_id;
    }
}
