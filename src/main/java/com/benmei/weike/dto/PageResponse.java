package com.benmei.weike.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Micoo on 2017/4/18.
 */
public class PageResponse<T> {
    @JsonIgnore
    private Long currentPage;//当前页
    @JsonIgnore
    private Long pageSize;//每页大小
    @JsonIgnore
    private Long total;//总记录数

    private Long totalPage = 0L;

    private List<T> data;

    public PageResponse() {
    }

    public PageResponse(Long pageSize, Long currentPage) {
        this.pageSize = pageSize;
        this.currentPage = currentPage;
    }

    @JsonIgnore
    public Long getOffset() {
        if (currentPage <= 0) {
            return 0L;
        }
        return (currentPage - 1) * pageSize;
    }

    public void setTotalPage(Long totalPage) {
        this.totalPage = totalPage;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        if (data == null) {
            this.data = new ArrayList<>();
        } else {

            this.data = data;
        }
    }

    /**
     * 获取总页数
     *
     * @return
     */
    public Long getTotalPage() {
        if (total == 0) {
            return totalPage;
        }
        if (pageSize == 0) {
            return totalPage;
        }

        if (total % pageSize > 0) {
            totalPage = total / pageSize + 1;
        } else {
            totalPage = total / pageSize;
        }

        return totalPage;
    }

    public Long getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Long currentPage) {
        this.currentPage = currentPage;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}
