package com.benmei.weike.dto.IntoClassRoom;

import com.benmei.weike.entity.ClassRoom;
import com.benmei.weike.entity.Teacher;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by Peter on 2017/9/8.
 */
public class IntoClassRoomTeacher {
    private Integer teacherId;
    private String avatarUrl;
    private String name;
    private Integer isInRoom;

    public Integer getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIsInRoom() {
        return isInRoom;
    }

    public void setIsInRoom(Integer isInRoom) {
        this.isInRoom = isInRoom;
    }

    @JsonIgnore
    public static IntoClassRoomTeacher fromEntity(Teacher teacher, ClassRoom room) {
        IntoClassRoomTeacher o = new IntoClassRoomTeacher();
        o.setAvatarUrl(teacher.getTea_head_portrait());
        o.setName(teacher.getTea_name());
        o.setTeacherId(teacher.getTea_id());
        o.setIsInRoom(room.getTea_is_in_room());
        return o;
    }
}
