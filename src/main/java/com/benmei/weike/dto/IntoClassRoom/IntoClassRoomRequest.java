package com.benmei.weike.dto.IntoClassRoom;

/**
 * Created by Peter on 2017/9/8.
 */
public class IntoClassRoomRequest {
    private Integer roomId;

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }
}
