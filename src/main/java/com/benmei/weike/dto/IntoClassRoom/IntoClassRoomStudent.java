package com.benmei.weike.dto.IntoClassRoom;

import com.benmei.weike.entity.ClassRoom;
import com.benmei.weike.entity.Student;
import com.benmei.weike.entity.Teacher;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nativetalk.bean.member.TdNtMember;

/**
 * Created by Peter on 2017/9/8.
 */
public class IntoClassRoomStudent {

    private Integer studentId;
    private String avatarUrl;
    private String name;
    private Integer isInRoom;

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIsInRoom() {
        return isInRoom;
    }

    public void setIsInRoom(Integer isInRoom) {
        this.isInRoom = isInRoom;
    }

    @JsonIgnore
    public static IntoClassRoomStudent fromEntity(Student student, ClassRoom room) {
        IntoClassRoomStudent o = new IntoClassRoomStudent();
        o.setAvatarUrl(student.getMemb_head_portrait());
        o.setName(student.getMemb_name());
        o.setStudentId(student.getMemb_id());
        o.setIsInRoom(room.getMemb_is_in_room());
        return o;
    }
}
