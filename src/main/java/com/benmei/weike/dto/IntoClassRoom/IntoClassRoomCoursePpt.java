package com.benmei.weike.dto.IntoClassRoom;

/**
 * Created by Peter on 2017/9/8.
 */
public class IntoClassRoomCoursePpt {
    private String pictureUrl;
    private String pictureSort;

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getPictureSort() {
        return pictureSort;
    }

    public void setPictureSort(String pictureSort) {
        this.pictureSort = pictureSort;
    }
}
