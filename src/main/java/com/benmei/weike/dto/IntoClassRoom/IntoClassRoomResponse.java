package com.benmei.weike.dto.IntoClassRoom;

/**
 * Created by Peter on 2017/9/8.
 */
public class IntoClassRoomResponse {
    private IntoClassRoom room;
    private IntoClassRoomTeacher teacher;
    private IntoClassRoomStudent student;
    private IntoClassRoomCourseInfo courseInfo;


    public IntoClassRoom getRoom() {
        return room;
    }

    public void setRoom(IntoClassRoom room) {
        this.room = room;
    }

    public IntoClassRoomTeacher getTeacher() {
        return teacher;
    }

    public void setTeacher(IntoClassRoomTeacher teacher) {
        this.teacher = teacher;
    }

    public IntoClassRoomStudent getStudent() {
        return student;
    }

    public void setStudent(IntoClassRoomStudent student) {
        this.student = student;
    }


    public IntoClassRoomCourseInfo getCourseInfo() {
        return courseInfo;
    }

    public void setCourseInfo(IntoClassRoomCourseInfo courseInfo) {
        this.courseInfo = courseInfo;
    }

}
