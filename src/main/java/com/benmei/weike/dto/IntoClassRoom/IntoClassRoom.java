package com.benmei.weike.dto.IntoClassRoom;

import com.benmei.weike.entity.ClassRoom;
import com.benmei.weike.exception.ServerException;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;

/**
 * Created by Peter on 2017/9/11.
 */
public class IntoClassRoom {
    private Integer roomId;
    private Long roomRemainOpenSecond;//房间剩余开放时间，单位：秒

    @JsonIgnore
    private Date room_close_date;

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public Long getRoomRemainOpenSecond() {
        long endSecond = this.getRoom_close_date().getTime() / 1000;
        long nowSecond = System.currentTimeMillis() / 1000;
        this.roomRemainOpenSecond = endSecond - nowSecond;
        return roomRemainOpenSecond;
    }

    public Date getRoom_close_date() {
        return room_close_date;
    }

    public void setRoom_close_date(Date room_close_date) {
        this.room_close_date = room_close_date;
    }

    public static IntoClassRoom fromEntity(ClassRoom room) {
        IntoClassRoom classRoom = new IntoClassRoom();
        classRoom.setRoomId(room.getRoom_id());
        classRoom.setRoom_close_date(room.getRoom_close_date());
        if (room.getRoom_close_date() == null) {
            throw new ServerException("没有设置房间关闭时间");
        }
        return classRoom;
    }
}
