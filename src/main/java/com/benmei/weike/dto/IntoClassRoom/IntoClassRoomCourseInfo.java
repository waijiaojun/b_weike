package com.benmei.weike.dto.IntoClassRoom;

import com.benmei.weike.entity.CourseChapter;
import com.benmei.weike.entity.CourseInfo;

import java.util.List;

/**
 * Created by Peter on 2017/9/8.
 */
public class IntoClassRoomCourseInfo {
    private Integer courseId;
    private String courseName;
    private Integer lastPage;// 上次上课学到哪一个ppt，进入房间后继续学习
    private List<IntoClassRoomCoursePpt> ppts;
    private List<CourseChapter> courseChapterList;//课程的章节，目录

    public List<CourseChapter> getCourseChapterList() {
        return courseChapterList;
    }

    public void setCourseChapterList(List<CourseChapter> courseChapterList) {
        this.courseChapterList = courseChapterList;
    }

    public Integer getLastPage() {
        return lastPage;
    }

    public void setLastPage(Integer lastPage) {
        this.lastPage = lastPage;
    }

    public List<IntoClassRoomCoursePpt> getPpts() {
        return ppts;
    }

    public void setPpts(List<IntoClassRoomCoursePpt> ppts) {
        this.ppts = ppts;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public static IntoClassRoomCourseInfo fromEntity(CourseInfo courseInfo, List<IntoClassRoomCoursePpt> pictures) {
        IntoClassRoomCourseInfo intoClassRoomCourseInfo = new IntoClassRoomCourseInfo();
        intoClassRoomCourseInfo.setCourseId(courseInfo.getCou_id());
        intoClassRoomCourseInfo.setCourseName(courseInfo.getCou_name());
        intoClassRoomCourseInfo.setPpts(pictures);
        return intoClassRoomCourseInfo;
    }
}
