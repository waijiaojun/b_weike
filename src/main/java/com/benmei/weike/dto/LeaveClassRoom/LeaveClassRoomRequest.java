package com.benmei.weike.dto.LeaveClassRoom;

/**
 * Created by Peter on 2017/9/8.
 */
public class LeaveClassRoomRequest {
    private Integer roomId;
    private String reason;//离开教室的原因：正常（用户返回其他页面），闪退，到时间。

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
