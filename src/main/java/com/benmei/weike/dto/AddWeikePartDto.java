package com.benmei.weike.dto;

/**
 * Created by waijiaojun on 2017/9/20.
 */
public class AddWeikePartDto {
    private String weike_id;
    private String title;

    public String getWeike_id() {
        return weike_id;
    }

    public void setWeike_id(String weike_id) {
        this.weike_id = weike_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
