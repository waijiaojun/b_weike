package com.benmei.weike.dto;

/**
 * Created by Micoo on 2017/5/5.
 */
public class StudentFindAllCoursePageRequest extends PageRequest {
    private Integer category_id;//分类id,0、Null会查询所有分类

    public Integer getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Integer category_id) {
        this.category_id = category_id;
    }
}
