package com.benmei.weike.dto;

import java.util.Date;
import java.util.List;

/**
 * Created by Micoo on 2017/5/5.
 */
public class AllCourseDto {
    private Long id; //根据类型的不同，分别对应不同的课程id(不同的表的id)
    private Integer type; //1:course,2:live,3:wieke
    private String title;
    private Date create_date;

    private Integer teacher_id;
    private String teacher_name;        //老师名字
    private String teacher_avatar_url;  //老师头像
    private Integer teacher_valid;      //老师是否是VIP,0:否，1:是
    private List<String> labels;         //老师的标签


    static class Type{
        public static final Integer course = 1;
        public static final Integer live = 2;
        public static final Integer wieke = 3;
    }
}
