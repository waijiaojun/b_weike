package com.benmei.weike.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by Micoo on 2017/5/3.
 */
public class WeikeBoutiqueListDto {
    private Long id;
    private String teacher_name;
    private String teacher_avatar_url;
    private String title;   //                varchar(128) comment '标题',
    private String cover_url;   //            varchar(512) comment '封面图片',

    @JsonIgnore
    private String title_student;   //                varchar(128) comment '标题',
    @JsonIgnore
    private String cover_url_student;   //

    private Integer viewer_count = 0;           // 微课购买人数,收听人数
    private Double student_price = 0.0;//      float default 0 comment '学生购买价格',

    private Integer category_id;
    private String category_name;
    private Double score;
    private Integer is_lock = 1; //锁定状态   0：解锁   1：锁定

    public Integer getIs_lock() {
        return is_lock;
    }

    public void setIs_lock(Integer is_lock) {
        this.is_lock = is_lock;
    }

    public Integer getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Integer category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getTitle_student() {
        return title_student;
    }

    public void setTitle_student(String title_student) {
        this.title_student = title_student;
    }

    public String getCover_url_student() {
        return cover_url_student;
    }

    public void setCover_url_student(String cover_url_student) {
        this.cover_url_student = cover_url_student;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTeacher_name() {
        return teacher_name;
    }

    public void setTeacher_name(String teacher_name) {
        this.teacher_name = teacher_name;
    }

    public String getTeacher_avatar_url() {
        return teacher_avatar_url;
    }

    public void setTeacher_avatar_url(String teacher_avatar_url) {
        this.teacher_avatar_url = teacher_avatar_url;
    }

    public String getTitle() {
        if(StringUtils.isNotBlank(title_student)){
            return title_student;
        }
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCover_url() {
        if(StringUtils.isNotBlank(cover_url_student)){
            return cover_url_student;
        }
        return cover_url;
    }

    public void setCover_url(String cover_url) {
        this.cover_url = cover_url;
    }

    public Integer getViewer_count() {
        return viewer_count;
    }

    public void setViewer_count(Integer viewer_count) {
        this.viewer_count = viewer_count;
    }

    public Double getStudent_price() {
        return student_price;
    }

    public void setStudent_price(Double student_price) {
        this.student_price = student_price;
    }
}
