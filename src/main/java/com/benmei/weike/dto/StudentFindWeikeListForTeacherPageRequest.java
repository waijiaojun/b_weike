package com.benmei.weike.dto;

/**
 * Created by Micoo on 2017/5/5.
 */
public class StudentFindWeikeListForTeacherPageRequest extends PageRequest {
    private Integer tea_id;//老师id

    public Integer getTea_id() {
        return tea_id;
    }

    public void setTea_id(Integer tea_id) {
        this.tea_id = tea_id;
    }
}
