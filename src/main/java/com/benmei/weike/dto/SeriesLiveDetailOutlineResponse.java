package com.benmei.weike.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;

/**
 * Created by Peter on 2017/8/30.
 */
public class SeriesLiveDetailOutlineResponse {
    private Long seriesLiveCourseInstanceId;
    private String title;
    private String duration;//持续时间，单位：分钟
    private Date startTime;
    private Integer status;// 1 正在上课，3 未开始，4 已结束

    // 优先显示中文标题，如果没有中文标题就显示英文标题
    public String getTitle() {
        if (StringUtils.isNotBlank(this.title_cn)) {
            this.title = this.title_cn;
        } else {
            this.title = this.title_en;
        }
        return title;
    }

    @JsonIgnore
    private String title_en;
    @JsonIgnore
    private String title_cn;

    public String getTitle_en() {
        return title_en;
    }

    public void setTitle_en(String title_en) {
        this.title_en = title_en;
    }

    public String getTitle_cn() {
        return title_cn;
    }

    public void setTitle_cn(String title_cn) {
        this.title_cn = title_cn;
    }

    public Long getSeriesLiveCourseInstanceId() {
        return seriesLiveCourseInstanceId;
    }

    public void setSeriesLiveCourseInstanceId(Long seriesLiveCourseInstanceId) {
        this.seriesLiveCourseInstanceId = seriesLiveCourseInstanceId;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
