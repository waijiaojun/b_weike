package com.benmei.weike.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by Micoo on 2017/4/18.
 */
public class TeacherWeikeListRequest {

    public static class Status{
        /**
         * 已发布
         */
        public static final Integer page1 = 1;
        /**
         * 2 审核通过(包含已发布和未发布)
         */
        public static final Integer page2 = 2;
        /**
         * 3 审核未通过和未审核
         */
        public static final Integer page3 = 3;
    }

    private Long currentPage = 1L;
    private Long pageSize = 10L;
    private Integer status;// 1 已发布； 2 审核通过(包含已发布和未发布)；3 审核未通过和未审核

    public Long getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Long currentPage) {
        this.currentPage = currentPage;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
