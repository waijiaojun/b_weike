package com.benmei.weike.dto;

/**
 * Created by Peter on 2017/9/22.
 */
public class CloseSalesPromotionRequest {
    private Integer salesPromotionId;

    public Integer getSalesPromotionId() {
        return salesPromotionId;
    }

    public void setSalesPromotionId(Integer salesPromotionId) {
        this.salesPromotionId = salesPromotionId;
    }
}
