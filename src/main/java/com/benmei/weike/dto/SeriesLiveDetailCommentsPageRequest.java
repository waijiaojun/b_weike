package com.benmei.weike.dto;

/**
 * Created by Peter on 2017/8/30.
 */
public class SeriesLiveDetailCommentsPageRequest extends PageRequest{
    private Integer seriesLiveInstanceId;

    public Integer getSeriesLiveInstanceId() {
        return seriesLiveInstanceId;
    }

    public void setSeriesLiveInstanceId(Integer seriesLiveInstanceId) {
        this.seriesLiveInstanceId = seriesLiveInstanceId;
    }
}
