package com.benmei.weike.dto;

import org.apache.commons.lang3.StringUtils;

import javax.persistence.Transient;

/**
 * Created by Micoo on 2017/5/10.
 */
public class WeikeDto {
    private Integer id;//  bigint(20) NOT NULL AUTO_INCREMENT ,
    private String title;//  课程标题，直播课名称，微课标题     varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
    private String intro;
    private String cover_url;//课程封面图片URL

    private String title_student;//  课程标题，直播课名称，微课标题     varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
    private String intro_student;
    private String cover_url_student;//课程封面图片URL
    private Double student_price;// 价格
    private Integer viewer_count;//  收听人数 int(9) NULL DEFAULT NULL COMMENT '收听人数' ,
    private Integer update_lesson_count; //更新节数

    public Integer getUpdate_lesson_count() {
        return update_lesson_count;
    }

    public void setUpdate_lesson_count(Integer update_lesson_count) {
        this.update_lesson_count = update_lesson_count;
    }

    private Integer category_id; //分类id
    private String category_name; //分类名称
    private Double score;//评分

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Integer getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Integer category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    @Transient
    private String teacher_name;
    @Transient
    private String teacher_avatar_url;
    @Transient
    private Integer teacher_valid;////是否验证，0:未验证，1:已验证

    ////标签 0无 1new 2V
    private Integer tea_label;

    public Integer getTea_label() {
        return tea_label;
    }

    public void setTea_label(Integer tea_label) {
        this.tea_label = tea_label;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getCover_url() {
        return cover_url;
    }

    public void setCover_url(String cover_url) {
        this.cover_url = cover_url;
    }

    public Integer getViewer_count() {
        return viewer_count;
    }

    public void setViewer_count(Integer viewer_count) {
        this.viewer_count = viewer_count;
    }

    public Double getStudent_price() {
        return student_price;
    }

    public void setStudent_price(Double student_price) {
        this.student_price = student_price;
    }

    public String getTeacher_name() {
        return teacher_name;
    }

    public void setTeacher_name(String teacher_name) {
        this.teacher_name = teacher_name;
    }

    public String getTeacher_avatar_url() {
        return teacher_avatar_url;
    }

    public void setTeacher_avatar_url(String teacher_avatar_url) {
        this.teacher_avatar_url = teacher_avatar_url;
    }

    public Integer getTeacher_valid() {
        if(tea_label.intValue() == 0){
            teacher_valid = 0;
        }
        if(tea_label.intValue() == 1){
            teacher_valid = 0;
        }
        if(tea_label.intValue() == 2){
            teacher_valid = 1;
        }
        return teacher_valid;
    }

    public void setTeacher_valid(Integer teacher_valid) {
        this.teacher_valid = teacher_valid;
    }

    public String getTitle_student() {
        if (StringUtils.isBlank(title_student)) {
            this.title_student = title;
        }
        return title_student;
    }

    public void setTitle_student(String title_student) {
        this.title_student = title_student;
    }

    public String getIntro_student() {
        if (StringUtils.isBlank(intro_student)) {
            this.intro_student = intro;
        }
        return intro_student;
    }

    public void setIntro_student(String intro_student) {
        this.intro_student = intro_student;
    }

    public String getCover_url_student() {
        if (StringUtils.isBlank(cover_url_student)) {
            this.cover_url_student = cover_url;
        }
        return cover_url_student;
    }

    public void setCover_url_student(String cover_url_student) {
        this.cover_url_student = cover_url_student;
    }
}
