package com.benmei.weike.dto;

import com.benmei.weike.util.DateUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Peter on 2017/8/29.
 */
public class SeriesLiveIndexResponseDto {
    private Long id;
    private String title;
    private String cover;
    private Integer status;// 1 正在上课，2 学期已开始，3 学期未开始，4 学期已结束
    private Integer planSaleNumber;// 限量
    private Integer actualSaleNumber;// 实际销售数量
    private Integer planSaleDurationDays;//持续销售几天，限时几天，如果是null表示不限时
    private Integer isLive;  //类型：0录播，1直播
    private Integer seriesLiveInstanceId;
    private Double price;// 价格：单位元
    private Date startTime;// 课程开始时间
    private Date endTime;// 课程结束时间
    private List<SeriesLiveIndexTeacher> teacherList = new ArrayList<>();// 讲课老师列表

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List getTeacherList() {
        return teacherList;
    }

    public void setTeacherList(List teacherList) {
        this.teacherList = teacherList;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    @JsonIgnore
    private String title_en;
    @JsonIgnore
    private String title_cn;
    @JsonIgnore
    private Date sale_start_time;
    @JsonIgnore
    private Date sale_end_time;

    // 优先显示中文标题，如果没有中文标题就显示英文标题
    public String getTitle() {
        if (StringUtils.isNotBlank(this.title_cn)) {
            this.title = this.title_cn;
        } else {
            this.title = this.title_en;
        }
        return title;
    }

    //销售开始时间和销售结束时间相差的天数
    public Integer getPlanSaleDurationDays() {
        if (sale_start_time == null || sale_end_time == null) {
            return null;
        }
        return DateUtil.getDurationDays(this.sale_start_time, this.sale_end_time);
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPlanSaleNumber() {
        return planSaleNumber;
    }

    public void setPlanSaleNumber(Integer planSaleNumber) {
        this.planSaleNumber = planSaleNumber;
    }

    public Integer getActualSaleNumber() {
        return actualSaleNumber;
    }

    public void setActualSaleNumber(Integer actualSaleNumber) {
        this.actualSaleNumber = actualSaleNumber;
    }

    public Integer getIsLive() {
        return isLive;
    }

    public void setIsLive(Integer isLive) {
        this.isLive = isLive;
    }

    public Integer getSeriesLiveInstanceId() {
        return seriesLiveInstanceId;
    }

    public void setSeriesLiveInstanceId(Integer seriesLiveInstanceId) {
        this.seriesLiveInstanceId = seriesLiveInstanceId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getTitle_en() {
        return title_en;
    }

    public void setTitle_en(String title_en) {
        this.title_en = title_en;
    }

    public String getTitle_cn() {
        return title_cn;
    }

    public void setTitle_cn(String title_cn) {
        this.title_cn = title_cn;
    }

    public Date getSale_start_time() {
        return sale_start_time;
    }

    public void setSale_start_time(Date sale_start_time) {
        this.sale_start_time = sale_start_time;
    }

    public Date getSale_end_time() {
        return sale_end_time;
    }

    public void setSale_end_time(Date sale_end_time) {
        this.sale_end_time = sale_end_time;
    }
}
