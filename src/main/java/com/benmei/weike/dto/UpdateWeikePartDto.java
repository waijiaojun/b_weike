package com.benmei.weike.dto;

/**
 * Created by waijiaojun on 2017/9/20.
 */
public class UpdateWeikePartDto {
    private String id;
    private String title;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
