package com.benmei.weike.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by Peter on 2017/8/29.
 */
public class SeriesLiveIndexTeacher {
    private Integer teaId;
    private String teaName;
    private String avatar;

    @JsonIgnore
    private Integer seriesLiveInstanceId;

    public Integer getSeriesLiveInstanceId() {
        return seriesLiveInstanceId;
    }

    public void setSeriesLiveInstanceId(Integer seriesLiveInstanceId) {
        this.seriesLiveInstanceId = seriesLiveInstanceId;
    }

    public Integer getTeaId() {
        return teaId;
    }

    public void setTeaId(Integer teaId) {
        this.teaId = teaId;
    }

    public String getTeaName() {
        return teaName;
    }

    public void setTeaName(String teaName) {
        this.teaName = teaName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
