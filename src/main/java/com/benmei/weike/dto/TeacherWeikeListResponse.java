package com.benmei.weike.dto;

/**
 * Created by Micoo on 2017/5/3.
 */
public class TeacherWeikeListResponse extends PageResponse {
    private String shipin = "/nativetalk/course-intro-video/Coming-Soon.mp4";
    private String weikeListHelpUrl =       "http://app.waijiaojun.com/article/detail?arti_id=28";
    private String weikeContentEditHelpUrl ="http://app.waijiaojun.com/article/detail?arti_id=27";

    public String getWeikeListHelpUrl() {
        return weikeListHelpUrl;
    }

    public void setWeikeListHelpUrl(String weikeListHelpUrl) {
        this.weikeListHelpUrl = weikeListHelpUrl;
    }

    public String getWeikeContentEditHelpUrl() {
        return weikeContentEditHelpUrl;
    }

    public void setWeikeContentEditHelpUrl(String weikeContentEditHelpUrl) {
        this.weikeContentEditHelpUrl = weikeContentEditHelpUrl;
    }

    public String getShipin() {
        return shipin;
    }

    public void setShipin(String shipin) {
        this.shipin = shipin;
    }

    public TeacherWeikeListResponse(Long pageSize, Long currentPage) {
        super(pageSize, currentPage);
    }
}
