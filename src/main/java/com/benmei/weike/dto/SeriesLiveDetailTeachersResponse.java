package com.benmei.weike.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Peter on 2017/8/30.
 */
public class SeriesLiveDetailTeachersResponse {
    private Integer teaId;

    private String introImageUrl;

    public Integer getTeaId() {
        return teaId;
    }

    public void setTeaId(Integer teaId) {
        this.teaId = teaId;
    }

    public String getIntroImageUrl() {
        return introImageUrl;
    }

    public void setIntroImageUrl(String introImageUrl) {
        this.introImageUrl = introImageUrl;
    }
}
