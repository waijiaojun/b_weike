package com.benmei.weike.dto.payment;

import java.math.BigDecimal;

/**
 * Created by Peter on 2017/9/18.
 */
public class CreateOrderRequest {
    private String goodsCategoryCode;//商品类型 1v1Course,weike
    private String payType;//支付类型：wallet 余额支付； multiple 组合支付(余额 + P++)； thirdpart P++支付
    private String channel;//支付渠道 wx,alipay,paypal
    private Integer goodsId;//商品id
    private BigDecimal money;//支付金额
    private String paypalId;//paypal id

    public String getPaypalId() {
        return paypalId;
    }

    public void setPaypalId(String paypalId) {
        this.paypalId = paypalId;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getGoodsCategoryCode() {
        return goodsCategoryCode;
    }

    public void setGoodsCategoryCode(String goodsCategoryCode) {
        this.goodsCategoryCode = goodsCategoryCode;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }
}
