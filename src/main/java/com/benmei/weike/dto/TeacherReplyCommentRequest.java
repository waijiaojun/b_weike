package com.benmei.weike.dto;

/**
 * Created by Micoo on 2017/5/11.
 */
public class TeacherReplyCommentRequest {
    private Long commentsId;//评论id
    private String replyContent;//回复内容

    public Long getCommentsId() {
        return commentsId;
    }

    public void setCommentsId(Long commentsId) {
        this.commentsId = commentsId;
    }

    public String getReplyContent() {
        return replyContent;
    }

    public void setReplyContent(String replyContent) {
        this.replyContent = replyContent;
    }
}
