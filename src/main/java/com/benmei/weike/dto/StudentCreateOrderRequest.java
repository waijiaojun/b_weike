package com.benmei.weike.dto;

/**
 * Created by Micoo on 2017/5/9.
 */
public class StudentCreateOrderRequest {
    private Long weike_id;//支付金额
    private String channel;//支付渠道

    public Long getWeike_id() {
        return weike_id;
    }

    public void setWeike_id(Long weike_id) {
        this.weike_id = weike_id;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
