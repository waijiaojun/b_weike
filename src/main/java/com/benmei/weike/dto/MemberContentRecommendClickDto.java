package com.benmei.weike.dto;

/**
 * Created by Peter on 2017/6/12.
 */
public class MemberContentRecommendClickDto {
    private Integer recommend_id;

    public Integer getRecommend_id() {
        return recommend_id;
    }

    public void setRecommend_id(Integer recommend_id) {
        this.recommend_id = recommend_id;
    }
}
