package com.benmei.weike.dto;

/**
 * Created by waijiaojun on 2017/10/11.
 */
public class PartImageDto {
    private String id;
    private String oldSort;
    private String sort;

    public String getOldSort() {
        return oldSort;
    }

    public void setOldSort(String oldSort) {
        this.oldSort = oldSort;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
