package com.benmei.weike.dto;

/**
 * Created by Peter on 2017/5/4.
 */
public class SaveCommentRequest {
    private Long weike_id; //微课id
    private String content; //评论内容
    private Integer score;  //评分

    public Long getWeike_id() {
        return weike_id;
    }

    public void setWeike_id(Long weike_id) {
        this.weike_id = weike_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }
}
