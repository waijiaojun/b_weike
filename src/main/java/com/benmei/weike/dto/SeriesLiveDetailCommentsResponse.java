package com.benmei.weike.dto;

import java.util.Date;

/**
 * Created by Peter on 2017/8/30.
 */
public class SeriesLiveDetailCommentsResponse {
    private Integer memberId;       // 学生id
    private String memberAvatar;    // 学生头像
    private String memberName;      // 学生姓名
    private Date commentDate;       // 评论日期
    private String content;          // 评论内容

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getMemberAvatar() {
        return memberAvatar;
    }

    public void setMemberAvatar(String memberAvatar) {
        this.memberAvatar = memberAvatar;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
