package com.benmei.weike.dto;

/**
 * Created by Peter on 2017/6/20.
 */
public class AudioPlayStatusReceiveDto {
    public Long weike_id;
    public Long weike_content_id;

    public Long getWeike_id() {
        return weike_id;
    }

    public void setWeike_id(Long weike_id) {
        this.weike_id = weike_id;
    }

    public Long getWeike_content_id() {
        return weike_content_id;
    }

    public void setWeike_content_id(Long weike_content_id) {
        this.weike_content_id = weike_content_id;
    }
}
