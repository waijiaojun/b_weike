package com.benmei.weike.dto;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Peter on 2017/8/30.
 */
public class SeriesLiveIndexPageResponse extends PageResponse {
    public SeriesLiveIndexPageResponse(Long pageSize, Long currentPage) {
        super(pageSize,currentPage);
    }


    private Map<String,Object> header = new HashMap<>();

    public Map<String, Object> getHeader() {
        return header;
    }

    public void setHeader(Map<String, Object> header) {
        this.header = header;
    }
}
