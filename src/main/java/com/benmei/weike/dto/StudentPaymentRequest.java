package com.benmei.weike.dto;

/**
 * Created by Micoo on 2017/5/8.
 */
public class StudentPaymentRequest {
    private String orderNo;//ping++的订单编号
    private String channel;//支付渠道
    private Long weike_id;//微课id
    private Integer userBlanace;//是否使用钱包中的余额支付，0：使用余额，1不使用余额支付

    public Integer getUserBlanace() {
        return userBlanace;
    }

    public void setUserBlanace(Integer userBlanace) {
        this.userBlanace = userBlanace;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Long getWeike_id() {
        return weike_id;
    }

    public void setWeike_id(Long weike_id) {
        this.weike_id = weike_id;
    }

    @Override
    public String toString() {
        return "StudentPaymentRequest{" +
                "orderNo='" + orderNo + '\'' +
                ", channel='" + channel + '\'' +
                ", weike_id=" + weike_id +
                ", userBlanace=" + userBlanace +
                '}';
    }
}
