package com.benmei.weike.service;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.benmei.weike.entity.WeikeStudentBuy;
import com.benmei.weike.dao.WeikeStudentBuyDao;

@Service
public class WeikeStudentBuyService{

    @Resource
    private WeikeStudentBuyDao weikeStudentBuyDao;

    public int insert(WeikeStudentBuy pojo){
        return weikeStudentBuyDao.insert(pojo);
    }

    public int insertSelective(WeikeStudentBuy pojo){
        return weikeStudentBuyDao.insertSelective(pojo);
    }

    public int insertList(List<WeikeStudentBuy> pojos){
        return weikeStudentBuyDao.insertList(pojos);
    }

    public int update(WeikeStudentBuy pojo){
        return weikeStudentBuyDao.update(pojo);
    }
}
