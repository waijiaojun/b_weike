package com.benmei.weike.service;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.benmei.weike.entity.Order;
import com.benmei.weike.dao.OrderDao;

@Service
public class OrderService{

    @Resource
    private OrderDao orderDao;

    public int insert(Order pojo){
        return orderDao.insert(pojo);
    }

    public int insertSelective(Order pojo){
        return orderDao.insertSelective(pojo);
    }

    public int insertList(List<Order> pojos){
        return orderDao.insertList(pojos);
    }

    public int update(Order pojo){
        return orderDao.update(pojo);
    }
}
