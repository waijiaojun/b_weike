package com.benmei.weike.service;

import com.benmei.weike.dao.WeikeCommentsDao;
import com.benmei.weike.dao.WeikeDao;
import com.benmei.weike.dto.*;
import com.benmei.weike.entity.Weike;
import com.benmei.weike.entity.WeikeComments;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class WeikeCommentsService{

    @Resource
    private WeikeCommentsDao weikeCommentsDao;

    @Resource
    private WeikeDao weikeDao;

    public int insert(WeikeComments pojo){
        return weikeCommentsDao.insert(pojo);
    }

    public int insertSelective(WeikeComments pojo){
        return weikeCommentsDao.insertSelective(pojo);
    }

    public int insertList(List<WeikeComments> pojos){
        return weikeCommentsDao.insertList(pojos);
    }

    public int update(WeikeComments pojo){
        return weikeCommentsDao.update(pojo);
    }

    public void save(SaveCommentRequest req, Integer memb_id) {
        WeikeComments comments = new WeikeComments();
        comments.setContent(req.getContent());
        comments.setScore(req.getScore());
        Date now = new Date();
        comments.setCreate_date(now);
        comments.setUpdate_date(now);
        comments.setMemb_id(memb_id);
        comments.setUpdate_user(memb_id);
        comments.setWeike_id(req.getWeike_id());
        Weike weike = weikeDao.getById(req.getWeike_id());
        comments.setTea_id(weike.getCreate_user());

        weikeCommentsDao.insertSelective(comments);

        // 计算微课综合得分
        weikeCommentsDao.calculatorScore(req.getWeike_id());

    }

    public PageResponse studentFindComments(FindCommentsPageRequest req) {
        PageResponse pageResponse = new PageResponse(req.getPageSize(), req.getCurrentPage());

        RowBounds rowBounds = new RowBounds(pageResponse.getOffset().intValue(), pageResponse.getPageSize().intValue());
        //查询总数
        Long total = weikeCommentsDao.findByWeikeIdForCount(req.getWeike_id());
        //查询列表
        List<WeikeCommentsDto> commentsDtoList = weikeCommentsDao.findByWeikeId(req.getWeike_id(),rowBounds);


        pageResponse.setTotal(total);
        pageResponse.setData(commentsDtoList);

        return pageResponse;
    }

}
