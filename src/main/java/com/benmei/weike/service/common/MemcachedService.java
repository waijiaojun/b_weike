package com.benmei.weike.service.common;

import com.nativetalk.bean.member.TdNtMember;
import net.rubyeye.xmemcached.MemcachedClient;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MemcachedService {

    Logger logger = LoggerFactory.getLogger("MemcachedService");

    @Autowired
    private MemcachedClient memcachedClient;


    /**
     * get 命令用于检索与之前添加的键值对相关的值。
     *
     * @param key 键
     * @return
     */
    public Object get(String key) {
        Object obj = null;
        if (key == null) {
            return null;
        }
        if ("[object Null]".equalsIgnoreCase(key)) {
            logger.error("无效Key:" + key);
            return null;
        }
        try {
            obj = memcachedClient.get(key);
        } catch (Exception e) {
            logger.error("Memcached get方法报错，key值：" + key, e);
        }
        return obj;
    }

    /**
     * 仅当缓存中不存在键时，add 命令才会向缓存中添加一个键值对。
     *
     * @param key    键
     * @param value  值
     * @param expire 过期时间 New Date(1000*10)：十秒后过期
     * @return
     */
    public boolean add(String key, Object value, Integer expire) {
        boolean flag = false;
        try {
            flag = memcachedClient.add(key, expire, value);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return flag;
    }

    /**
     * 仅当键已经存在时，replace 命令才会替换缓存中的键。
     *
     * @param key    键
     * @param value  值
     * @param expire 过期时间 单位：秒，超过30天使用绝对时间，默认30天
     * @return
     */
    public void replace(String key, Object value, Integer expire) {
        try {
            if (expire == null) {
                expire = 60 * 60 * 24 * 30;
            }
            memcachedClient.replace(key, expire, value);
        } catch (Exception e) {
            logger.error("Memcached replace方法报错，key值：" + key, e);
        }
    }


    /**
     * @param token
     * @return MembId 注意：此membid有可能是null
     */
    public Integer getMembId(String token) {
        Integer memb_id = null;

        //校验token，如果校验通过就从缓存中读取Member
        if (!StringUtils.isBlank(token)) {
            Object o = this.get(token);
            if (o != null) {
                if (TdNtMember.class.isInstance(o)) {
                    TdNtMember tdNtMember = (TdNtMember) o;
                    memb_id = tdNtMember.getMemb_id();
                }
            }
        }
        return memb_id;
    }

}
