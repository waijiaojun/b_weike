package com.benmei.weike.service;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.benmei.weike.entity.Bill;
import com.benmei.weike.dao.BillDao;

@Service
public class BillService{

    @Resource
    private BillDao billDao;

    public int insert(Bill pojo){
        return billDao.insert(pojo);
    }

    public int insertSelective(Bill pojo){
        return billDao.insertSelective(pojo);
    }

    public int insertList(List<Bill> pojos){
        return billDao.insertList(pojos);
    }

    public int update(Bill pojo){
        return billDao.update(pojo);
    }
}
