package com.benmei.weike.service;

import com.benmei.weike.dao.MemberContentRecommendDao;
import com.benmei.weike.dto.MemberContentRecommendDto;
import com.benmei.weike.entity.MemberContentRecommend;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class MemberContentRecommendService {

    public static final Logger logger = LoggerFactory.getLogger(MemberContentRecommendService.class);

    @Resource
    private MemberContentRecommendDao memberContentRomendDao;

    public int insert(MemberContentRecommend pojo) {
        return memberContentRomendDao.insert(pojo);
    }

    public int insertSelective(MemberContentRecommend pojo) {
        return memberContentRomendDao.insertSelective(pojo);
    }

    public int insertList(List<MemberContentRecommend> pojos) {
        return memberContentRomendDao.insertList(pojos);
    }

    public int update(MemberContentRecommend pojo) {
        return memberContentRomendDao.update(pojo);
    }

    /**
     * 根据学生id查询最新一条推荐内容，如果没有推荐内容，则返回null
     *
     * @param memb_id
     * @return
     */
    public MemberContentRecommendDto findRecommendContent(Integer memb_id) {
        if (memb_id == null) {
            logger.info("memb_id is null,无推荐内容");
            return null;
        }

        MemberContentRecommend recomend = memberContentRomendDao.searchOneByMembId(memb_id);

        MemberContentRecommendDto dto = null;
        // 查询内容对应的详情
        if (recomend != null) {
            if (MemberContentRecommend.ContentType.course.equals(recomend.getContent_type())) {
                dto = memberContentRomendDao.findCourseByCourseId(recomend.getContent_id());
                if (dto != null) {
                    dto.setRecommend_id(recomend.getId());
                    dto.setFrom("课程一对一");
                    dto.setType(MemberContentRecommend.ContentType.course);
                } else {
                    logger.error("无效的课程id:" + recomend.getContent_id());
                }
            } else if (MemberContentRecommend.ContentType.weike.equals(recomend.getContent_type())) {
                dto = memberContentRomendDao.findWeikeByWeikeId(recomend.getContent_id());
                if (dto != null) {
                    dto.setRecommend_id(recomend.getId());
                    dto.setFrom("微课");
                    dto.setType(MemberContentRecommend.ContentType.weike);
                } else {
                    logger.error("无效的微课id:" + recomend.getContent_id());
                }
            } else if (MemberContentRecommend.ContentType.live.equals(recomend.getContent_type())) {
                dto = memberContentRomendDao.findLiveHisByLiveHisId(recomend.getContent_id());
                if (dto != null) {
                    dto.setRecommend_id(recomend.getId());
                    dto.setFrom("公开课");
                    dto.setType(MemberContentRecommend.ContentType.live);
                } else {
                    logger.error("无效的公开课id:" + recomend.getContent_id());
                }
            } else {
                logger.error("无效的推荐内容类型,content_type:" + recomend.getContent_type());
            }

            if(dto!=null){
                recomend.setSend_count(recomend.getSend_count()+1);
                memberContentRomendDao.update(recomend);
            }

        }

        return dto;
    }

    /**
     * 学生点击推荐内容，内容点击数+1
     *
     * @param recommend_id
     * @return
     */
    public void clickRecommendContent(Integer recommend_id) {
        memberContentRomendDao.clickRecommendContent(recommend_id);
    }
}
