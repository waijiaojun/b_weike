package com.benmei.weike.service;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.benmei.weike.entity.WeikeImage;
import com.benmei.weike.dao.WeikeImageDao;

@Service
public class WeikeImageService{

    @Resource
    private WeikeImageDao weikeImageDao;

    public int insert(WeikeImage pojo){
        return weikeImageDao.insert(pojo);
    }

    public int insertSelective(WeikeImage pojo){
        return weikeImageDao.insertSelective(pojo);
    }

    public int insertList(List<WeikeImage> pojos){
        return weikeImageDao.insertList(pojos);
    }

    public int update(WeikeImage pojo){
        return weikeImageDao.update(pojo);
    }
}
