package com.benmei.weike.service;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.benmei.weike.entity.MemberSalesPromotionSettings;
import com.benmei.weike.dao.MemberSalesPromotionSettingsDao;

@Service
public class MemberSalesPromotionSettingsService{

    @Resource
    private MemberSalesPromotionSettingsDao memberSalesPromotionSettingsDao;

    public int insert(MemberSalesPromotionSettings pojo){
        return memberSalesPromotionSettingsDao.insert(pojo);
    }

    public int insertSelective(MemberSalesPromotionSettings pojo){
        return memberSalesPromotionSettingsDao.insertSelective(pojo);
    }

    public int insertList(List<MemberSalesPromotionSettings> pojos){
        return memberSalesPromotionSettingsDao.insertList(pojos);
    }

    public int update(MemberSalesPromotionSettings pojo){
        return memberSalesPromotionSettingsDao.update(pojo);
    }
}
