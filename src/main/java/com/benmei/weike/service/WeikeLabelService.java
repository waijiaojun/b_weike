package com.benmei.weike.service;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.benmei.weike.entity.WeikeLabel;
import com.benmei.weike.dao.WeikeLabelDao;

@Service
public class WeikeLabelService{

    @Resource
    private WeikeLabelDao weikeLabelDao;

    public int insert(WeikeLabel pojo){
        return weikeLabelDao.insert(pojo);
    }

    public int insertSelective(WeikeLabel pojo){
        return weikeLabelDao.insertSelective(pojo);
    }

    public int insertList(List<WeikeLabel> pojos){
        return weikeLabelDao.insertList(pojos);
    }

    public int update(WeikeLabel pojo){
        return weikeLabelDao.update(pojo);
    }
}
