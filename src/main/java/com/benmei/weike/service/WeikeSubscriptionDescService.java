package com.benmei.weike.service;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.benmei.weike.entity.WeikeSubscriptionDesc;
import com.benmei.weike.dao.WeikeSubscriptionDescDao;

@Service
public class WeikeSubscriptionDescService{

    @Resource
    private WeikeSubscriptionDescDao weikeSubscriptionDescDao;

    public int insert(WeikeSubscriptionDesc pojo){
        return weikeSubscriptionDescDao.insert(pojo);
    }

    public int insertSelective(WeikeSubscriptionDesc pojo){
        return weikeSubscriptionDescDao.insertSelective(pojo);
    }

    public int insertList(List<WeikeSubscriptionDesc> pojos){
        return weikeSubscriptionDescDao.insertList(pojos);
    }

    public int update(WeikeSubscriptionDesc pojo){
        return weikeSubscriptionDescDao.update(pojo);
    }
}
