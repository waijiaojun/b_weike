package com.benmei.weike.service.rabbitmq;

import com.benmei.weike.dao.MemberContentRecommendDao;
import com.benmei.weike.entity.Weike;
import com.benmei.weike.util.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * MQ 消息消费者
 * Created by Peter on 2017/6/12.
 */
@Component
@RabbitListener(queues="teenager-weike-published")
public class WeikePublishedReceiver {

    public static final Logger logger = LoggerFactory.getLogger(WeikePublishedReceiver.class);

    @Autowired
    MemberContentRecommendDao memberContentRecomendDao;

    /**
     * 当结束到新的微课发布消息时，将该微课推荐给相关的学生。根据微课所属类型，查询学生需求，然后根据需求查询出学生，将微课内容发送给这些学生。
     * @param weike 只有微课id
     */
    @RabbitHandler
    public void process(Weike weike){
        logger.info("----------- 接收到MQ的订阅消息Queue:[weike-published] ----------");
        logger.info(JsonUtil.toJson(weike));
        // 站内推荐内容给相关的学生
        // 根据微课所属类型，查询学生需求，然后根据需求查询出学生，将微课内容发送给这些学生
        memberContentRecomendDao.genRecommendContentToMember(weike.getId());
    }

}
