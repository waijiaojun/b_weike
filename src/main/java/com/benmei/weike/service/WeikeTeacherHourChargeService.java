package com.benmei.weike.service;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.benmei.weike.entity.WeikeTeacherHourCharge;
import com.benmei.weike.dao.WeikeTeacherHourChargeDao;

@Service
public class WeikeTeacherHourChargeService{

    @Resource
    private WeikeTeacherHourChargeDao weikeTeacherHourChargeDao;

    public int insert(WeikeTeacherHourCharge pojo){
        return weikeTeacherHourChargeDao.insert(pojo);
    }

    public int insertSelective(WeikeTeacherHourCharge pojo){
        return weikeTeacherHourChargeDao.insertSelective(pojo);
    }

    public int insertList(List<WeikeTeacherHourCharge> pojos){
        return weikeTeacherHourChargeDao.insertList(pojos);
    }

    public int update(WeikeTeacherHourCharge pojo){
        return weikeTeacherHourChargeDao.update(pojo);
    }
}
