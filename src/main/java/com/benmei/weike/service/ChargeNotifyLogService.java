package com.benmei.weike.service;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.benmei.weike.entity.ChargeNotifyLog;
import com.benmei.weike.dao.ChargeNotifyLogDao;

@Service
public class ChargeNotifyLogService{

    @Resource
    private ChargeNotifyLogDao chargeNotifyLogDao;

    public int insert(ChargeNotifyLog pojo){
        return chargeNotifyLogDao.insert(pojo);
    }

    public int insertSelective(ChargeNotifyLog pojo){
        return chargeNotifyLogDao.insertSelective(pojo);
    }

    public int insertList(List<ChargeNotifyLog> pojos){
        return chargeNotifyLogDao.insertList(pojos);
    }

    public int update(ChargeNotifyLog pojo){
        return chargeNotifyLogDao.update(pojo);
    }
}
