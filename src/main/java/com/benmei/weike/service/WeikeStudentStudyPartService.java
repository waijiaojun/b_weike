package com.benmei.weike.service;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.benmei.weike.entity.WeikeStudentStudyPart;
import com.benmei.weike.dao.WeikeStudentStudyPartDao;

@Service
public class WeikeStudentStudyPartService{

    @Resource
    private WeikeStudentStudyPartDao weikeStudentStudyPartDao;

    public int insert(WeikeStudentStudyPart pojo){
        return weikeStudentStudyPartDao.insert(pojo);
    }

    public int insertSelective(WeikeStudentStudyPart pojo){
        return weikeStudentStudyPartDao.insertSelective(pojo);
    }

    public int insertList(List<WeikeStudentStudyPart> pojos){
        return weikeStudentStudyPartDao.insertList(pojos);
    }

    public int update(WeikeStudentStudyPart pojo){
        return weikeStudentStudyPartDao.update(pojo);
    }
}
