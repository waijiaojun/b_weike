package com.benmei.weike.service;

import com.benmei.weike.dao.WeikePartDao;
import com.benmei.weike.entity.WeikePart;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class WeikePartService{

    @Resource
    private WeikePartDao weikePartDao;

    public int insert(WeikePart pojo){
        return weikePartDao.insert(pojo);
    }

    public int insertSelective(WeikePart pojo){
        return weikePartDao.insertSelective(pojo);
    }

    public int insertList(List<WeikePart> pojos){
        return weikePartDao.insertList(pojos);
    }

    public int update(WeikePart pojo){
        return weikePartDao.update(pojo);
    }

    /**
     * 插入微课章节
     * @param weike_id
     * @param title
     * @param create_user
     * @return
     */
    @Transactional
    public WeikePart insertPart(String weike_id, String title, Integer create_user){

        Integer updateCourse=weikePartDao.updateLessonCount(Long.valueOf(weike_id));

        WeikePart weikePart=new WeikePart();
        weikePart.setTitle(title);
        weikePart.setWeike_id(Long.valueOf(weike_id));
        weikePart.setSort(updateCourse+1);
        weikePart.setCreate_user(create_user);
        weikePart.setDeleted(0);
        Integer insertCount=weikePartDao.insert(weikePart);
        return weikePart;
    }

    /**
     * 更新微课章节
     * @param id
     * @param title
     * @param update_user
     * @return
     */
    public WeikePart updatePart(String id,String title, Integer update_user){
        WeikePart weikePart=new WeikePart();
        weikePart.setTitle(title);
        weikePart.setUpdate_user(update_user);
        weikePart.setId(Long.valueOf(id));
        Integer updatePart=weikePartDao.update(weikePart);
        return weikePart;
    }

    /**
     * 删除微课章节
     * @param id
     * @param update_user
     * @return
     */
    public WeikePart deletePart(String id, Integer update_user){
        WeikePart weikePart=new WeikePart();
        weikePart.setUpdate_user(update_user);
        weikePart.setId(Long.valueOf(id));
        weikePart.setDeleted(1);
        Integer updatePart=weikePartDao.update(weikePart);
        return weikePart;
    }
}
