package com.benmei.weike.service;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.benmei.weike.entity.WeikeContent;
import com.benmei.weike.dao.WeikeContentDao;

@Service
public class WeikeContentService{

    @Resource
    private WeikeContentDao weikeContentDao;

    public int insert(WeikeContent pojo){
        return weikeContentDao.insert(pojo);
    }

    public int insertSelective(WeikeContent pojo){
        return weikeContentDao.insertSelective(pojo);
    }

    public int insertList(List<WeikeContent> pojos){
        return weikeContentDao.insertList(pojos);
    }

    public int update(WeikeContent pojo){
        return weikeContentDao.update(pojo);
    }
}
