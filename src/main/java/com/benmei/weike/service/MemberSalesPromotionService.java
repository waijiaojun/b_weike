package com.benmei.weike.service;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.benmei.weike.entity.MemberSalesPromotion;
import com.benmei.weike.dao.MemberSalesPromotionDao;

@Service
public class MemberSalesPromotionService{

    @Resource
    private MemberSalesPromotionDao memberSalesPromotionDao;

    public int insert(MemberSalesPromotion pojo){
        return memberSalesPromotionDao.insert(pojo);
    }

    public int insertSelective(MemberSalesPromotion pojo){
        return memberSalesPromotionDao.insertSelective(pojo);
    }

    public int insertList(List<MemberSalesPromotion> pojos){
        return memberSalesPromotionDao.insertList(pojos);
    }

    public int update(MemberSalesPromotion pojo){
        return memberSalesPromotionDao.update(pojo);
    }
}
