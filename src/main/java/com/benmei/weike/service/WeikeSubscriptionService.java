package com.benmei.weike.service;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.benmei.weike.entity.WeikeSubscription;
import com.benmei.weike.dao.WeikeSubscriptionDao;

@Service
public class WeikeSubscriptionService {

    @Resource
    private WeikeSubscriptionDao wiekeSubscriptionDao;

    public int insert(WeikeSubscription pojo){
        return wiekeSubscriptionDao.insert(pojo);
    }

    public int insertSelective(WeikeSubscription pojo){
        return wiekeSubscriptionDao.insertSelective(pojo);
    }

    public int insertList(List<WeikeSubscription> pojos){
        return wiekeSubscriptionDao.insertList(pojos);
    }

    public int update(WeikeSubscription pojo){
        return wiekeSubscriptionDao.update(pojo);
    }
}
