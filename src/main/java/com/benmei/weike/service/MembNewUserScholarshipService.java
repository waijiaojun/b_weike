package com.benmei.weike.service;

import com.benmei.weike.common.GoodsCategory;
import com.benmei.weike.common.PayType;
import com.benmei.weike.common.PaymentConstants;
import com.benmei.weike.dao.BillDao;
import com.benmei.weike.dao.DictDao;
import com.benmei.weike.dao.TdNtMemberDao;
import com.benmei.weike.entity.Bill;
import com.benmei.weike.util.CommonUtil;
import com.benmei.weike.util.DateUtil;
import com.benmei.weike.util.OrderUtil;
import com.benmei.weike.web.WeikeStudentController;
import com.nativetalk.bean.member.TdNtMember;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.benmei.weike.entity.MembNewUserScholarship;
import com.benmei.weike.dao.MembNewUserScholarshipDao;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MembNewUserScholarshipService {

    public static final Logger logger = LoggerFactory.getLogger(MembNewUserScholarshipService.class);

    @Resource
    private MembNewUserScholarshipDao membNewUserScholarshipDao;

    @Resource
    private TdNtMemberDao membDao;

    @Resource
    private BillDao billDao;

    @Resource
    private DictDao dictDao;

    public int insert(MembNewUserScholarship pojo) {
        return membNewUserScholarshipDao.insert(pojo);
    }

    public int insertSelective(MembNewUserScholarship pojo) {
        return membNewUserScholarshipDao.insertSelective(pojo);
    }

    public int insertList(List<MembNewUserScholarship> pojos) {
        return membNewUserScholarshipDao.insertList(pojos);
    }

    public int update(MembNewUserScholarship pojo) {
        return membNewUserScholarshipDao.update(pojo);
    }

    @Transactional
    public Map<String,Object> findByMembId(Integer memb_id) throws ParseException {
        Map<String,Object> m=new HashMap<>();
        TdNtMember member = membDao.getById(memb_id);
        // 老用户 统一返回已领取
        Timestamp t = new Timestamp(System.currentTimeMillis());
        Date d =  DateUtil.createDate("2017-11-10 00:00:00");
        if(member.getMemb_register_time().before(new Timestamp(d.getTime()))){
            logger.info("老用户");
            MembNewUserScholarship obj = new MembNewUserScholarship(memb_id);
            m.put("obj",obj);
            return m;
        }else{
            logger.info("新用户");
            MembNewUserScholarship obj =membNewUserScholarshipDao.findByMembId(memb_id);
            m.put("obj",obj);
            TdNtMember tdNtMember=membDao.getById(memb_id);
            logger.info(""+tdNtMember.getMemb_birthday());
            String name= CommonUtil.getZodiac(tdNtMember.getMemb_birthday());
            String image_url=dictDao.searchByCodeName(name);
            m.put("img_url",image_url);
            logger.info(image_url);
        }

        return m;
    }

    // 领取新用户红包
    @Transactional
    public void collectUserScholarship(Integer memb_id) {
        MembNewUserScholarship ship = membNewUserScholarshipDao.findByMembId(memb_id);
        if (ship != null) {
            logger.warn("已领取红包，memb_id=" + memb_id);
            return;
        }
        // 1. 添加红包领取记录
        MembNewUserScholarship pojo = new MembNewUserScholarship(memb_id);
        membNewUserScholarshipDao.insert(pojo);

        // 2. 添加账单
        TdNtMember member = membDao.getById(memb_id);
        BigDecimal amount = new BigDecimal(20);// 红包金额
        BigDecimal membBalance = member.getMemb_balance();
        BigDecimal balanceAfter = membBalance.add(amount);

        Bill bill = new Bill();
        bill.setActionType(PaymentConstants.ActionType.IN);
        bill.setActionName(GoodsCategory.OTHER.getName());
        bill.setActionIconUrl(GoodsCategory.OTHER.getIcon());

        bill.setGoodsCategoryCode(GoodsCategory.OTHER.getCode());
        bill.setGoods_desc("新注册用户领取红包");
        bill.setPaymentStatus(PaymentConstants.Status.SUCCESS);
        bill.setPayMoney(new BigDecimal(0));
        bill.setPayType(PayType.system.getName());
        bill.setChannel(PaymentConstants.Channel.system);
        bill.setCreateOrderTime(new Date());
        bill.setOrderNo(OrderUtil.getOrderNumber());// 订单号（交易单号）

        bill.setAmount(amount);
        bill.setBalanceBefore(membBalance);
        bill.setBalanceChange(amount);
        bill.setBalanceAfter(balanceAfter);
        bill.setMembId(memb_id);
        billDao.insert(bill);

        // 3. 添加余额
        member.setMemb_balance(balanceAfter);
        membDao.updateBalance(member);
    }

    public static void main(String[] args) {
        try {
            Date reg =  DateUtil.createDate("2017-10-17 18:38:43");
            Date d =  DateUtil.createDate("2017-11-10 00:00:00");

            if(reg.before(new Timestamp(d.getTime()))){
                logger.info("老用户");
            }else{
                logger.info("新用户");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
