package com.benmei.weike.dao;

import com.nativetalk.bean.member.TdNtMembFoundChange;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * Created by lizhun on 15/12/23.
 */
@Mapper
public interface TdNtMembFoundChangeDao {
    /**
     * @Purpose  添加用户资金变动记录
     * @version  1.0
     * @author   lizhun
     * @param    tdNtMembFoundChangeInset
     * @return   void
     */
    public void insertTdNtMembFoundChange(TdNtMembFoundChange tdNtMembFoundChangeInset);

    /**
     * 根据订单编号查询资金变化记录
     *
     * @param orderNo
     * @return
     */
    public TdNtMembFoundChange findTdNtMembFoundChangeByOrderNo(@Param("orderNo") String orderNo);

    /**
     * 根据订单号补全订单号的信息
     * @param tdNtMembFoundChangeInset
     */
    public void updatePaymentInfo(TdNtMembFoundChange tdNtMembFoundChangeInset);
}
