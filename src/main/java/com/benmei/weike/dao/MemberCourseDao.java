package com.benmei.weike.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.benmei.weike.entity.MemberCourse;

@Mapper
public interface MemberCourseDao {
    int insert(@Param("pojo") MemberCourse pojo);

    int insertSelective(@Param("pojo") MemberCourse pojo);

    int insertList(@Param("pojos") List<MemberCourse> pojo);

    int update(@Param("pojo") MemberCourse pojo);

    MemberCourse findByStudentIdAndCouId(@Param("memb_id")Integer memb_id, @Param("cou_id")Integer cou_id);
}
