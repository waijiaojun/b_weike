package com.benmei.weike.dao;

import com.benmei.weike.entity.WeikeStudentPlayedAudio;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface WeikeStudentPlayedAudioDao {
    int insert(@Param("pojo") WeikeStudentPlayedAudio pojo);

    int insertSelective(@Param("pojo") WeikeStudentPlayedAudio pojo);

    int insertList(@Param("pojos") List<WeikeStudentPlayedAudio> pojo);

    int update(@Param("pojo") WeikeStudentPlayedAudio pojo);

    WeikeStudentPlayedAudio search(@Param("pojo")WeikeStudentPlayedAudio pojo);
}
