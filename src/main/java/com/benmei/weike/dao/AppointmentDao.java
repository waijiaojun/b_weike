package com.benmei.weike.dao;

import com.benmei.weike.entity.ReserveTime;
import com.benmei.weike.entity.Setmeal;
import com.benmei.weike.entity.TeacherAppointmentTime;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import com.benmei.weike.entity.Appointment;

@Mapper
public interface AppointmentDao {

    int insert(@Param("pojo") Appointment pojo);

    int insertSelective(@Param("pojo") Appointment pojo);

    int insertList(@Param("pojos") List<Appointment> pojo);

    int update(@Param("pojo") Appointment pojo);

    Appointment findConflictAppointmentForTeacher(@Param("tea_id") Integer tea_id, @Param("start_time")Date reserve_start_time, @Param("end_time")Date reserve_end_time);

    Appointment findConflictAppointmentForMember(@Param("memb_id") Integer memb_id, @Param("start_time")Date reserve_start_time, @Param("end_time")Date reserve_end_time);

    Setmeal findFirstExpireSetmeal(@Param("memb_id")Integer memb_id, @Param("cou_id")Integer cou_id);

    void updateAppointmentSize(@Param("set_reserve_size")int set_reserve_size, @Param("memb_set_id")Integer memb_set_id);

    void updateAppointmentTimeStatus(TeacherAppointmentTime appointmentTime);

    ReserveTime findConflictReserveTimeForTeacher(@Param("tea_id") Integer tea_id, @Param("start_time")Date reserve_start_time, @Param("end_time")Date reserve_end_time,@Param("cou_time") Integer cou_time);
}
