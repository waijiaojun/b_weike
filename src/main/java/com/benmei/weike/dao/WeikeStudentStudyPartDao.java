package com.benmei.weike.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.benmei.weike.entity.WeikeStudentStudyPart;

@Mapper
public interface WeikeStudentStudyPartDao {
    int insert(@Param("pojo") WeikeStudentStudyPart pojo);

    int insertSelective(@Param("pojo") WeikeStudentStudyPart pojo);

    int insertList(@Param("pojos") List<WeikeStudentStudyPart> pojo);

    int update(@Param("pojo") WeikeStudentStudyPart pojo);

    WeikeStudentStudyPart search(@Param("pojo") WeikeStudentStudyPart pojo);
}
