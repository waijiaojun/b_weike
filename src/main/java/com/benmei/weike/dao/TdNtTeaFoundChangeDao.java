package com.benmei.weike.dao;

import com.nativetalk.bean.teacher.TdNtTeaFoundChange;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by lizhun on 16/1/7.
 */
@Mapper
public interface TdNtTeaFoundChangeDao {
    /**
     * @Purpose  添加教师资金变动记录
     * @version  1.0
     * @author   lizhun
     * @param    tdNtTeaFoundChange
     * @return   void
     */
    public void insert(TdNtTeaFoundChange tdNtTeaFoundChange);
}
