package com.benmei.weike.dao;

import com.benmei.weike.entity.WeikeStudentBuy;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface WeikeStudentBuyDao {
    int insert(@Param("pojo") WeikeStudentBuy pojo);

    int insertSelective(@Param("pojo") WeikeStudentBuy pojo);

    int insertList(@Param("pojos") List<WeikeStudentBuy> pojo);

    int update(@Param("pojo") WeikeStudentBuy pojo);

    List<WeikeStudentBuy> find(@Param("pojo") WeikeStudentBuy pojo);

    Long findStudentBuyCourseCount(@Param("memb_id") Integer memb_id);

    WeikeStudentBuy selectExit(@Param("pojo") WeikeStudentBuy pojo);
}
