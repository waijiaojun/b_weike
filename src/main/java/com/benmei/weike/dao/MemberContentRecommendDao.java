package com.benmei.weike.dao;

import com.benmei.weike.dto.MemberContentRecommendDto;
import com.benmei.weike.entity.MemberContentRecommend;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface MemberContentRecommendDao {
    int insert(@Param("pojo") MemberContentRecommend pojo);

    int insertSelective(@Param("pojo") MemberContentRecommend pojo);

    int insertList(@Param("pojos") List<MemberContentRecommend> pojo);

    int update(@Param("pojo") MemberContentRecommend pojo);

    MemberContentRecommend searchOneByMembId(Integer memb_id);

    MemberContentRecommendDto findCourseByCourseId(Integer content_id);

    MemberContentRecommendDto findWeikeByWeikeId(Integer content_id);

    MemberContentRecommendDto findLiveHisByLiveHisId(Integer content_id);

    void clickRecommendContent(Integer recommend_id);

    void genRecommendContentToMember(Long id);
}
