package com.benmei.weike.dao;

import com.benmei.weike.entity.CompositeCourse;
import com.nativetalk.bean.live.LiveCourse;
import com.nativetalk.bean.teacher.TeacherLabel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

@Mapper
public interface CompositeCourseDao {
    int insert(@Param("pojo") CompositeCourse pojo);

    int insertSelective(@Param("pojo") CompositeCourse pojo);

    int insertList(@Param("pojos") List<CompositeCourse> pojo);

    int update(@Param("pojo") CompositeCourse pojo);



    List<CompositeCourse> studentFindAllCourseForTodayBoutique();

    List<CompositeCourse> studentFindAllCourseForTodayBoutiqueAll();

    List<LiveCourse> getLiveCourseByIds(@Param("liveCourseIds")List<Integer> liveCourseIds);

    void initWeikeData();

    void initLiveData();

    List<TeacherLabel> getTeachersLabels(@Param("teaIds")List<Integer> teaIds);

    void truncate();

    List<CompositeCourse> studentFindAllCourse(@Param("category_id")Integer category_id,RowBounds rowBounds);

    Long studentFindAllCourseCount(@Param("category_id")Integer category_id);

    List<CompositeCourse> studentFindAllCourseForUnionAll(@Param("category_id")Integer categor_id, RowBounds rowBounds);
    Long studentFindAllCourseCountForUnionAll(@Param("category_id")Integer categor_id);
}
