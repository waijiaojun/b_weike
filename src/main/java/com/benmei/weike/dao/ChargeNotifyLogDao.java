package com.benmei.weike.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.benmei.weike.entity.ChargeNotifyLog;

@Mapper
public interface ChargeNotifyLogDao {
    int insert(@Param("pojo") ChargeNotifyLog pojo);

    int insertSelective(@Param("pojo") ChargeNotifyLog pojo);

    int insertList(@Param("pojos") List<ChargeNotifyLog> pojo);

    int update(@Param("pojo") ChargeNotifyLog pojo);
}
