package com.benmei.weike.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.benmei.weike.entity.MembNewUserScholarship;

@Mapper
public interface MembNewUserScholarshipDao {
    int insert(@Param("pojo") MembNewUserScholarship pojo);

    int insertSelective(@Param("pojo") MembNewUserScholarship pojo);

    int insertList(@Param("pojos") List<MembNewUserScholarship> pojo);

    int update(@Param("pojo") MembNewUserScholarship pojo);

    MembNewUserScholarship findByMembId(Integer memb_id);
}
