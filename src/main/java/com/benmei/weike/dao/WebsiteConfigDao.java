package com.benmei.weike.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.benmei.weike.entity.WebsiteConfig;

@Mapper
public interface WebsiteConfigDao {
    int insert(@Param("pojo") WebsiteConfig pojo);

    int insertSelective(@Param("pojo") WebsiteConfig pojo);

    int insertList(@Param("pojos") List<WebsiteConfig> pojo);

    int update(@Param("pojo") WebsiteConfig pojo);

    String getData(int web_id);
}
