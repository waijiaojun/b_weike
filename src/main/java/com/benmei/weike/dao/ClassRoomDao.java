package com.benmei.weike.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.benmei.weike.entity.ClassRoom;

@Mapper
public interface ClassRoomDao {
    int insert(@Param("pojo") ClassRoom pojo);

    int insertSelective(@Param("pojo") ClassRoom pojo);

    int insertList(@Param("pojos") List<ClassRoom> pojo);

    int update(@Param("pojo") ClassRoom pojo);

    ClassRoom findById(Integer roomId);
}
