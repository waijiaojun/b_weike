package com.benmei.weike.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.benmei.weike.entity.SystemAccessLog;

@Mapper
public interface SystemAccessLogDao {
    int insert(@Param("pojo") SystemAccessLog pojo);

    int insertSelective(@Param("pojo") SystemAccessLog pojo);

    int insertList(@Param("pojos") List<SystemAccessLog> pojo);

    int update(@Param("pojo") SystemAccessLog pojo);
}
