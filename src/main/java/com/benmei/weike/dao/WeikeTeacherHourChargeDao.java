package com.benmei.weike.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.benmei.weike.entity.WeikeTeacherHourCharge;

@Mapper
public interface WeikeTeacherHourChargeDao {
    int insert(@Param("pojo") WeikeTeacherHourCharge pojo);

    int insertSelective(@Param("pojo") WeikeTeacherHourCharge pojo);

    int insertList(@Param("pojos") List<WeikeTeacherHourCharge> pojo);

    int update(@Param("pojo") WeikeTeacherHourCharge pojo);

    Integer findChargeTotalDuration(Long weike_id);
}
