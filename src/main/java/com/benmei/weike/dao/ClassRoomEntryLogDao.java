package com.benmei.weike.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.benmei.weike.entity.ClassRoomEntryLog;

@Mapper
public interface ClassRoomEntryLogDao {
    int insert(@Param("pojo") ClassRoomEntryLog pojo);

    int insertSelective(@Param("pojo") ClassRoomEntryLog pojo);

    int insertList(@Param("pojos") List<ClassRoomEntryLog> pojo);

    int update(@Param("pojo") ClassRoomEntryLog pojo);
}
