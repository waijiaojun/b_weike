package com.benmei.weike.dao;

import com.benmei.weike.entity.WeikeContent;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface WeikeContentDao {
    int insert(@Param("pojo") WeikeContent pojo);

    int insertSelective(@Param("pojo") WeikeContent pojo);

    int insertList(@Param("pojos") List<WeikeContent> pojo);

    int update(@Param("pojo") WeikeContent pojo);

    List<WeikeContent> searchByWeikeId(@Param("weike_id") Long weike_id);

    List<WeikeContent> searchByWeikeIdWithPlayNumber(@Param("weike_id") Long weike_id, @Param("memb_id") Integer memb_id);

    Integer searchSortCount(@Param("weike_id") Long weike_id,@Param("weike_part_sort") Integer weike_part_sort);

    Integer updatePublish(@Param("weike_id") Long weike_id,@Param("update_user") Integer update_user);

    WeikeContent findById(Long weike_content_id);

    List<WeikeContent> searchByWeikeIdForTeacher(Long weikeId);
}
