package com.benmei.weike.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.benmei.weike.entity.SystemConfig;

@Mapper
public interface SystemConfigDao {
    int insert(@Param("pojo") SystemConfig pojo);

    int insertSelective(@Param("pojo") SystemConfig pojo);

    int insertList(@Param("pojos") List<SystemConfig> pojo);

    int update(@Param("pojo") SystemConfig pojo);

    SystemConfig findByCode(String code);
}
