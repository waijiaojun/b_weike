package com.benmei.weike.dao;

import com.benmei.weike.entity.TsNtSendHis;
import org.apache.ibatis.annotations.Mapper;

import java.sql.Timestamp;

@Mapper
public interface TsNtSendHisDao {
    /**
     * 统计当天发送短信总条数
     * @param phoneno
     * @return
     */
    public Integer codeCountByPhone(String phoneno);

    /**
     * 最近发送短信的记录
     * @param phoneno
     * @return
     */
    public TsNtSendHis lastCodeByPhone(String phoneno);
    /**
     * 添加发送短信的记录
     * @param tsNtSendHis
     */
    public void insertCode(TsNtSendHis tsNtSendHis);
}
