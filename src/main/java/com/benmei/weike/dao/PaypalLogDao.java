package com.benmei.weike.dao;

import com.benmei.weike.entity.PaypalLog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface PaypalLogDao {

    public void insertSeletive(@Param("pojo") PaypalLog pojo);
}
