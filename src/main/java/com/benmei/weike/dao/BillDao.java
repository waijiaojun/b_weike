package com.benmei.weike.dao;

import com.benmei.weike.dto.WeikeBoutiqueListDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.benmei.weike.entity.Bill;
import org.apache.ibatis.session.RowBounds;

@Mapper
public interface BillDao {
    int insert(@Param("pojo") Bill pojo);

    int insertSelective(@Param("pojo") Bill pojo);

    int insertList(@Param("pojos") List<Bill> pojo);

    int update(@Param("pojo") Bill pojo);

    Bill getByOrderNo(String orderNo);

    void updatePaymentStatus(@Param("orderNo") String orderNo,@Param("paymentStatus") Integer  paymentStatus);

    Long findByMembIdCount(Integer memb_id);

    List<Bill> findByMembId(RowBounds rowBounds, Integer memb_id);
}
