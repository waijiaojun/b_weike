package com.benmei.weike.dao;

import com.benmei.weike.entity.WeikeSubscription;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface WeikeSubscriptionDao {
    int insert(@Param("pojo") WeikeSubscription pojo);

    int insertSelective(@Param("pojo") WeikeSubscription pojo);

    int insertList(@Param("pojos") List<WeikeSubscription> pojo);

    int update(@Param("pojo") WeikeSubscription pojo);

    WeikeSubscription findValidSubscribedByMembId(@Param("memb_id") Integer memb_id);

    List<WeikeSubscription> findAllByMembId(Integer memb_id);
}
