package com.benmei.weike.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.benmei.weike.entity.WeiChatPayNotifyRequest;

@Mapper
public interface WeiChatPayNotifyRequestDao {
    int insert(@Param("pojo") WeiChatPayNotifyRequest pojo);

    int insertSelective(@Param("pojo") WeiChatPayNotifyRequest pojo);

    int insertList(@Param("pojos") List<WeiChatPayNotifyRequest> pojo);

    int update(@Param("pojo") WeiChatPayNotifyRequest pojo);
}
