package com.benmei.weike.dao;

import com.benmei.weike.dto.weikeDetailResponse.Part;
import com.benmei.weike.entity.WeikePart;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface WeikePartDao {
    int insert(@Param("pojo") WeikePart pojo);

    int insertSelective(@Param("pojo") WeikePart pojo);

    int insertList(@Param("pojos") List<WeikePart> pojo);

    int update(@Param("pojo") WeikePart pojo);

    List<WeikePart> searchByWeikeId(Long weikeId);

    /**
     * 一个特殊的查询方法，查询每个Part，并且汇总每一个Part下面录音的时长,如果 memb_id!=null 查询
     *
     * @param weike_id
     * @return
     */
    List<Part> findByWeikeIdWithDuration(@Param("weike_id") Long weike_id, @Param("memb_id") Integer memb_id);

    Integer findUpdateLessonCount(Long weike_id);

    Integer updatePublish(@Param("weike_id") Long weike_id,@Param("update_user") Integer update_user);

    WeikePart findByWeikeIdAndSort(@Param("weike_id")Long weike_id, @Param("weike_part_sort")Integer weike_part_sort);

    List<WeikePart> searchByWeikeIdForTeacher(Long weikeId);

    Integer updateLessonCount(Long weike_id);
}
