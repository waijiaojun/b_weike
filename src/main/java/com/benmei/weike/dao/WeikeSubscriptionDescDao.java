package com.benmei.weike.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.benmei.weike.entity.WeikeSubscriptionDesc;

@Mapper
public interface WeikeSubscriptionDescDao {
    int insert(@Param("pojo") WeikeSubscriptionDesc pojo);

    int insertSelective(@Param("pojo") WeikeSubscriptionDesc pojo);

    int insertList(@Param("pojos") List<WeikeSubscriptionDesc> pojo);

    int update(@Param("pojo") WeikeSubscriptionDesc pojo);

    WeikeSubscriptionDesc find();
}
