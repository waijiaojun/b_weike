package com.benmei.weike.dao;

import com.nativetalk.bean.teacher.TdNtTeacherLabel;
import com.nativetalk.bean.teacher.TdNtTeacherResult;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

@Mapper
public interface TdNtTeacherDao {

    /**
     * @Purpose  根据token查找老师信息
     * @version  4.1
     * @author   wangzheng
     * @return   TdNtTeacherResult
     */
    public TdNtTeacherResult findTdNtTeacherByToken(String token);

    TdNtTeacherResult getById(Integer tea_id);

    /**
     * 根据老师id查询老师的标签
     * @param tea_id
     * @return
     */
    List<TdNtTeacherLabel> getLabelsByTeaId(Integer tea_id);

    BigDecimal findTdNtTeacherBalance(Integer tea_id);

    void addBalance(@Param("tea_id")Integer teaId, @Param("money")BigDecimal money);

    void addVideo(@Param("tea_id")Integer teaId,@Param("tea_video_intro")String tea_video_intro);

    void addVideoCover(@Param("tea_id")Integer tea_id, @Param("tea_intro_video_cover")String tea_intro_video_cover);
}
