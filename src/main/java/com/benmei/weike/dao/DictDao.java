package com.benmei.weike.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * Created by waijiaojun on 2017/9/7.
 */
@Mapper
public interface DictDao {
    String searchTagName(@Param("dict_id")Integer dict_id);
    /**
     * 根据生肖返回红包图片
     * @param dict_name
     * @return
     */
    String searchByCodeName(@Param("dict_name") String dict_name);
}
