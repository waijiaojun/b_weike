package com.benmei.weike.dao;

import com.benmei.weike.entity.*;
import com.nativetalk.bean.member.TdNtMembFoundChange;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

@Mapper
public interface PaymentDao {

    Setmeal findCourseSetmealById(Integer goodsId);

    void insertTdNtMembFoundChange(TdNtMembFoundChange tdNtMembFoundChangeInset);

    void updateMenberBalance(@Param("memb_id") Integer memb_id, @Param("balance") BigDecimal backMembBalance);

    Setmeal findTsNtSetmeal(@Param("goodsId") Integer goodsId);

    void addMembSetmeal(Setmeal setmeal);

    void updateSetmealBuyNumber(Integer goodsId);

    List<PayActivity> findPayActivities();

    void updateCourseStudyNumber(@Param("setmealId")Integer setmealId);
}
