package com.benmei.weike.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.benmei.weike.entity.MemberSalesPromotion;

@Mapper
public interface MemberSalesPromotionDao {
    int insert(@Param("pojo") MemberSalesPromotion pojo);

    int insertSelective(@Param("pojo") MemberSalesPromotion pojo);

    int insertList(@Param("pojos") List<MemberSalesPromotion> pojo);

    int update(@Param("pojo") MemberSalesPromotion pojo);

    MemberSalesPromotion getForMember(Integer memb_id);
}
