package com.benmei.weike.dao;

import com.benmei.weike.entity.MemberResister;
import com.nativetalk.bean.member.TdNtMember;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface TdNtMemberDao {

	/**
	 * @Purpose  根据token查找用户信息
	 * @version  4.1
	 * @author   wangzheng
	 * @param    token
	 * @return   TdNtMember
	 */
	public TdNtMember findTdNtMemberByToken(String token);

    TdNtMember getById(Integer memb_id);

	void updateBalance(TdNtMember tdNtMember);

	/**
	 * 根据用户openID查询用户信息
	 * @param openId
	 * @return
	 */
	public TdNtMember findTdNtMemberByOpenId(String openId);

	/**
	 * 小程序注册用户信息
	 * @param memberResister
	 * @return
	 */
	public Integer insertTdNtMember(MemberResister memberResister);

	/**
	 * 根据手机号查询用户是否存在
	 * @param memb_phone
	 * @return
	 */
	public TdNtMember findTdNtMemberRegisterSms(String memb_phone);

	public Integer updateOpenId(@Param("open_id") String open_id,@Param("memb_id") Integer memb_id);
}
