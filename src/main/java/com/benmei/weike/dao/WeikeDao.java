package com.benmei.weike.dao;

import com.benmei.weike.dto.*;
import com.benmei.weike.entity.Weike;
import com.benmei.weike.entity.WeikeTodayBoutique;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

@Mapper
public interface WeikeDao {
    int insert(@Param("pojo") Weike pojo);

    int insertSelective(@Param("pojo") Weike pojo);

    int insertList(@Param("pojos") List<Weike> pojo);

    int update(@Param("pojo") Weike pojo);

    List<Weike> searchWeike(@Param("pojo") Weike pojo, RowBounds rowBounds);

    Long searchWeikeCount(@Param("pojo") Weike pojo);

    /**
     * 查询未审核和审核不通过的微课
     *
     * @param weike
     * @return
     */
    List<Weike> searchWeikeForPending(@Param("pojo") Weike weike, RowBounds rowBounds);

    Long searchWeikeForPendingCount(@Param("pojo") Weike weike);

    Weike getById(Long id);

    /**
     * 微课精选 专用查询语句
     *
     * @param rowBounds
     * @return
     */
    List<WeikeBoutiqueListDto> searchWeikeBoutiqueList(RowBounds rowBounds);

    Long searchWeikeBoutiqueListCount();

    List<WeikeBoutiqueListDto> searchWeikeHotList(RowBounds rowBounds);

    Long searchWeikeHotListCount();

    Long searchByTeacherIdCount(Integer tae_id);

    List<StudentFindWeikeListForTeacherDto> searchByTeacherId(Integer tae_id, RowBounds rowBounds);

    List<HotLiveDto> liveHot();

    List<WeikeDto> getByIds(@Param("weikeIds")List<Integer> weikeIds);

    Long studentMyWeikeCount(@Param("memb_id")Integer memb_id);

    List<StudentMyWeikeDto> studentMyWeike(@Param("memb_id")Integer memb_id, RowBounds rowBounds);

    List<WeikeTodayBoutique> studentFindWeikeForTodayBoutique();

    void updateBuyNumber(Long id);

    Integer findTotalDurationById(String weike_id);

    Weike selectAllById(Long id);
}
