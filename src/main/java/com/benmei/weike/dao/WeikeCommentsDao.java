package com.benmei.weike.dao;

import com.benmei.weike.dto.TeacherFindCommentsDto;
import com.benmei.weike.dto.WeikeCommentsDto;
import com.benmei.weike.entity.WeikeComments;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

@Mapper
public interface WeikeCommentsDao {
    int insert(@Param("pojo") WeikeComments pojo);

    int insertSelective(@Param("pojo") WeikeComments pojo);

    int insertList(@Param("pojos") List<WeikeComments> pojo);

    int update(@Param("pojo") WeikeComments pojo);

    /**
     * 计算微课综合得分
     * @param weike_id
     */
    void calculatorScore(Long weike_id);

    /**
     * 查询微课评论list
     * @param weike_id
     * @return
     */
    List<WeikeCommentsDto> findByWeikeId(Long weike_id, RowBounds rowBounds);
    Long findByWeikeIdForCount(Long weike_id);

    List<WeikeComments> find(@Param("pojo") WeikeComments pojo);

    Long teacherFindCommentsCount(@Param("tea_id")Integer tea_id);

    List<TeacherFindCommentsDto> teacherFindComments(@Param("tea_id")Integer tea_id, RowBounds rowBounds);
}
