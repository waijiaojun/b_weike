package com.benmei.weike.dao;

import com.benmei.weike.entity.CourseChapter;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.benmei.weike.entity.CourseInfo;

@Mapper
public interface CourseInfoDao {
    int insert(@Param("pojo") CourseInfo pojo);

    int insertSelective(@Param("pojo") CourseInfo pojo);

    int insertList(@Param("pojos") List<CourseInfo> pojo);

    int update(@Param("pojo") CourseInfo pojo);

    CourseInfo getById(Integer cou_id);

    CourseInfo getByIdSetmealId(Integer goodsId);

    List<CourseChapter> findChapterByCouId(Integer cou_id);
}
