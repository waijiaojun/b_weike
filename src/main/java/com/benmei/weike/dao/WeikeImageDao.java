package com.benmei.weike.dao;

import com.benmei.weike.entity.WeikeImage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface WeikeImageDao {
    int insert(@Param("pojo") WeikeImage pojo);

    int insertSelective(@Param("pojo") WeikeImage pojo);

    int insertList(List<WeikeImage> pojo);

    int update(@Param("pojo") WeikeImage pojo);

    Integer deleteByUrl(@Param("url") String url,@Param("updateUser") Integer updateUser);

    List<WeikeImage> searchByWeikeId(Long weike_id);

    Integer searchPartCount(@Param("weike_id") Long weike_id,@Param("weike_part_sort") Integer weike_part_sort);

    Integer updatePublish(@Param("weike_id") Long weike_id,@Param("update_user") Integer update_user);

    Integer updateForword(@Param("id") Long id,@Param("small") Integer small,@Param("big") Integer big);

    Integer updateLater(@Param("id") Long id,@Param("small") Integer small,@Param("big") Integer big);

    List<WeikeImage> searchByWeikeIdForTeacher(Long weikeId);
}
