package com.benmei.weike.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

import com.benmei.weike.entity.MemberSalesPromotionSettings;

@Mapper
public interface MemberSalesPromotionSettingsDao {
    int insert(@Param("pojo") MemberSalesPromotionSettings pojo);

    int insertSelective(@Param("pojo") MemberSalesPromotionSettings pojo);

    int insertList(@Param("pojos") List<MemberSalesPromotionSettings> pojo);

    int update(@Param("pojo") MemberSalesPromotionSettings pojo);

    MemberSalesPromotionSettings findById(@Param("salesPromotionId") Integer salesPromotionId, @Param("memb_id") Integer memb_id);
}
