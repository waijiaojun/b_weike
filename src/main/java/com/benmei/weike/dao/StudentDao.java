package com.benmei.weike.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.benmei.weike.entity.Student;

@Mapper
public interface StudentDao {
    int insert(@Param("pojo") Student pojo);

    int insertSelective(@Param("pojo") Student pojo);

    int insertList(@Param("pojos") List<Student> pojo);

    int update(@Param("pojo") Student pojo);

    Student getById(Integer memb_id);
}
