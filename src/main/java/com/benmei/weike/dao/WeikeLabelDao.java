package com.benmei.weike.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.benmei.weike.entity.WeikeLabel;

@Mapper
public interface WeikeLabelDao {
    int insert(@Param("pojo") WeikeLabel pojo);

    int insertSelective(@Param("pojo") WeikeLabel pojo);

    int insertList(@Param("pojos") List<WeikeLabel> pojo);

    int update(@Param("pojo") WeikeLabel pojo);
}
