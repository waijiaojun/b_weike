package com.benmei.weike.dao;

import com.benmei.weike.dto.IntoClassRoom.IntoClassRoomCoursePpt;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.benmei.weike.entity.CoursePicture;

@Mapper
public interface CoursePictureDao {
    int insert(@Param("pojo") CoursePicture pojo);

    int insertSelective(@Param("pojo") CoursePicture pojo);

    int insertList(@Param("pojos") List<CoursePicture> pojo);

    int update(@Param("pojo") CoursePicture pojo);

    List<IntoClassRoomCoursePpt> findByCouId(Integer cou_id);
}
