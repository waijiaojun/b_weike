package com.benmei.weike.common;

/**
 * Created by Peter on 2017/9/20.
 */
public class PaymentConstants {

    public static class ActionType {
        /**
         * +1 充值
         */
        public static final Integer IN = 1;
        /**
         * -1 付款
         */
        public static final Integer OUT = -1;
    }

    /**
     * 学生支付原因
     */
    public static class StudentActionName {
        /**
         * 充值
         */
        public static final String recharge_msg = "充值";
        public static final String recharge_msg_icon = "充值";

        public static final String recharge_gift_msg = "充值赠送";
        public static final String recharge_gift_msg_icon = "充值赠送";

        /**
         * 购买课程
         */
        public static final String buy_course_msg = "购买课程";
        public static final String buy_course_msg_icon = "";

        /**
         * 取消预约
         */
        public static final String cancel_reserver_msg = "取消预约";
        public static final String cancel_reserver_msg_icon = "取消预约";

        /**
         * 购买微课
         */
        public static final String buy_weike_msg = "购买系统课";
        public static String buy_weike_msg_icon = "";

        /**
         * 购买系统课年度会员
         */
        public static final String buy_weike_vip_year_msg = "购买系统课年度会员";
        public static final String buy_weike_vip_year_msg_icon = "";

    }

    //1待付款，2用户取消支付，3支付失败，4支付成功
    public static class Status {
        public static final Integer UN_PAY = 1;// 1待付款
        public static final Integer CANCEL = 2;// 2用户取消支付
        public static final Integer FAILED = 3;// 3支付失败
        public static final Integer SUCCESS = 4;// 4支付成功
    }

    // #支付渠道 wx,alipay,wallet,system
    public class Channel {
        public static final String wx = "wx";
        public static final String alipay = "alipay";
        public static final String wallet = "wallet";
        public static final String system = "system";
    }

}
