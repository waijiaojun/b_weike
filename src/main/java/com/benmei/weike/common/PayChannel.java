package com.benmei.weike.common;

/**
 * Created by Peter on 2017/9/20.
 * wx 微信；alipay 支付宝
 */
public enum PayChannel {

    alipay("alipay", "支付宝", "支付宝 APP 支付"),
    wx("wx", "微信", "微信 APP 支付");

    private final String code;//alipay

    private final String name;//支付宝

    private final String detail;// 支付宝 APP 支付


    PayChannel(String code, String name, String detail) {
        this.code = code;
        this.name = name;
        this.detail = detail;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getDetail() {
        return detail;
    }

    public static PayChannel getByCode(String code) {
        PayChannel result = null;
        for (PayChannel payChannel : PayChannel.values()) {
            if (payChannel.code.equals(code)) {
                result = payChannel;
            }
        }
        return result;
    }

    public static boolean contains(String code) {
        boolean result = false;
        for (PayChannel ayChannel : PayChannel.values()) {
            if (ayChannel.code.equals(code)) {
                result = true;
                break;
            }
        }
        return result;
    }
}
