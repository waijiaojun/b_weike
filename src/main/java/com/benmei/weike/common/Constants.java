package com.benmei.weike.common;

import java.math.BigDecimal;

/**
 * Created by Peter on 2017/4/18.
 */
public class Constants {

    //********************* 1 推送配置 start **********************************************
/*
    // ------- 测试环境 推送配置-----------------
    //个推学生端
    public final static String student_appId = "yreq61s9Gh8jg7kv7PPxv2";
    public final static String student_appKey = "tCAbE6qfXN6CyvENuJ8XB8";
    public final static String student_masterSecret = "6UBAe4GxTC6YnYadLidMR";
    //个推教师端
    public final static String teacher_appId = "HL4Px7tJ0U8Q6MKp5gbMj6";
    public final static String teacher_appKey = "vwN8yDDEnmAWJZFXeb7We7";
    public final static String teacher_masterSecret = "2XbD733ms7A6RUFlFhBpw9";

*/

    // ------- 正式环境 推送配置-----------------
    //    个推教师端
	public final static String teacher_appId = "bbw9mbNGub6KhlqYA2FQQ8";
	public final static String teacher_appKey = "Z3KMXt5Y3H60UWHGLuZBT";
	public final static String teacher_masterSecret = "lFsSVn0jvf5LzvYnqhVoZ5";
	//个推学生端
	public final static String student_appId = "gSCgThOHXt78FGdiwVQl03";
	public final static String student_appKey = "1Eh77wzoamAoZdz5xEi545";
	public final static String student_masterSecret = "oyfhlMe94v6Efr30kd3Ul7";




    public static final String Oss_Url_Prefix = "http://file.waijiaojun.com";

/*    public static class PaypalUrl{

 *//*       //测试环境
        public static final String payDetailUrl = "https://api.sandbox.paypal.com/v1/payments/payment/";
        public static final String oath2TokenUrl = "https://api.sandbox.paypal.com/v1/oauth2/token";*//*

        //正式环境
        public static final String payDetailUrl = "https://api.paypal.com/v1/payments/payment/";
        public static final String oath2TokenUrl = "https://api.paypal.com/v1/oauth2/token";
    }*/

    public static class DeviceType {
        public static final String Android = "1";
        public static final String IOS = "2";
        public static final String Weixin = "3";
    }

    //log 记录方法入惨的前缀
    public static final String prefix_in = "\nparameters in -->> ";
    //log 记录方法出惨的前缀
    public static final String prefix_out = "\nparameters out -->>";

    public static class Weike {
        /**
         * 微课默认的评分为5.0
         */
        public static final double default_score = 5.0;
        public static final BigDecimal default_student_price = new BigDecimal(0.0);
        public static final double default_teacher_earn_price = 0.0;
        public static final Integer default_category_id = 325;//口语

        public static class Approved {
            /**
             * 0 未审核
             */
            public static final Integer NONE = 0;
            /**
             * 1 审核不通过
             */
            public static final Integer UN_PASS = 1;
            /**
             * 2 通过
             */
            public static final Integer PASS = 2;
        }

        /**
         * 是否发布
         */
        public static class Published {
            /**
             * 0 未发布
             */
            public static final Integer NO = 0;
            /**
             * 1 已发布
             */
            public static final Integer YES = 1;
        }

        /**
         * 是否显示给学生
         */
        public static class Show {
            /**
             * 0 不显示
             */
            public static final Integer NO = 0;
            /**
             * 1 显示
             */
            public static final Integer YES = 1;
        }

        /**
         * 精品
         */
        public static class Boutique {
            /**
             * 0 不是精品
             */
            public static final Integer NO = 0;
            /**
             * 1 是精品
             */
            public static final Integer YES = 1;
        }

        /**
         * 今日精品
         */
        public static class TodayBoutique {
            /**
             * 0 不是精品
             */
            public static final Integer NO = 0;
            /**
             * 1 是精品
             */
            public static final Integer YES = 1;
        }

        /**
         * 最热微课
         */
        public static class Hot {
            /**
             * 0 不是
             */
            public static final Integer NO = 0;
            /**
             * 1 是
             */
            public static final Integer YES = 1;
        }

        public static class CtxType {
            public static final String PART = "part";
            public static final String IMAGE = "image";
            public static final String TEXT = "text";
            public static final String AUDIO = "audio";
        }
    }

    /**
     * 是否删除
     */
    public static class Deleted {
        /**
         * 0 使用
         */
        public static final Integer NO = 0;

        /**
         * 1删除
         */
        public static final Integer YES = 1;
    }


    /**
     * 支付渠道：0 余额支付；1 阿里支付；2 微信支付
     */
    public static class PaymentChannel {
        /**
         * 余额支付
         */
        public static final int wallet = 0;

        /**
         * 阿里支付
         */
        public static final int alipay = 1;

        /**
         * 微信支付
         */
        public static final int wxpay = 2;
        /**
         * 微信支付
         */
        public static final int paypal = 3;

        /**
         * 阿里支付
         */
        public static final String alipay_name = "alipay";

        /**
         * 微信支付
         */
        public static final String wxpay_name = "wx";

        /**
         * paypal支付
         */
        public static final String paypal_name = "paypal";
    }

    /**
     * 学生支付原因
     */
    public static class ChargeReason {
        /**
         * 充值
         */
        public static final int recharge_code = 0;
        public static final String recharge_msg = "充值";

        /**
         * 购买课程
         */
        public static final int buy_course_code = 1;
        public static final String buy_course_msg = "购买课程";

        /**
         * 取消预约
         */
        public static final int cancel_reserver_code = 2;
        public static final String cancel_reserver_msg = "取消预约";

        /**
         * 呼叫老师(闲聊)
         */
        public static final int call_teacher_free_talk_code = 3;
        public static final String call_teacher_free_talk_msg = "呼叫老师(闲聊)";

        /**
         * 其它(优惠码、周排行奖励、系统调整)
         */
        public static final int other_code = 4;
        public static final String other_msg = "其它(优惠码、周排行奖励、系统调整)";

        /**
         * 购买公开课
         */
        public static final int buy_live_course_code = 5;
        public static final String buy_live_course_msg = "购买公开课";

        /**
         * 充值付款失败，系统扣除
         */
        public static final int recharge_failure_code = 6;
        public static final String recharge_failure_msg = "充值付款失败，系统扣除";

        /**
         * 直播打赏
         */
        public static final int live_award_code = 7;
        public static final String live_award_msg = "直播打赏";

        /**
         * 购买微课
         */
        public static final int buy_weike_code = 8;
        public static final String buy_weike_msg = "购买微课";

        /**
         * 购买微课
         */
        public static final int buy_weike_vip_year = 9;
        public static final String buy_weike_vip_year_msg = "购买系统课年度会员";
    }

    /**
     * 支付状态 1转账中 2已付款 3 付款失败
     */
    public static class PayState {
        /**
         * 转账中
         */
        public static final String transfering = "1";
        public static final String transfering_msg = "转账中";

        /**
         * 已付款
         */
        public static final String success = "2";
        public static final String success_msg = "已付款";

        /**
         * 付款失败
         */
        public static final String failure = "3";
        public static final String failure_msg = "付款失败";

    }

    /**
     * 用户余额变化类型：1 充值（进入余额：+）；2 消费（余额支出：-）
     */
    public static class FundChangeType {
        /**
         * 1 充值（进入余额：+）
         */
        public static final String wallet_in = "1";

        /**
         * 2 消费（余额支出：-）
         */
        public static final String wallet_out = "2";
    }

    /**
     * 课程类型：weike微课，live公开课，直播课
     */
    public static class CourseType {
        public static final String weike = "weike";
        public static final String live = "live";
    }

    /**
     * 老师资金变化类型
     * 0:Freetalk 1:Appointment 2:Withdraw deposit 3:Others 4:live
     */
    public static class fundChangeTypeForTeacher {
        public static int freetalk_code = 0;
        public static String freetalk_msg = "Free talk class";

        public static int appointment_code = 1;
        public static String appointment_msg = "Appointment class";

        public static int drawDeposit_code = 2;
        public static String drawDeposit_msg = "Current deposit";

        public static int other_code = 3;
        public static String other_msg = "Other";

        public static int live_code = 4;
        public static String live_msg = "Live";

        public static int weike_code = 5;
        public static String weike_msg = "Audio Class";

    }

    /**
     * 老师资金流向类型（以用户在系统中的钱包为基准）：
     * 1流入（进入余额：+）；2流出（余额支出：-）
     */
    public static class FundFlowType {

        public static String in = "1";

        public static String out = "2";

    }

    /**
     * 预约状态
     */
    public static class ReserveState {
        //待上课
        public static final int ready_to_begin = 1;
        //已完成
        public static final int finished = 2;
        //已取消
        public static final int canceled = 3;
    }

    /**
     * 预约类型
     */
    public static class ReserveType {
        //试听预约
        public static final int listen_course = 1;
        //购买课程预约
        public static final int setmeal_course = 2;
    }

    /**
     * 预约时间表状态
     */
    public static class ReserveTimeState {
        //可用(open)
        public static final int open = 0;
        //不可用(closed)
        public static final int closed = 1;
        //被预约(booked)
        public static final int booked = 2;
    }

    public static class RoomState {
        // 教室关闭
        public static final int close = 1;

        // 教室打开
        public static final int open = 2;

        //上课已经结束，教室关闭
        public static final int end = 3;
    }

    public static class ClientOsType {
        /**
         * 客户端手机类型 1 android
         */
        public static final String android = "1";

        /**
         * 客户端手机类型 2 ios
         */
        public static final String ios = "2";
    }

    public static class ClassRoom {
        public static class RoomStatus {
            public static final int UN_START = 1;// 未开始
            public static final int OPEN = 2;// 开放
            public static final int END = 3;// 上完课后教室关闭
        }

        public static class InRoom {
            public static final int yes = 1;
            public static final int no = 0;
        }

        public static class EventType {
            public static final String enter = "enter";// 进入教室
            public static final String leave = "leave";// 离开教室
        }
    }

    public static class UserRole {
        public static final int student = 1;
        public static final int teacher = 2;
    }

    /**
     * 学生购买课程套餐，套餐总的支付状态 1 待付款 2待结束 3支付完成
     */
    public static class SetMealPayState {
        /**
         * 1 待付款
         */
        public static final int unpaid = 1;
        public static final String unpaid_msg = "待付款";

        /**
         * 待结束
         */
        public static final int un_end = 2;
        public static final String un_end_msg = "待结束";

        /**
         * 支付完成
         */
        public static final int paid = 3;
        public static final String paid_msg = "支付完成";

    }

    public static class isGifg {
        public static final int YES = 1;
        public static final int NO = 0;
    }

    public static class SalesPromotionSettingsCloseStatus {
        public static final int YES = 1;
        public static final int NO = 0;
    }

    /**
     * 短信接口使用的相关参数
     */
    public static class SMS253 {
        public static final String account = "N2551506";
        public static final String password = "UjGnvkEm0Jfaae";

        /**
         * 国内短信 api地址
         */
        public static final String url = "http://sms.253.com/msg/send";


        public static final String inter_account = "I3223831";
        public static final String inter_password = "jWdEByf6Vh2ec2";
        /**
         * 国际短信 api地址
         */
        public static final String inter_url = "http://intapi.253.com/send/json";

    }

}
