package com.benmei.weike.common;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

/**
 * Created by Micoo on 2017/4/19.
 */
public class Page<T> {
    @JsonIgnore
    private int currentPage;//当前页
    @JsonIgnore
    private int pageSize;//每页大小
    @JsonIgnore
    private int total;//总记录数

    private int totalPage;
    private List<T> data;

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public Page(int pageSize, int total) {
        this.pageSize = pageSize;
        this.total = total;
    }

    public Page(int currentPage, int pageSize, int total) {
        this.currentPage = currentPage;
        this.pageSize = pageSize;
        this.total = total;
    }

    /**
     * 获取总页数
     * @return
     */
    public int getTotalPage() {
        int totalPage = 0;
        if (total == 0) {
            return totalPage;
        }
        if (pageSize == 0) {
            return totalPage;
        }

        if (total % pageSize > 0) {
            totalPage = total / pageSize + 1;
        } else {
            totalPage = total / pageSize;
        }

        return totalPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
