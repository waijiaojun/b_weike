package com.benmei.weike.common;

/**
 * Created by Peter on 2017/8/21.
 * recharge 余额充值；course 购买课程；sys_course 购买系统课；sys_course_vip_year 购买系统课VIP；appointment_cancel 取消预约扣费；
 */
public enum GoodsCategory {

    COURSE("course", "Purchase course", "", PaymentConstants.ActionType.OUT), //1v1课程

    SYS_COURSE("sys_course", "Purchase systematic course", "", PaymentConstants.ActionType.OUT),// 系统课

    SYS_COURSE_VIP_YEAR("sys_course_vip_year", "Purchase VIP", "", PaymentConstants.ActionType.OUT),// 系统课VIP,包年

    APPOINTMENT_CANCEL("appointment_cancel", "Cancellation fee", "", PaymentConstants.ActionType.OUT),// 余额充值

    RECHARGE("recharge", "Top up", "", PaymentConstants.ActionType.IN),// 余额充值

    RECHARGE_GIFT("recharge_gift", "Prepaid gift", "", PaymentConstants.ActionType.IN),// 充值赠送

    OTHER("other", "Other", "", PaymentConstants.ActionType.IN);// 其他

    public static final String recharge_gift = "recharge_gift";

    private final String code;
    private final String name;
    private final String icon;
    private final Integer inOut;

    GoodsCategory(String code, String name, String icon, Integer inOut) {
        this.code = code;
        this.name = name;
        this.icon = icon;
        this.inOut = inOut;
    }

    public static GoodsCategory getByCode(String code) {
        GoodsCategory result = null;
        for (GoodsCategory goodsCategory : GoodsCategory.values()) {
            if (goodsCategory.code.equals(code)) {
                result = goodsCategory;
                break;
            }
        }
        return result;
    }

    public static boolean contains(String code) {
        boolean result = false;
        for (GoodsCategory goodsType : GoodsCategory.values()) {
            if (goodsType.code.equals(code)) {
                result = true;
                break;
            }
        }
        return result;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getIcon() {
        return icon;
    }

    public Integer getInOut() {
        return inOut;
    }


}
