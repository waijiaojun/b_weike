package com.benmei.weike.common;

/**
 * Created by Micoo on 2017/4/20.
 */
public enum Lang{
    /**
     *英文
     */
    EN,
    /**
     * 中文
     */
    ZH
}