package com.benmei.weike.common;

/**
 * Created by Peter on 2017/11/23.
 * 支付第三方
 */
public enum PayThirdPart {
    // Ping++聚合支付
    PingPlusPlus("Ping++"),

    // 微信支付
    WeChatPay("WeChat Pay");

    private final String name;


    PayThirdPart(String name) {
        this.name = name;

    }


    public String getName() {
        return name;
    }


    public static PayThirdPart getByName(String name) {
        PayThirdPart result = null;
        for (PayThirdPart thirdPart : PayThirdPart.values()) {
            if (thirdPart.getName().equals(name)) {
                result = thirdPart;
            }
        }
        return result;
    }

    public static boolean contains(String code) {
        boolean result = false;
        for (PayChannel ayChannel : PayChannel.values()) {
            if (ayChannel.getName().equals(code)) {
                result = true;
                break;
            }
        }
        return result;
    }
}
