package com.benmei.weike.common;

/**
 * Created by Peter on 2017/9/21.
 * wallet 余额支付；multiple 余额 + P++ 组合支付；thirdpart 第三方支付：P++支付
 */
public enum PayType {


    wallet("wallet", "余额支付"),
    multiple("multiple", "余额 + P++ 组合支付"),
    thirdpart("thirdpart", "第三方支付：P++支付"),
    system("system", "系统支付");

    private final String name;

    private final String desc;

    public static final String WALLET = "wallet";
    public static final String MULTIPLE = "multiple";
    public static final String THIRDPART = "thirdpart";
    public static final String SYSTEM = "system";


    PayType(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }

    public static PayType getByCode(String name) {
        PayType result = null;
        for (PayType payType : PayType.values()) {
            if (payType.name.equals(name)) {
                result = payType;
            }
        }
        return result;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public static boolean contains(String name) {
        boolean result = false;
        for (PayType payType : PayType.values()) {
            if (payType.name.equals(name)) {
                result = true;
                break;
            }
        }
        return result;
    }
}
