package com.benmei.weike.web;

import com.benmei.weike.common.Constants;
import com.benmei.weike.common.Lang;
import com.benmei.weike.dto.*;
import com.benmei.weike.dto.weikeContentResponse.WeikeContentResponse;
import com.benmei.weike.entity.Weike;
import com.benmei.weike.entity.WeikeContent;
import com.benmei.weike.entity.WeikeImage;
import com.benmei.weike.entity.WeikePart;
import com.benmei.weike.exception.ClientException;
import com.benmei.weike.service.WeikePartService;
import com.benmei.weike.service.WeikeService;
import com.benmei.weike.service.common.MemcachedService;
import com.benmei.weike.util.JsonUtil;
import com.nativetalk.base.RetInfo;
import com.nativetalk.bean.teacher.TdNtTeacherResult;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 老师端 微课接口
 */
@RestController
@RequestMapping(value = "/v4.2/teacher")
public class WeikeTeacherController {

    public static final Logger logger = LoggerFactory.getLogger(WeikeTeacherController.class);

    //log 记录方法入惨的前缀
    private static final String prefix_in = "\nparameters in -->> ";
    //log 记录方法出惨的前缀
    private static final String prefix_out = "\nparameters out -->>";

    @Autowired
    private MemcachedService memcachedService;

    @Autowired
    private WeikeService weikeService;

    @Resource
    private WeikePartService weikePartService;

    /**
     * 老师端查询微课列表 老师-微课首页
     *
     * @return
     */
    @RequestMapping(value = "/teatoken/list", method = {RequestMethod.POST})
    @ResponseBody
    public RetInfo teacherWeikeList(@RequestBody TeacherWeikeListRequest req, @RequestHeader(value = "token") String token) {
        try {
            logger.info(prefix_in + JsonUtil.toJson(req));
            //校验token，如果校验通过就从缓存中读取Teacher
            if (StringUtils.isBlank(token)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("token is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            Object o = memcachedService.get(token);
            if (o == null) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存中没有teatoken:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }

            if (!TdNtTeacherResult.class.isInstance(o)) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存的token不是TdNtTeacherResult类型:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            TdNtTeacherResult tdNtTeacherResult = (TdNtTeacherResult) o;

            //参数校验
            if (req == null) {
                return RetInfo.getClientErrorInfo("Parameter is required");
            }
            if (req.getStatus() == null) {
                return RetInfo.getClientErrorInfo("Parameter 'status' is required");
            }


            TeacherWeikeListResponse obj = weikeService.teacherWeikeList(req, tdNtTeacherResult.getTea_id());

            RetInfo retInfo = RetInfo.getSuccessInfo();
            retInfo.setObj(obj);
            logger.info(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        } catch (ClientException e) {
            RetInfo retInfo =  RetInfo.getClientErrorInfo(e.getMessage());
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        } catch (Exception e) {
            RetInfo retInfo = RetInfo.getServerErrorInfo(Lang.EN);
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }

    }

    /**
     * <b>注意：</b>
     * <p>
     * <i>You should NEVER set that header yourself.
     * We set the header properly with the boundary.
     * If you set that header, we won't and your server won't know what boundary to expect (since it is added to the header).
     * Remove your custom Content-Type header and you'll be fine.</i><br>
     * 当使用postman做接口测试时，如果你想上传附件，那么请不要在header中设置content-type。
     * </p>
     * 老师端创建微课
     *
     * @return
     */
    @RequestMapping(value = "/teatoken/create", method = {RequestMethod.POST})
    @ResponseBody
    public RetInfo teacherCreateWeike(MultipartHttpServletRequest request, @RequestHeader(value = "token") String token) {
        try {
            logger.info(prefix_in);
            //校验token，如果校验通过就从缓存中读取Teacher
            if (StringUtils.isBlank(token)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("token  is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            Object o = memcachedService.get(token);
            if (o == null) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存中没有teatoken:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }

            if (!TdNtTeacherResult.class.isInstance(o)) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存的token不是TdNtTeacherResult类型:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            TdNtTeacherResult tdNtTeacherResult = (TdNtTeacherResult) o;

            String title = request.getParameter("title");
            String intro = request.getParameter("intro");
            List<MultipartFile> files = request.getFiles("cover");
            //参数校验
            if (StringUtils.isBlank(title)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'title' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (StringUtils.isBlank(intro)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'intro' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (files == null || files.size() == 0) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'cover' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (files.get(0).getSize() == 0) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'cover' size not be zero");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }

            TeacherWeikeCreateRequest req = new TeacherWeikeCreateRequest();
            req.setCover(files.get(0));
            req.setIntro(intro);
            req.setTitle(title);
            Weike weike = weikeService.teacherCreateWeike(req, tdNtTeacherResult.getTea_id());

            RetInfo retInfo = RetInfo.getSuccessInfo();
            retInfo.setObj(weike);
            logger.info(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        } catch (Exception e) {
            RetInfo retInfo = RetInfo.getServerErrorInfo(Lang.EN);
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }

    }


    /**
     * 更新微课
     *
     * @param request
     * @param token
     * @return
     */
    @RequestMapping(value = "/teatoken/update", method = {RequestMethod.POST})
    @ResponseBody
    public RetInfo teacherUpdateWeike(MultipartHttpServletRequest request, @RequestHeader(value = "token") String token) {
        try {
            logger.info(prefix_in);
            //校验token，如果校验通过就从缓存中读取Teacher
            if (StringUtils.isBlank(token)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter token  is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            Object o = memcachedService.get(token);
            if (o == null) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存中没有teatoken:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }

            if (!TdNtTeacherResult.class.isInstance(o)) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存的token不是TdNtTeacherResult类型:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            TdNtTeacherResult tdNtTeacherResult = (TdNtTeacherResult) o;

            //校验 业务参数
            String str_id = request.getParameter("id");
            String title = request.getParameter("title");
            String intro = request.getParameter("intro");
            List<MultipartFile> files = request.getFiles("cover");
            if (StringUtils.isBlank(str_id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'id' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!StringUtils.isNumeric(str_id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'id' must be numeric");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (StringUtils.isBlank(title)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'title' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (StringUtils.isBlank(intro)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'intro' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }

            MultipartFile file = null;
            if (files != null && files.size() > 0 && files.get(0).getSize() > 0) {
                file = files.get(0);
            }


            TeacherWeikeCreateRequest req = new TeacherWeikeCreateRequest();
            req.setCover(file);
            req.setId(Long.parseLong(str_id));
            req.setIntro(intro);
            req.setTitle(title);

            Weike weike = weikeService.teacherUpdateWeike(req, tdNtTeacherResult.getTea_id());

            RetInfo retInfo = RetInfo.getSuccessInfo(weike);
            logger.info(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        } catch (ClientException e) {
            RetInfo retInfo = RetInfo.getClientErrorInfo(e.getMessage());
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        } catch (Exception e) {
            RetInfo retInfo = RetInfo.getServerErrorInfo(Lang.EN);
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }

    }

    /**
     * 创建微课章节
     * @param token
     * @param request
     * @return
     */
    @RequestMapping(value = "/part/add",method = {RequestMethod.POST})
    @ResponseBody
    public RetInfo teacherAddPart(@RequestHeader(value = "token") String token,@RequestBody AddWeikePartDto request){
        try{
            logger.info(prefix_in);
            //校验token，如果校验通过就从缓存中读取Teacher
            if (StringUtils.isBlank(token)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("token  is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            Object o = memcachedService.get(token);
            if (o == null) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存中没有teatoken:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!TdNtTeacherResult.class.isInstance(o)) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存的token不是TdNtTeacherResult类型:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            TdNtTeacherResult tdNtTeacherResult = (TdNtTeacherResult) o;
            //参数校验
            String weike_id=request.getWeike_id();
            String title=request.getTitle();

            if (StringUtils.isBlank(weike_id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'weike_id' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!StringUtils.isNumeric(weike_id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'weike_id' must be numeric");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (StringUtils.isBlank(title)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'title' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            //执行
            WeikePart weikePart=weikePartService.insertPart(weike_id,title,tdNtTeacherResult.getTea_id());

            RetInfo retInfo = RetInfo.getSuccessInfo();
            retInfo.setObj(weikePart);
            logger.info(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }catch (Exception e) {
            RetInfo retInfo = RetInfo.getServerErrorInfo(Lang.EN);
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }
    }

    /**
     * 更新章节标题
     * @param token
     * @param request
     * @return
     */
    @RequestMapping(value = "/part/update",method = {RequestMethod.POST})
    @ResponseBody
    public RetInfo teacherUpdatePart(@RequestHeader(value = "token") String token,@RequestBody UpdateWeikePartDto request) {
        try{
            logger.info(prefix_in);
            //校验token，如果校验通过就从缓存中读取Teacher
            if (StringUtils.isBlank(token)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("token  is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            Object o = memcachedService.get(token);
            if (o == null) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存中没有teatoken:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!TdNtTeacherResult.class.isInstance(o)) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存的token不是TdNtTeacherResult类型:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            TdNtTeacherResult tdNtTeacherResult = (TdNtTeacherResult) o;
            //参数校验
            String id=request.getId();
            String title=request.getTitle();

            if (StringUtils.isBlank(id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'id' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!StringUtils.isNumeric(id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'id' must be numeric");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (StringUtils.isBlank(title)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'title' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            //执行
            WeikePart weikePart=weikePartService.updatePart(id,title,tdNtTeacherResult.getTea_id());

            RetInfo retInfo = RetInfo.getSuccessInfo();
            retInfo.setObj(weikePart);
            logger.info(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }catch (Exception e) {
            RetInfo retInfo = RetInfo.getServerErrorInfo(Lang.EN);
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }
    }

    /**
     * 删除章节
     * @param token
     * @param request
     * @return
     */
    @RequestMapping(value = "/part/delete",method = {RequestMethod.POST})
    @ResponseBody
    public RetInfo teacherDeletePart(@RequestHeader(value = "token") String token,@RequestBody UpdateWeikePartDto request) {
        try{
            logger.info(prefix_in);
            //校验token，如果校验通过就从缓存中读取Teacher
            if (StringUtils.isBlank(token)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("token  is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            Object o = memcachedService.get(token);
            if (o == null) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存中没有teatoken:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!TdNtTeacherResult.class.isInstance(o)) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存的token不是TdNtTeacherResult类型:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            TdNtTeacherResult tdNtTeacherResult = (TdNtTeacherResult) o;
            //参数校验
            String id=request.getId();

            if (StringUtils.isBlank(id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'id' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!StringUtils.isNumeric(id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'id' must be numeric");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            //执行
            WeikePart weikePart=weikePartService.deletePart(id,tdNtTeacherResult.getTea_id());
            RetInfo retInfo = RetInfo.getSuccessInfo();
            retInfo.setObj(weikePart);
            logger.info(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }catch (Exception e) {
            RetInfo retInfo = RetInfo.getServerErrorInfo(Lang.EN);
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }
    }

    /**
     * 创建章节内容(插入文字)
     */
    @RequestMapping(value = "/partContent/addText",method = {RequestMethod.POST})
    @ResponseBody
    public RetInfo teacherAddPartContentText(@RequestHeader(value = "token") String token,@RequestBody AddWeikePartTextDto request){
        try{
            logger.info(prefix_in);
            //校验token，如果校验通过就从缓存中读取Teacher
            if (StringUtils.isBlank(token)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("token  is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            Object o = memcachedService.get(token);
            if (o == null) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存中没有teatoken:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!TdNtTeacherResult.class.isInstance(o)) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存的token不是TdNtTeacherResult类型:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            TdNtTeacherResult tdNtTeacherResult = (TdNtTeacherResult) o;
            WeikeContent weikeContent=new WeikeContent();
            //参数校验
            String weike_id=request.getWeike_id();
            String weike_part_sort=request.getWeike_part_sort();
            if (StringUtils.isBlank(weike_id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'weike_id' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!StringUtils.isNumeric(weike_id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'weike_id' must be numeric");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (StringUtils.isBlank(weike_part_sort)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'weike_part_sort' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!StringUtils.isNumeric(weike_part_sort)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'weike_part_sort' must be numeric");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }

            String text=request.getContent();
            if (StringUtils.isBlank(text)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'text' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            weikeContent=weikeService.teacherAddText("text",weike_id,weike_part_sort,text,tdNtTeacherResult.getTea_id());

            RetInfo retInfo = RetInfo.getSuccessInfo();
            retInfo.setObj(weikeContent);
            logger.info(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }catch (Exception e) {
            RetInfo retInfo = RetInfo.getServerErrorInfo(Lang.EN);
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }
    }

    /**
     * 创建章节内容(插入语音)
     */
    @RequestMapping(value = "/partContent/addAudio",method = {RequestMethod.POST})
    @ResponseBody
    public RetInfo teacherAddPartContentAudio(@RequestHeader(value = "token") String token,MultipartHttpServletRequest request){
        try{
            logger.info(prefix_in);
            //校验token，如果校验通过就从缓存中读取Teacher
            if (StringUtils.isBlank(token)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("token  is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            Object o = memcachedService.get(token);
            if (o == null) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存中没有teatoken:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!TdNtTeacherResult.class.isInstance(o)) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存的token不是TdNtTeacherResult类型:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            TdNtTeacherResult tdNtTeacherResult = (TdNtTeacherResult) o;
            WeikeContent weikeContent=new WeikeContent();
            //参数校验
            String weike_id=request.getParameter("weike_id");
            String weike_part_sort=request.getParameter("weike_part_sort");
            if (StringUtils.isBlank(weike_id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'weike_id' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!StringUtils.isNumeric(weike_id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'weike_id' must be numeric");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (StringUtils.isBlank(weike_part_sort)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'weike_part_sort' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!StringUtils.isNumeric(weike_part_sort)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'weike_part_sort' must be numeric");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            MultipartFile audio = request.getFile("content");
            String duration=request.getParameter("duration").trim();
            if (audio == null || audio.getSize() == 0) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'audio' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!StringUtils.isNumeric(duration)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'duration' must be numeric");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            weikeContent=weikeService.teacherAddAudio("audio",weike_id,weike_part_sort,audio,duration,tdNtTeacherResult.getTea_id());

            RetInfo retInfo = RetInfo.getSuccessInfo();
            retInfo.setObj(weikeContent);
            logger.info(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }catch (Exception e) {
            RetInfo retInfo = RetInfo.getServerErrorInfo(Lang.EN);
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }
    }



    /**
     * 修改章节内容(插入语音，文字)
     */
    @RequestMapping(value = "/partContent/update",method = {RequestMethod.POST})
    @ResponseBody
    public RetInfo teacherUpdatePartContent(@RequestHeader(value = "token") String token,MultipartHttpServletRequest request){
        try{
            logger.info(prefix_in);
            //校验token，如果校验通过就从缓存中读取Teacher
            if (StringUtils.isBlank(token)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("token  is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            Object o = memcachedService.get(token);
            if (o == null) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存中没有teatoken:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!TdNtTeacherResult.class.isInstance(o)) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存的token不是TdNtTeacherResult类型:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            TdNtTeacherResult tdNtTeacherResult = (TdNtTeacherResult) o;
            WeikeContent weikeContent=new WeikeContent();
            //参数校验
            String type=request.getParameter("type");
            String id=request.getParameter("id");
            if (StringUtils.isBlank(id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'id' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!StringUtils.isNumeric(id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'id' must be numeric");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (StringUtils.isBlank(type)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'type' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if(type.equals("text")){
                String text=request.getParameter("content");
                if (StringUtils.isBlank(text)) {
                    RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'text' is required");
                    logger.info(JsonUtil.toJson(retInfo));
                    return retInfo;
                }
                weikeContent=weikeService.teacherUpdateText(id,text,type,tdNtTeacherResult.getTea_id());
            }else if(type.equals("audio")){
                MultipartFile audio = request.getFile("content");
                String weike_id=request.getParameter("weike_id");
                String duration=request.getParameter("duration").trim();
                if (StringUtils.isBlank(weike_id)) {
                    RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'weike_id' is required");
                    logger.info(JsonUtil.toJson(retInfo));
                    return retInfo;
                }
                if (!StringUtils.isNumeric(weike_id)) {
                    RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'weike_id' must be numeric");
                    logger.info(JsonUtil.toJson(retInfo));
                    return retInfo;
                }
                if (audio == null || audio.getSize() == 0) {
                    RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'audio' is required");
                    logger.info(JsonUtil.toJson(retInfo));
                    return retInfo;
                }
                if (!StringUtils.isNumeric(duration)) {
                    RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'duration' must be numeric");
                    logger.info(JsonUtil.toJson(retInfo));
                    return retInfo;
                }
                weikeContent=weikeService.teacherUpdateAudio(id,weike_id,type,audio,duration,tdNtTeacherResult.getTea_id());
            }
            RetInfo retInfo = RetInfo.getSuccessInfo();
            retInfo.setObj(weikeContent);
            logger.info(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }catch (Exception e) {
            RetInfo retInfo = RetInfo.getServerErrorInfo(Lang.EN);
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }
    }

    /**
     * 删除章节内容
     * @param token
     * @param request
     * @return
     */
    @RequestMapping(value = "/partContent/delete",method = {RequestMethod.GET})
    @ResponseBody
    public RetInfo teacherDeletePartContent(@RequestHeader(value = "token") String token,HttpServletRequest request)
    {
        try{
            logger.info(prefix_in);
            //校验token，如果校验通过就从缓存中读取Teacher
            if (StringUtils.isBlank(token)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("token  is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            Object o = memcachedService.get(token);
            if (o == null) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存中没有teatoken:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!TdNtTeacherResult.class.isInstance(o)) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存的token不是TdNtTeacherResult类型:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            TdNtTeacherResult tdNtTeacherResult = (TdNtTeacherResult) o;
            WeikeContent weikeContent=new WeikeContent();
            //参数校验
            String id=request.getParameter("id");
            if (StringUtils.isBlank(id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'id' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!StringUtils.isNumeric(id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'id' must be numeric");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            String obj=weikeService.teacherDeletePartContent(id,tdNtTeacherResult.getTea_id());
            RetInfo retInfo = RetInfo.getSuccessInfo();
            retInfo.setObj(obj);
            logger.info(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }catch (Exception e) {
            RetInfo retInfo = RetInfo.getServerErrorInfo(Lang.EN);
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }
    }

    /**
     * 上传ppt
     * @param token
     * @param request
     * @return
     */
    @RequestMapping(value = "/partImage/upload",method = {RequestMethod.POST})
    @ResponseBody
    public RetInfo teacherUploadPartImage(@RequestHeader(value = "token") String token,MultipartHttpServletRequest request) {
        try {
            logger.info(prefix_in);
            //校验token，如果校验通过就从缓存中读取Teacher
            if (StringUtils.isBlank(token)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("token  is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            Object o = memcachedService.get(token);
            if (o == null) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存中没有teatoken:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!TdNtTeacherResult.class.isInstance(o)) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存的token不是TdNtTeacherResult类型:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            TdNtTeacherResult tdNtTeacherResult = (TdNtTeacherResult) o;
            String weike_id = request.getParameter("weike_id");
            String weike_part_sort = request.getParameter("weike_part_sort");
            if (StringUtils.isBlank(weike_id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'weike_id' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!StringUtils.isNumeric(weike_id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'weike_id' must be numeric");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (StringUtils.isBlank(weike_part_sort)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'weike_part_sort' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!StringUtils.isNumeric(weike_part_sort)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'weike_part_sort' must be numeric");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            List<MultipartFile> files = request.getFiles("file");
            if(files==null || files.size()==0){
                RetInfo retInfo = RetInfo.getClientErrorInfo("Error: file size can not be 0");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            for(MultipartFile file:files){
                if (file == null || file.getSize() == 0) {
                    RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'file' is required");
                    logger.info(JsonUtil.toJson(retInfo));
                    return retInfo;
                }
            }
            List<WeikeImage> weikeImage=weikeService.uploadImage(weike_id,weike_part_sort,tdNtTeacherResult.getTea_id(),files);
            RetInfo retInfo = RetInfo.getSuccessInfo();
            retInfo.setObj(weikeImage);
            logger.info(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }catch (Exception e){
            RetInfo retInfo = RetInfo.getServerErrorInfo(Lang.EN);
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }
    }

    /**
     * 删除ppt图片
     * @param token
     * @param request
     * @return
     */
    @RequestMapping(value = "/partImage/delete",method = {RequestMethod.GET})
    @ResponseBody
    public RetInfo teacherDeletePartImage(@RequestHeader(value = "token") String token,HttpServletRequest request) {
        try {
            logger.info(prefix_in);
            //校验token，如果校验通过就从缓存中读取Teacher
            if (StringUtils.isBlank(token)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("token  is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            Object o = memcachedService.get(token);
            if (o == null) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存中没有teatoken:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!TdNtTeacherResult.class.isInstance(o)) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存的token不是TdNtTeacherResult类型:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            TdNtTeacherResult tdNtTeacherResult = (TdNtTeacherResult) o;
            String id = request.getParameter("id");
            if (StringUtils.isBlank(id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'id' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!StringUtils.isNumeric(id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'id' must be numeric");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            WeikeImage weikeImage=weikeService.deleteImage(id,tdNtTeacherResult.getTea_id());
            RetInfo retInfo = RetInfo.getSuccessInfo();
            retInfo.setObj(weikeImage);
            logger.info(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }catch (Exception e){
            RetInfo retInfo = RetInfo.getServerErrorInfo(Lang.EN);
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }
    }

    /**
     * 根据url删除ppt图片
     * @param token
     * @param request
     * @return
     */
    @RequestMapping(value = "/partImage/deleteByUrl",method = {RequestMethod.GET})
    @ResponseBody
    public RetInfo teacherDeletePartImageByUrl(@RequestHeader(value = "token") String token,HttpServletRequest request) {
        try {
            logger.info(prefix_in);
            //校验token，如果校验通过就从缓存中读取Teacher
            if (StringUtils.isBlank(token)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("token  is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            Object o = memcachedService.get(token);
            if (o == null) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存中没有teatoken:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!TdNtTeacherResult.class.isInstance(o)) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存的token不是TdNtTeacherResult类型:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            TdNtTeacherResult tdNtTeacherResult = (TdNtTeacherResult) o;
            String url = request.getParameter("url");
            if (StringUtils.isBlank(url)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'url' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            WeikeImage weikeImage=weikeService.deleteImageByUrl(url,tdNtTeacherResult.getTea_id());
            RetInfo retInfo = RetInfo.getSuccessInfo();
            retInfo.setObj(weikeImage);
            logger.info(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }catch (Exception e){
            RetInfo retInfo = RetInfo.getServerErrorInfo(Lang.EN);
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }
    }


    /**
     * 移动PPT位置
     * @param token
     * @param request
     * @return
     */
    @RequestMapping(value = "/partImage/moveImage",method = {RequestMethod.POST})
    @ResponseBody
    public RetInfo teacherupdatePartImage(@RequestHeader(value = "token") String token,@RequestBody PartImageDto request) {
        try {
            logger.info(prefix_in);
            //校验token，如果校验通过就从缓存中读取Teacher
            if (StringUtils.isBlank(token)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("token  is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            Object o = memcachedService.get(token);
            if (o == null) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存中没有teatoken:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!TdNtTeacherResult.class.isInstance(o)) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存的token不是TdNtTeacherResult类型:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            TdNtTeacherResult tdNtTeacherResult = (TdNtTeacherResult) o;
            String id = request.getId();
            String oldSort=request.getOldSort();
            String sort=request.getSort();
            if (StringUtils.isBlank(id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'id' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!StringUtils.isNumeric(id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'id' must be numeric");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (StringUtils.isBlank(oldSort)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'oldSort' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!StringUtils.isNumeric(oldSort)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'oldSort' must be numeric");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (StringUtils.isBlank(sort)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'sort' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!StringUtils.isNumeric(sort)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'sort' must be numeric");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            String weikeImage=weikeService.moveImageSort(id,oldSort,sort,tdNtTeacherResult.getTea_id());
            RetInfo retInfo = RetInfo.getSuccessInfo();
            retInfo.setObj(weikeImage);
            logger.info(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }catch (Exception e){
            RetInfo retInfo = RetInfo.getServerErrorInfo(Lang.EN);
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }
    }

    /**
     * 审核微课
     * @param token
     * @param request
     * @return
     */

    @RequestMapping(value = "/weike/submitApprove",method = {RequestMethod.GET})
    @ResponseBody
    public RetInfo submitWeike(@RequestHeader(value = "token") String token,HttpServletRequest request) {
        try {
            logger.info(prefix_in);
            //校验token，如果校验通过就从缓存中读取Teacher
            if (StringUtils.isBlank(token)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("token  is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            Object o = memcachedService.get(token);
            if (o == null) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存中没有teatoken:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!TdNtTeacherResult.class.isInstance(o)) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存的token不是TdNtTeacherResult类型:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            TdNtTeacherResult tdNtTeacherResult = (TdNtTeacherResult) o;
            String weike_id = request.getParameter("weike_id");
            if (StringUtils.isBlank(weike_id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'weike_id' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!StringUtils.isNumeric(weike_id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'weike_id' must be numeric");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            weikeService.teacherSubmitWeike(weike_id,tdNtTeacherResult.getTea_id());
            RetInfo retInfo = RetInfo.getSuccessInfo();
            logger.info(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }catch (Exception e){
            RetInfo retInfo = RetInfo.getServerErrorInfo(Lang.EN);
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }
    }

    /**
     * 发布微课
     * @param token
     * @param request
     * @return
     */
    @RequestMapping(value = "/weike/publish",method = {RequestMethod.GET})
    @ResponseBody
    public RetInfo publishWeike(@RequestHeader(value = "token") String token,HttpServletRequest request) {
        try {
            logger.info(prefix_in);
            //校验token，如果校验通过就从缓存中读取Teacher
            if (StringUtils.isBlank(token)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("token  is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            Object o = memcachedService.get(token);
            if (o == null) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存中没有teatoken:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!TdNtTeacherResult.class.isInstance(o)) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存的token不是TdNtTeacherResult类型:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            TdNtTeacherResult tdNtTeacherResult = (TdNtTeacherResult) o;
            String weike_id = request.getParameter("weike_id");
            if (StringUtils.isBlank(weike_id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'weike_id' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!StringUtils.isNumeric(weike_id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'weike_id' must be numeric");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            weikeService.uploadWeike(weike_id,tdNtTeacherResult.getTea_id());
            RetInfo retInfo = RetInfo.getSuccessInfo("发布微课成功");
            logger.info(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }catch (Exception e){
            RetInfo retInfo = RetInfo.getServerErrorInfo(Lang.EN);
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }
    }



        /**
         * 发布微课
         * 保存微课的具体内容，即课程内容。(目前页面上的逻辑是：当用户保存内容就是发布微课，因此当用户保存的时候需要更改微课状态为：已发布)
         * <br>微课的课程内容包含：课程ppt(图片格式)、课程part，课程part中包含语音和文本内容，就像聊天一样顺序发送语音或者文本消息。
         * 微课内容一次性上传，因此这个接口接收的数据会有很多附件内容，包括语音和图片，并且其标题、图片、语音和文本的数量不确定，因此需要对表单字段命名进行约定。<br>
         * <p>字段名称格式约定：字段名称用下划线分为四部分<b>A_B_C_D</b></p>
         * <p>
         * <b>A</b> 表示类型（part、image、audio、text）<br>
         * <b>B</b> 表示所属part        int类型<br>
         * <b>C</b> 表示序号，排序    int类型<br>
         * <b>D</b> 表示录音的时长（秒）  int类型<br>
         * </p>
         * <p>e.g.</p>
         * <p>part_1 表示课程的第1部分，属于part1，序号为1</p>
         * <p>image_1_1 标识课程ppt，属于part1，序号为1</p>
         * <p>audio_1_1_120    语音内容，属于part1，序号为1，录音时长为120秒</p>
         * <p>text_1_1 文本内容，属于part1，序号为1</p>
         *
         * @param request
         * @param token
         * @return <code>
         * {
         * images[{"sort":1,"part":1,"url":"xxx/xxx.jpg"},...],
         * parts[{"sort":1,"title":"段落标题","contents":[
         * {"id":1,"sort":1,"type":"audio",:"content":"xxx/sss.wav","duration":320,"play_number":0},
         * {"id":2,"sort":2,"type":"text","content":"This content very good.","duration":0,"play_number":0}]},{}...]
         * }
         * </code>
         */
    @RequestMapping(value = "/teatoken/save/content/Deprecated", method = {RequestMethod.POST})
    @ResponseBody
    public RetInfo teacherSaveWeikeContent(MultipartHttpServletRequest request, @RequestHeader(value = "token") String token) {
        try {
            logger.info(prefix_in);

            //校验token，如果校验通过就从缓存中读取Teacher
            if (StringUtils.isBlank(token)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter token is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            Object o = memcachedService.get(token);
            if (o == null) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                String msg = "缓存中没有teatoken:[" + token + "]，用户需要重新登录系统。 retInfo-->" + retInfo;
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!TdNtTeacherResult.class.isInstance(o)) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                String msg = "缓存的token不是TdNtTeacherResult类型:[" + token + "]，用户需要重新登录系统。 retInfo-->" + retInfo;
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            TdNtTeacherResult tdNtTeacherResult = (TdNtTeacherResult) o;

            //校验 业务参数 id
            String str_id = request.getParameter("id");
            if (StringUtils.isBlank(str_id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'id' is required");
                return retInfo;
            }
            if (!StringUtils.isNumeric(str_id)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'id' must be numeric");
                return retInfo;
            }
            Long weikeId = Long.parseLong(str_id);//微课id

            //校验微课是否已经发布：用户发布过微课有，有可能仍然在发布页面，并且该页面没有以前保存的内容，用户以为没有保存成功，再次上传微课内容，导致微课内容重复，因此这里校验微课是否已经发布过了。
            if(weikeService.isPublished(weikeId)){
                RetInfo retInfo = RetInfo.getClientErrorInfo("Audio class is released");
                return retInfo;
            }

            //提取所有字段的名称(text和file两种类型都包括)
            StringBuffer sb = new StringBuffer("\n --------表单字段名称---------\n ");
            List<String> names = new ArrayList<>();
            Enumeration<String> parameterNames = request.getParameterNames();//文本字段
            while (parameterNames.hasMoreElements()) {
                String name = parameterNames.nextElement();
                names.add(name);
                sb.append(name + "\n");
            }
            sb.append(" --- files name --- \n");

            Iterator<String> fileNames = request.getFileNames();//文件字段
            while (fileNames.hasNext()) {
                String name = fileNames.next();
                names.add(name);
                sb.append(name + "\n");
            }
            logger.info(sb.toString());

            //校验参数名称的格式是否符合A_B_C_D的约定，如果名称符合约定，并且将数据封装成对象并放入对应的list
            List<WeikePart> parts = new ArrayList<>();
            List<WeikeImage> pictures = new ArrayList<>();
            List<WeikeContent> contents = new ArrayList<>();
            for (String name : names) {
                if (!"id".equals(name.trim())) {
                    processNames(request, name, parts, pictures, contents, tdNtTeacherResult.getTea_id(), weikeId);
                }
            }

            if (parts.size() == 0) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'Part' last need 1");
                return retInfo;
            }
            //可以不上传图片
           /* if (pictures.size() == 0) {
                return RetInfo.getClientErrorInfo("Parameter 'Image' last need 1");
            }*/
            if (contents.size() == 0) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'Content' last need 1");

            }

            weikeService.saveContents(weikeId, parts, pictures, contents,tdNtTeacherResult.getTea_id());

            RetInfo retInfo = RetInfo.getSuccessInfo();
            logger.info(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        } catch (ClientException e) {
            RetInfo retInfo = RetInfo.getClientErrorInfo(e.getMessage());
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        } catch (Exception e) {
            RetInfo retInfo = RetInfo.getServerErrorInfo(Lang.EN);
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }

    }


    /**
     * text_1_2
     * part_1
     * image_1_2
     * audio_1_1
     * 格式校验，并且返回A段，可以根据A段对字段类型做判断
     *
     * @param name
     * @return
     */
    private String processNames(MultipartHttpServletRequest request,
                                String name, List<WeikePart> parts,
                                List<WeikeImage> images,
                                List<WeikeContent> contents,
                                Integer tea_id, Long weikeId) throws ClientException {
        //1. 校验名称是否符合4段约束
        String[] _4_parts = name.split("_");
        if (_4_parts == null) {
            throw new ClientException("字段名称格式错误：名称解析为null，name=" + name);
        }
        if (_4_parts.length < 2) {
            throw new ClientException("字段名称格式错误：需要包含下划线，name=" + name);
        }

        String _A = _4_parts[0].trim();//名称中的A部分--类型
        //2. 校验名称开头是否属于4种类型之一：part,image,text,audio
        if (!_A.equals(Constants.Weike.CtxType.IMAGE) && !_A.equals(Constants.Weike.CtxType.PART) && !_A.equals(Constants.Weike.CtxType.TEXT) && !_A.equals(Constants.Weike.CtxType.AUDIO)) {
            throw new ClientException("字段名称格式错误：A段只能是以下4种：part、image、text、audio，name=" + name);
        }

        if (Constants.Weike.CtxType.PART.equals(_A) && _4_parts.length != 2) {
            throw new ClientException("字段名称格式错误：part类型的名称格式为‘A_C’，name=" + name);
        }
        if (Constants.Weike.CtxType.IMAGE.equals(_A) && _4_parts.length != 3) {
            throw new ClientException("字段名称格式错误：image类型的名称格式为‘A_B_C’，name=" + name);
        }
        if (Constants.Weike.CtxType.TEXT.equals(_A) && _4_parts.length != 3) {
            throw new ClientException("字段名称格式错误：text类型的名称格式为‘A_B_C’，name=" + name);
        }
        if (Constants.Weike.CtxType.AUDIO.equals(_A) && _4_parts.length != 4) {
            throw new ClientException("字段名称格式错误：audio类型的名称格式为‘A_B_C_D’，name=" + name);
        }

        //3. 校验B段是否是数字（都会有第2段）
        String _B = _4_parts[1].trim();     //名称中的B部分
        if (!StringUtils.isNumeric(_B)) {
            throw new ClientException("字段名称格式错误：B段必须是数字，name=" + name);
        }
        int _B_int = Integer.parseInt(_B);

        //4. 校验C段是否是数字(除了part都会有A_B_C 3段)
        int _C_int = 0;
        if (!Constants.Weike.CtxType.PART.equals(_A)) {
            String _C = _4_parts[2].trim(); //名称中的C部分
            if (!StringUtils.isNumeric(_C)) {
                throw new ClientException("字段名称格式错误：C段必须是数字，name=" + name);
            }
            _C_int = Integer.parseInt(_C);
        }

        //5. 校验D段是否是数字（audio有D段，并且是int类型，表示持续时长）
        long _D_long = 0;
        if (_A.equals(Constants.Weike.CtxType.AUDIO)) {
            String _D = _4_parts[3].trim(); //名称中的C部分
            if (!StringUtils.isNumeric(_D)) {
                throw new ClientException("字段名称格式错误：D段必须是数字，name=" + name);
            }
            _D_long = Long.parseLong(_D);

            //6. D段表示语音的时长，不能为0
            if (_D_long == 0) {
                throw new ClientException("语音时长不能为0，name=" + name);
            }
        }


        //封装数据到part、image、audio、text四种对象中
        Date now = new Date();
        //part格式为A_C
        if (_A.equals(Constants.Weike.CtxType.PART)) {
            String title = request.getParameter(name);
            if (StringUtils.isBlank(title)) {
                throw new ClientException("Parameter 'title' of part is required");
            }
            WeikePart part = new WeikePart();
            part.setWeike_id(weikeId);
            part.setTitle(title);
            part.setSort(_B_int);
            part.setCreate_user(tea_id);
            part.setCreate_date(now);
            part.setUpdate_user(tea_id);
            part.setUpdate_date(now);
            part.setDeleted(Constants.Deleted.NO);
            parts.add(part);
        }
        //image格式为A_B_C
        else if (_A.equals(Constants.Weike.CtxType.IMAGE)) {
            WeikeImage image = new WeikeImage();
            image.setWeike_id(weikeId);
            image.setWeike_part_sort(_B_int);

            MultipartFile imageFile = request.getFile(name);
            if (imageFile == null || imageFile.getSize() == 0) {
                throw new ClientException("image can not be null or zero size");
            }
            image.setFile(imageFile);
            image.setSort(_C_int);
            image.setCreate_user(tea_id);
            image.setCreate_date(now);
            image.setUpdate_user(tea_id);
            image.setUpdate_date(now);
            image.setDeleted(Constants.Deleted.NO);
            image.setClientFieldName(name);
            images.add(image);

        }
        //text or audio
        else {
            WeikeContent content = new WeikeContent();
            content.setWeike_id(weikeId);
            content.setWeike_part_sort(_B_int);
            content.setSort(_C_int);
            content.setCreate_user(tea_id);
            content.setCreate_date(now);
            content.setUpdate_user(tea_id);
            content.setUpdate_date(now);
            content.setDeleted(Constants.Deleted.NO);
            content.setDuration(-1L);
            content.setClientFieldName(name);
            //text格式为A_B_C
            if (_A.equals(Constants.Weike.CtxType.TEXT)) {
                String str_ctx = request.getParameter(name);//文本内容
                if (StringUtils.isBlank(str_ctx)) {
                    throw new ClientException("Text content is required");
                }
                content.setType(Constants.Weike.CtxType.TEXT);
                content.setContent(str_ctx);

            }
            //audio格式为A_B_C_D
            else if (_A.equals(Constants.Weike.CtxType.AUDIO)) {
                MultipartFile audio = request.getFile(name);//语音内容
                if (audio == null || audio.getSize() == 0) {
                    throw new ClientException("Audio content is required");
                }
                content.setType(Constants.Weike.CtxType.AUDIO);
                content.setFile(audio);
                content.setDuration(_D_long);
            } else {
                throw new ClientException("Unknown feild name [" + name + "]");
            }
            contents.add(content);
        }

        return _4_parts[0];
    }


    /**
     * 查询微课内容
     *
     * @param req
     * @return
     */
    @RequestMapping(value = "/teatoken/find/content", method = {RequestMethod.POST})
    @ResponseBody
    public RetInfo teacherFindWeikeContent(@RequestBody WeikeFindContentRequest req, @RequestHeader(value = "token") String token) {
        try {
            logger.info(prefix_in + JsonUtil.toJson(req));

            //校验token，如果校验通过就从缓存中读取Teacher
            if (StringUtils.isBlank(token)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter token  is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            Object o = memcachedService.get(token);
            if (o == null) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                String msg = "缓存中没有teatoken:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo);
                logger.info(JsonUtil.toJson(msg));
                return retInfo;
            }
            if (!TdNtTeacherResult.class.isInstance(o)) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                String msg = "缓存的token不是TdNtTeacherResult类型:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo);
                logger.info(msg);
                return retInfo;
            }
            TdNtTeacherResult tdNtTeacherResult = (TdNtTeacherResult) o;

            //校验 业务参数
            String weike_id_str = req.getWeike_id();
            if (StringUtils.isBlank(weike_id_str)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'weike_id' is required");
                return retInfo;
            }
            if (!StringUtils.isNumeric(weike_id_str)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'weike_id' must be numeric");
                return retInfo;
            }
            Long weike_id = Long.parseLong(weike_id_str);
            WeikeContentResponse response = weikeService.getWeikeContent(weike_id);
            response.setTeacher_avatar_url(tdNtTeacherResult.getTea_head_portrait());

            RetInfo retInfo = RetInfo.getSuccessInfo(response);
            logger.info(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        } catch (Exception e) {
            RetInfo retInfo = RetInfo.getServerErrorInfo(Lang.EN);
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }

    }

    /**
     * 6 查询微课评论
     *
     * @param req
     * @return
     */
    @RequestMapping(value = "/teatoken/find/comments", method = {RequestMethod.POST})
    @ResponseBody
    public RetInfo teacherFindComments(@RequestBody PageRequest req, @RequestHeader(value = "token") String token) {
        try {
            logger.info(prefix_in + JsonUtil.toJson(req));

            //校验token，如果校验通过就从缓存中读取Teacher
            if (StringUtils.isBlank(token)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter token  is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            Object o = memcachedService.get(token);
            if (o == null) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                String msg = "缓存中没有teatoken:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo);
                logger.info(JsonUtil.toJson(msg));
                return retInfo;
            }
            if (!TdNtTeacherResult.class.isInstance(o)) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                String msg = "缓存的token不是TdNtTeacherResult类型:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo);
                logger.info(msg);
                return retInfo;
            }
            TdNtTeacherResult tdNtTeacherResult = (TdNtTeacherResult) o;


            PageResponse response = weikeService.teacherFindComments(req, tdNtTeacherResult.getTea_id());

            RetInfo retInfo = RetInfo.getSuccessInfo(response);
            logger.info(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        } catch (Exception e) {
            RetInfo retInfo = RetInfo.getServerErrorInfo(Lang.EN);
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }
    }

    /**
     * 7 回复评论
     *
     * @param req
     * @return
     */
    @RequestMapping(value = "/teatoken/reply/comments", method = {RequestMethod.POST})
    @ResponseBody
    public RetInfo teacherReplyComments(@RequestBody TeacherReplyCommentRequest req, @RequestHeader(value = "token") String token) {
        try {
            logger.info(prefix_in + JsonUtil.toJson(req));

            //校验token，如果校验通过就从缓存中读取Teacher
            if (StringUtils.isBlank(token)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter token is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            Object o = memcachedService.get(token);
            if (o == null) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                String msg = "缓存中没有teatoken:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo);
                logger.info(JsonUtil.toJson(msg));
                return retInfo;
            }
            if (!TdNtTeacherResult.class.isInstance(o)) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                String msg = "缓存的token不是TdNtTeacherResult类型:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo);
                logger.info(msg);
                return retInfo;
            }
            TdNtTeacherResult tdNtTeacherResult = (TdNtTeacherResult) o;

            if(req.getCommentsId() == null){
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'commentsId' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if(req.getReplyContent() == null){
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'replyContent' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if(StringUtils.isBlank(req.getReplyContent().trim())){
                RetInfo retInfo = RetInfo.getClientErrorInfo("参数 'replyContent' 不能为空字符");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }

            weikeService.teacherReplyComments(req, tdNtTeacherResult.getTea_id());

            RetInfo retInfo = RetInfo.getSuccessInfo();
            logger.info(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        } catch (Exception e) {
            RetInfo retInfo = RetInfo.getServerErrorInfo(Lang.EN);
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }
    }

    @RequestMapping(value="/teaintro/upload/video",method = {RequestMethod.POST})
    @ResponseBody
    public RetInfo teacherUploadVideo(MultipartHttpServletRequest request,@RequestHeader(value = "token") String token){
        try {
            logger.info(prefix_in);
            //校验token，如果校验通过就从缓存中读取Teacher
            if (StringUtils.isBlank(token)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("token  is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            Object o = memcachedService.get(token);
            if (o == null) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存中没有teatoken:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!TdNtTeacherResult.class.isInstance(o)) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存的token不是TdNtTeacherResult类型:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            TdNtTeacherResult tdNtTeacherResult = (TdNtTeacherResult) o;

            MultipartFile tea_video_intro = request.getFile("tea_video_intro");
            if (tea_video_intro == null || tea_video_intro.getSize() == 0) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'audio' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }

            String obj=weikeService.uploadTeaIntro(tea_video_intro,tdNtTeacherResult.getTea_id());
            RetInfo retInfo = RetInfo.getSuccessInfo();
            retInfo.setObj(obj);
            logger.info(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }catch (Exception e){
            RetInfo retInfo = RetInfo.getServerErrorInfo(Lang.EN);
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }
    }

    @RequestMapping(value="/teaintro/upload/videoCover",method = {RequestMethod.POST})
    @ResponseBody
    public RetInfo teacherUploadVideoCover(MultipartHttpServletRequest request,@RequestHeader(value = "token") String token){
        try {
            logger.info(prefix_in);
            //校验token，如果校验通过就从缓存中读取Teacher
            if (StringUtils.isBlank(token)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("token  is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            Object o = memcachedService.get(token);
            if (o == null) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存中没有teatoken:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            if (!TdNtTeacherResult.class.isInstance(o)) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存的token不是TdNtTeacherResult类型:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            TdNtTeacherResult tdNtTeacherResult = (TdNtTeacherResult) o;

            MultipartFile tea_video_intro_cover = request.getFile("tea_intro_video_cover");
            if (tea_video_intro_cover == null || tea_video_intro_cover.getSize() == 0) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter 'tea_intro_video_cover' is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }

            String obj=weikeService.uploadTeaIntroCover(tea_video_intro_cover,tdNtTeacherResult.getTea_id());
            RetInfo retInfo = RetInfo.getSuccessInfo();
            retInfo.setObj(obj);
            logger.info(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }catch (Exception e){
            RetInfo retInfo = RetInfo.getServerErrorInfo(Lang.EN);
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }
    }

}
