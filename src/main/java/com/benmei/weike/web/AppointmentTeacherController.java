package com.benmei.weike.web;

import com.benmei.weike.common.Constants;
import com.benmei.weike.common.Lang;
import com.benmei.weike.dto.IntoClassRoom.IntoClassRoomRequest;
import com.benmei.weike.dto.IntoClassRoom.IntoClassRoomResponse;
import com.benmei.weike.dto.LeaveClassRoom.LeaveClassRoomRequest;
import com.benmei.weike.entity.Appointment;
import com.benmei.weike.exception.ClientException;
import com.benmei.weike.service.AppointmentService;
import com.benmei.weike.service.common.MemcachedService;
import com.benmei.weike.util.JsonUtil;
import com.nativetalk.base.RetInfo;
import com.nativetalk.bean.member.TdNtMember;
import com.nativetalk.bean.teacher.TdNtTeacherResult;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Peter on 2017/9/6.
 */
@RestController
@RequestMapping(value = "/v4.3.1/teacher/appointment")
public class AppointmentTeacherController {
    public static final Logger logger = LoggerFactory.getLogger(AppointmentTeacherController.class);

    @Autowired
    private MemcachedService memcachedService;

    @Autowired
    private AppointmentService appointmentService;


    @RequestMapping(value = "/teatoken/intoClassRoom", method = {RequestMethod.POST}, consumes = "application/json")
    public RetInfo teacherIntoClassRoomFor(@RequestBody IntoClassRoomRequest request,
                                           @RequestHeader(value = "token", required = false) String token) {
        try {
            //打印入参
            logger.info(Constants.prefix_in + JsonUtil.toJson(request));

            //校验token，如果校验通过就从缓存中读取Teacher
            if (StringUtils.isBlank(token)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter token  is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            Object o = memcachedService.get(token);
            if (o == null) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存中没有teatoken:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }

            if (!TdNtTeacherResult.class.isInstance(o)) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存的token不是TdNtTeacherResult类型:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            TdNtTeacherResult teacher = (TdNtTeacherResult) o;

            IntoClassRoomResponse response = appointmentService.intoClassRoom(request, Constants.UserRole.teacher, teacher.getTea_id());
            RetInfo retInfo = RetInfo.getSuccessInfo(response);
            //打印出参
            logger.info(Constants.prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        } catch (ClientException e) {
            //记录错误日志
            RetInfo retInfo = RetInfo.getServerErrorInfo(e.getMessage());
            logger.error(e.getMessage(), e);
            //打印出参
            logger.error(Constants.prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        } catch (Exception e) {
            //记录错误日志
            RetInfo retInfo = RetInfo.getServerErrorInfo();
            logger.error(e.getMessage(), e);
            //打印出参
            logger.error(Constants.prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }
    }

    @RequestMapping(value = "/teatoken/leaveClassRoom", method = {RequestMethod.POST}, consumes = "application/json")
    public RetInfo teacherLeaveClassRoom(@RequestBody LeaveClassRoomRequest request,
                                         @RequestHeader(value = "token", required = false) String token) {
        try {
            // 打印入参
            logger.info(Constants.prefix_in + JsonUtil.toJson(request));

            // 校验token，如果校验通过就从缓存中读取Teacher
            if (StringUtils.isBlank(token)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter token  is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            Object o = memcachedService.get(token);
            if (o == null) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存中没有teatoken:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }

            if (!TdNtTeacherResult.class.isInstance(o)) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.EN);
                logger.info("缓存的token不是TdNtTeacherResult类型:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            TdNtTeacherResult tdNtTeacherResult = (TdNtTeacherResult) o;

            appointmentService.leaveClassRoom(request, Constants.UserRole.teacher,tdNtTeacherResult.getTea_id());
            RetInfo retInfo = RetInfo.getSuccessInfo();
            //打印出参
            logger.info(Constants.prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        } catch (ClientException e) {
            //记录错误日志
            RetInfo retInfo = RetInfo.getServerErrorInfo(e.getMessage());
            logger.error(e.getMessage(), e);
            //打印出参
            logger.error(Constants.prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        } catch (Exception e) {
            //记录错误日志
            RetInfo retInfo = RetInfo.getServerErrorInfo();
            logger.error(e.getMessage(), e);
            //打印出参
            logger.error(Constants.prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }
    }

}
