package com.benmei.weike.web;

import com.benmei.weike.common.Lang;
import com.benmei.weike.dto.AudioPlayStatusReceiveDto;
import com.benmei.weike.dto.MemberContentRecommendClickDto;
import com.benmei.weike.dto.MemberContentRecommendDto;
import com.benmei.weike.service.MemberContentRecommendService;
import com.benmei.weike.service.WeikeService;
import com.benmei.weike.service.common.MemcachedService;
import com.benmei.weike.util.JsonUtil;
import com.nativetalk.base.RetInfo;
import com.nativetalk.bean.member.TdNtMember;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 学生端 微课接口
 */
@RestController
@RequestMapping(value = "/v4.3/student")
public class StudentControllerV4_3 {

    public static final Logger logger = LoggerFactory.getLogger(StudentControllerV4_3.class);

    @Autowired
    private WeikeService weikeService;

    @Autowired
    private MemcachedService memcachedService;

    @Autowired
    private MemberContentRecommendService memberContentRecommendService;

    //log 记录方法入惨的前缀
    private static final String prefix_in = "\nparameters in -->> ";
    //log 记录方法出惨的前缀
    private static final String prefix_out = "\nparameters out -->>";

    /**
     * 接口编号：v4.3-1 首页 -> 推荐
     *
     * @return
     */
    @RequestMapping(value = "/index/recomend", method = {RequestMethod.POST})
    @ResponseBody
    public RetInfo studentIndexRecomend(@RequestHeader(value = "token") String token) {
        try {
            logger.info(prefix_in);

            Integer memb_id = null;
            //校验token，如果校验通过就从缓存中读取Member
            if (StringUtils.isNotBlank(token)) {
                Object o = memcachedService.get(token);
                if (o != null && TdNtMember.class.isInstance(o)) {
                    TdNtMember tdNtMember = (TdNtMember) o;
                    memb_id = tdNtMember.getMemb_id();
                }
            }
            MemberContentRecommendDto recommendDto = memberContentRecommendService.findRecommendContent(memb_id);

            RetInfo retInfo = RetInfo.getSuccessInfo();
            retInfo.setObj(recommendDto);
            logger.info(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        } catch (Exception e) {
            RetInfo retInfo = RetInfo.getServerErrorInfo();
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }
    }

    /**
     * 接口编号：v4.3-2 首页 -> 推荐
     *
     * @return
     */
    @RequestMapping(value = "/index/recomend/click", method = {RequestMethod.POST})
    @ResponseBody
    public RetInfo studentIndexRecomendClick(@RequestBody MemberContentRecommendClickDto req) {
        try {
            logger.info(prefix_in + JsonUtil.toJson(req));

            if(req.getRecommend_id() == null){
                return RetInfo.getClientErrorInfo("The parameter 'recommend_id' is required.");
            }

            memberContentRecommendService.clickRecommendContent(req.getRecommend_id());

            RetInfo retInfo = RetInfo.getSuccessInfo();
            logger.info(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        } catch (Exception e) {
            RetInfo retInfo = RetInfo.getServerErrorInfo();
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }
    }

    /**
     * 接口编号：v4.3-8 记录语音播放次数
     *
     * @return
     */
    @RequestMapping(value = "/token/content/audio/play/status/receive", method = {RequestMethod.POST})
    @ResponseBody
    public RetInfo audioPlayStatusReceive(@RequestBody AudioPlayStatusReceiveDto req, @RequestHeader(value = "token") String token) {
        try {
            logger.info(prefix_in+ JsonUtil.toJson(req));

            //校验token，如果校验通过就从缓存中读取Member
            if (StringUtils.isBlank(token)) {
                RetInfo retInfo = RetInfo.getClientErrorInfo("Parameter token  is required");
                logger.info(JsonUtil.toJson(retInfo));
                return retInfo;
            }
            Object o = memcachedService.get(token);
            if (o == null) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.ZH);
                logger.info("缓存中没有MemberToken:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }

            if (!TdNtMember.class.isInstance(o)) {
                RetInfo retInfo = RetInfo.getReloginInfo(Lang.ZH);
                logger.info("缓存的token不是TdNtMember类型:[" + token + "]，用户需要重新登录系统。 retInfo-->" + JsonUtil.toJson(retInfo));
                return retInfo;
            }
            TdNtMember tdNtMember = (TdNtMember) o;

            logger.info("memb_id:"+tdNtMember.getMemb_id());
            weikeService.audioPlayStatusReceive(req, tdNtMember.getMemb_id());

            RetInfo retInfo = RetInfo.getSuccessInfo();
            logger.info(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        } catch (Exception e) {
            RetInfo retInfo = RetInfo.getServerErrorInfo();
            logger.error(e.getMessage(), e);
            logger.error(prefix_out + JsonUtil.toJson(retInfo));
            return retInfo;
        }
    }
}
