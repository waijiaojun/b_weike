package com.benmei.weike.web;

import com.benmei.weike.dao.WebsiteConfigDao;
import com.benmei.weike.dto.FindCommentsPageRequest;
import com.benmei.weike.dto.PageResponse;
import com.benmei.weike.dto.weikeContentResponse.StudentWeikeContentRespons;
import com.benmei.weike.dto.weikeDetailResponse.WeikeDetailResponse;
import com.benmei.weike.entity.WeikeContent;
import com.benmei.weike.service.WeikeCommentsService;
import com.benmei.weike.service.WeikeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Peter on 2017/5/25.
 */
@Controller
@RequestMapping(value = "/html/share")
public class WeikeShareController {
    public static final Logger logger = LoggerFactory.getLogger(WeikeShareController.class);

    //log 记录方法入惨的前缀
    private static final String prefix_in = "\nparameters in -->> ";
    //log 记录方法出惨的前缀
    private static final String prefix_out = "\nparameters out -->>";

    @Autowired
    private WeikeService weikeService;

    @Autowired
    private WeikeCommentsService weikeCommentsService;

    @Resource
    private WebsiteConfigDao websiteConfigDao;

    /**
     * 微课详情
     * 页面与app页面的区别：
     * 页面无法获取用户信息
     * 1.学生个人评分无法获取
     * 2.购买微课无法实现
     * 3.
     *
     * @param weikeId
     * @param model
     * @return
     */
    @RequestMapping(value = "/detail/{weikeId}")
    public String detail(@PathVariable("weikeId") Long weikeId, Model model) {
        logger.info(prefix_in + "weikeId:" + weikeId);
        try {
            //查询微课详情
            WeikeDetailResponse weikeDetail = weikeService.getWeikeDetailForSharePage(weikeId);

            //查询微课评论
            FindCommentsPageRequest req = new FindCommentsPageRequest();
            req.setWeike_id(weikeId);
            PageResponse comments = weikeCommentsService.studentFindComments(req);
            model.addAttribute("computer_download_url",websiteConfigDao.getData(31));
            model.addAttribute("iOS_download_url",websiteConfigDao.getData(32));
            model.addAttribute("Android_download_url",websiteConfigDao.getData(33));
            model.addAttribute("weikeDetail", weikeDetail);
            model.addAttribute("comments", comments);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return "weike-detail";
    }

    /**
     * 微课详情
     * 页面与app页面的区别：
     * 页面无法获取用户信息
     * 1.学生个人评分无法获取
     * 2.购买微课无法实现
     * 3.
     *
     * @param weikeId
     * @param model
     * @return
     */
    @RequestMapping(value = "/content/{weikeId}")
    public String content(@PathVariable("weikeId") Long weikeId, Model model) {
        logger.info(prefix_in + "weikeId:" + weikeId);
        try {
            StudentWeikeContentRespons weikeContent = weikeService.getWeikeContent(null,weikeId);
            model.addAttribute("computer_download_url",websiteConfigDao.getData(31));
            model.addAttribute("iOS_download_url",websiteConfigDao.getData(32));
            model.addAttribute("Android_download_url",websiteConfigDao.getData(33));
            model.addAttribute("weikeContent",weikeContent);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return "weike-content";
    }


    @RequestMapping(value = "/createWeikeHelp")
    public String createWeikeHelp( Model model) {
        return "createWeikeHelp";
    }

    @RequestMapping(value = "/toUpdateOssFileNamePage/{weikeId}")
    public String toUpdateOssFileNamePage(@PathVariable("weikeId") Long weikeId, Model model) {
        logger.info(prefix_in + "weikeId:" + weikeId);
        try {
            //查询微课详情
            List<WeikeContent> list =  weikeService.getWeikeAudio(weikeId);

            model.addAttribute("list", list);
            model.addAttribute("count", list.size());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return "weike-update-ossFileName";
    }
}
