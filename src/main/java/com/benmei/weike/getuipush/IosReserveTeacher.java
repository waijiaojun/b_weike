package com.benmei.weike.getuipush;

import com.benmei.weike.common.Constants;
import com.benmei.weike.util.JsonUtil;
import com.gexin.rp.sdk.base.IPushResult;
import com.gexin.rp.sdk.base.impl.SingleMessage;
import com.gexin.rp.sdk.base.payload.APNPayload;
import com.gexin.rp.sdk.http.IGtPush;
import com.gexin.rp.sdk.template.TransmissionTemplate;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by lizhun on 16/1/12.
 */
public class IosReserveTeacher {
    public static final Logger logger = LoggerFactory.getLogger(IosReserveTeacher.class);
    static String appId = Constants.teacher_appId;
    static String appKey = Constants.teacher_appKey;
    static String masterSecret = Constants.teacher_masterSecret;
    static String url ="http://sdk.open.api.igexin.com/serviceex";
    public static void apnpush(String title,String body,String devicetoken,int reserve_id)  {
        logger.info("apnpush : title=" + title + ",body=" + body + ",devicetoken=" + devicetoken + ",reserve_id=" + reserve_id);
        TransmissionTemplate template = new TransmissionTemplate();
        template.setAppId(appId);
        template.setAppkey(appKey);
        template.setTransmissionType(2);//应用启动类型，1：强制应用启动 2：等待应用启动

        //APN高级推送
        APNPayload apnpayload = new APNPayload();
        APNPayload.DictionaryAlertMsg alertMsg = new APNPayload.DictionaryAlertMsg();
        alertMsg.setBody(body);
        alertMsg.setTitle(title);
        apnpayload.setAlertMsg(alertMsg);

        apnpayload.addCustomMsg("body",body);
        apnpayload.addCustomMsg("title",title);
        apnpayload.setContentAvailable(20);//透传数据
        apnpayload.addCustomMsg("reserve_id",reserve_id);
        template.setAPNInfo(apnpayload);

        IGtPush push = new IGtPush(url, appKey, masterSecret);


        SingleMessage message = new SingleMessage();
        message.setData(template);

        IPushResult pushResult = push.pushAPNMessageToSingle(appId,devicetoken,message);
       // JSONObject msgJsonObj = JSONObject.fromObject(message);
        if (pushResult != null) {
            logger.info("IOS消息推送成功");
            logger.info("发送消息体==>>" + message.toString());
            logger.info("个推返回消息==>>" + JsonUtil.toJson(pushResult.getResponse()));
        } else {
            logger.error("IOS消息推送成功消息推送失败");
            logger.error("发送消息体==>>" + message.toString());
            logger.error("个推服务器响应异常,返回消息==>>null");
        }

    }

    public static void main(String[] args) {
        IosReserveTeacher.apnpush("Nativetalk", "测试 message push", "be38c31008a6358c128267bdfb0cc0cefa4b48c8d399d90c62ccb7907cd137e2", 1204);
    }

}
