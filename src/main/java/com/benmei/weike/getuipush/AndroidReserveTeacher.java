package com.benmei.weike.getuipush;

import com.benmei.weike.common.Constants;
import com.benmei.weike.util.JsonUtil;
import com.gexin.rp.sdk.base.IPushResult;
import com.gexin.rp.sdk.base.impl.SingleMessage;
import com.gexin.rp.sdk.base.impl.Target;
import com.gexin.rp.sdk.exceptions.RequestException;
import com.gexin.rp.sdk.http.IGtPush;
import com.gexin.rp.sdk.template.TransmissionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lizhun on 16/1/12.
 */
public class AndroidReserveTeacher {
    public static final Logger logger = LoggerFactory.getLogger(AndroidReserveTeacher.class);
    static String appId = Constants.teacher_appId;
    static String appKey = Constants.teacher_appKey;
    static String masterSecret = Constants.teacher_masterSecret;
    static String url ="http://sdk.open.api.igexin.com/serviceex";

    public static void apnpush(String title, String body, String CID, int reserve_id) {
        logger.info("apnpush title="+title+",body"+body+",CID="+CID+",reserve_id="+reserve_id);
        String host = "http://sdk.open.api.igexin.com/apiex.htm";
        IGtPush push = new IGtPush(host, appKey, masterSecret);
        TransmissionTemplate template = new TransmissionTemplate();
        template.setAppId(appId);
        template.setAppkey(appKey);
        // 透传消息设置，1为强制启动应用，客户端接收到消息后就会立即启动应用；2为等待应用启动
        template.setTransmissionType(2);
        Map<String,Object> map = new HashMap<>();
        map.put("reserve_id",reserve_id);

        map.put("title",title);
        map.put("body",body);
        map.put("number",20);
        String contentJson = JsonUtil.toJson(map);
        template.setTransmissionContent(contentJson);
        // 设置定时展示时间
        // template.setDuration("2015-01-16 11:40:00", "2015-01-16 12:24:00");
        SingleMessage message = new SingleMessage();
        message.setOffline(true);
        //离线有效时间，单位为毫秒，可选
        message.setOfflineExpireTime(24 * 3600 * 1000);
        message.setData(template);
        message.setPushNetWorkType(0); //可选。判断是否客户端是否wifi环境下推送，1为在WIFI环境下，0为不限制网络环境。

        Target target = new Target();
        target.setAppId(appId);
        target.setClientId(CID);
        //用户别名推送，cid和用户别名只能2者选其一
        //String alias = "个";
        //target.setAlias(alias);
        IPushResult ret = null;
        try {
            ret = push.pushMessageToSingle(message, target);
        } catch (RequestException e) {
            e.printStackTrace();
            ret = push.pushMessageToSingle(message, target, e.getRequestId());
        }
        if (ret != null) {
            logger.info("Android消息推送成功");
            logger.info("发送消息体==>>" + contentJson);
            String response=JsonUtil.toJson(ret.getResponse());
            logger.info("个推返回消息==>>" + response);
        } else {
            logger.error("Android消息推送成功消息推送失败");
            logger.error("发送消息体==>>" + contentJson);
            logger.error("个推服务器响应异常,返回消息==>>null");
        }

    }
}
