package com.benmei.weike.exception;

import com.nativetalk.base.RetInfo;

/**
 * Created by Micoo on 2017/4/19.
 */
public class ClientException extends RuntimeException {

    private String code;
    private String message;

    public ClientException(String message, Throwable throwable) {
        super(message,throwable);
        this.code = RetInfo.ReturnCode.CLIENT_ERROR;
        this.message = message;
    }

    public ClientException(String message) {
        super(message);
        this.code = RetInfo.ReturnCode.CLIENT_ERROR;
        this.message = message;
    }

    public ClientException(String code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
