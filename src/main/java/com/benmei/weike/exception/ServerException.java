package com.benmei.weike.exception;

import com.benmei.weike.common.Lang;
import com.nativetalk.base.RetInfo;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by Micoo on 2017/4/19.
 */
public class ServerException extends RuntimeException {
    private String code;
    private String message;

    public ServerException(String code, String message) {
        if (StringUtils.isBlank(code)) {
            this.code = RetInfo.ReturnCode.SERVER_ERROR;
        } else {
            this.code = code;
        }
        this.message = message;
    }

    public ServerException(String message) {
        this.code = RetInfo.ReturnCode.SERVER_ERROR;
        this.message = message;
    }

    public ServerException() {
        this.code = RetInfo.ReturnCode.SERVER_ERROR;
        this.message = RetInfo.getServerErrorInfo(Lang.ZH).getTip();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
