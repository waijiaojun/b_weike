package com.benmei.weike.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Micoo on 2017/4/18.
 */
public class MD5Util {
    /**
     * 将给定的字符串进行MD5加密
     * @param str
     * @return
     */
    public static String code(String str) {
        try {
            MessageDigest digest = MessageDigest.getInstance("md5");
            byte[] data = digest.digest(str.getBytes());
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < data.length; i++) {
                String result = Integer.toHexString(data[i] & 0xff);
                if (result.length() == 1) {
                    result = "0" + result;
                }
                sb.append(result);
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
        }
        return null;
    }
}
