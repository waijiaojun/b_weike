package com.benmei.weike.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Peter on 2017/4/13.
 * 该类是工具类和常量类，用于方便调用和查找缓存中的key.
 */
public class MemcacheKeys {
    private static final Logger logger = LoggerFactory.getLogger("memcached");

    /**
     * 启动页
     */
    public static String tdNtStartPageIndex = "tdNtStartPageIndex";
    /**
     * 礼物 点亮
     */
    public static String tsNtLiveGivingLists = "tsNtLiveGivingLists";
    public static String tsNtLiveGivingListsMap = "tsNtLiveGivingListsMap";
    public static String lighticon = "lighticon";
    /**
     * 首页banner图片
     */
    public static String bannerInfoAPPIndexs = "bannerInfoAPPIndexs";

    /**
     * 首页教学父菜单
     */
    public static String tsNtCategoryIndexParent = "tsNtCategoryIndexParent";
    /**
     * 首页教学子菜单 map
     */
    public static String tsNtCategoryIndexsMap = "tsNtCategoryIndexsMap";
    /**
     * 教师端
     */
    public static String tsNtCategoryTeachers = "tsNtCategoryTeachers";
    /**
     * 课程信息
     */
    public static String tsNtCourseInfoIndexMap = "tsNtCourseInfoIndexMap";
    public static String tsNtCourseInfoIndicesTwoMap = "tsNtCourseInfoIndicesTwoMap";
    public static String courseMapIdTwo = "courseMapIdTwo";
    public static String tsNtCategoryIndices = "tsNtCategoryIndices";
    public static String tsNtCategoryParentsInfo = "tsNtCategoryParentsInfo";
    /**
     * 课程信息2.1
     */
    public static String tsNtCourseInfoIndexMapTwo = "tsNtCourseInfoIndexMapTwo";
    /**
     * 课程信息 教师端
     */
    public static String tsNtCourseInfoTeachersMap = "tsNtCourseInfoTeachersMap";
    /**
     * 课程信息 教师端(map)
     */
    public static String tsNtCategoryTeacherMap = "tsNtCategoryTeacherMap";
    /**
     * 课程信息 详情
     */
    public static String tsNtCourseInfoDetailMap = "tsNtCourseInfoDetailMap";
    public static String tsNtCourseInfoDetailMapTwo = "tsNtCourseInfoDetailMapTwo";
    public static String tdNtCourseCatalogLevelMap = "tdNtCourseCatalogLevelMap";
    /**
     * 课程信息banner
     */
    public static String tdNtCourseBannerListMap = "tdNtCourseBannerListMap";
    /**
     * 课程信息 教材图片
     */
    public static String tdNtCoursePictureListMap = "tdNtCoursePictureListMap";
    public static String tdNtCourseLevelPageListMap = "tdNtCourseLevelPageListMap";
    /**
     * 课程信息 评论
     */
    public static String tdNtCommentCouseMap = "tdNtCommentCouseMap";
    /**
     * 课程信息 目录
     */
    public static String tdNtCourseLevelDetailsMap = "tdNtCourseLevelDetailsMap";
    public static String tdNtCourseCatalogMap = "tdNtCourseCatalogMap";
    public static String tdNtCourseCatalogEnglishMap = "tdNtCourseCatalogEnglishMap";
    //自由话题
    public static String freeCourse = "freeCourse";
    public static String freeCoursePicture = "freeCoursePicture";


    /**
     * 帮助中心
     */
    public static String helpCenter = "helpCenter";
    /**
     * 帮助中心(外教版)
     */
    public static String whelpCenter = "whelpCenter";
    /**
     * 所有文章
     */
    public static String tsNtArticleInfoIndexMap = "tsNtArticleInfoIndexMap";
    /**
     * 静态信息
     */
    public static String tsNtWebSiteConfIndexMap = "tsNtWebSiteConfIndexMap";

    /**
     * 套餐信息
     */
    public static String tsNtSetmealIndex = "tsNtSetmealIndex";
    public static String tsNtSetmealIndexMap = "tsNtSetmealIndexMap";
    public static String tsNtSetmealAllIndexMap = "tsNtSetmealAllIndexMap";
    /**
     * 套餐简介
     */
    public static String tsNtSetmealSynopisMap = "tsNtSetmealSynopisMap";
    /**
     * 静态字典
     */
    public static  String tsNtDictTeas = "tsNtDictTeas";
    public static  String tsNtDictIndicesMap = "tsNtDictIndicesMap";
    public static  String tsNtDictDeatilsMap = "tsNtDictDeatilsMap";
    public static  String tsNtDictIndices = "tsNtDictIndices";
    public static  String demand = "demand";

    /**
     * List<TsNtDictTea> country
     */
    public static  String country = "country";
    public static  String livetype = "livetype";
    public static  String systemLabel = "systemLabel";
    /**
     * 满减信息
     */
    public static String TdNtPayActivityIndex = "TdNtPayActivityIndex";
}
