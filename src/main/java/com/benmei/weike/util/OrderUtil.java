package com.benmei.weike.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Micoo on 2017/5/9.
 */
public class OrderUtil {
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    public static String getOrderNumber() {
        String orderNub = sdf.format(new Date()).substring(2, 12);

        String code = "";
        while (code.length() < 5) {
            code += (int) (Math.random() * 10);
        }
        orderNub = orderNub + code;
        return orderNub;
    }
}
