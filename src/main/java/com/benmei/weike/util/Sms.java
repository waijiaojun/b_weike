package com.benmei.weike.util;

/**
 * 发送短信验证
 *
 * @author lizhun
 */
public class Sms {
    private String memb_phone;
    private String memb_phone_area;

    public Sms() {

    }

    public Sms(String memb_phone, String memb_phone_area) {
        this.memb_phone = memb_phone;
        this.memb_phone_area = memb_phone_area;
    }

    public String getMemb_phone_area() {
        return memb_phone_area;
    }

    public void setMemb_phone_area(String memb_phone_area) {
        this.memb_phone_area = memb_phone_area;
    }

    public String getMemb_phone() {
        return memb_phone;
    }

    public void setMemb_phone(String memb_phone) {
        this.memb_phone = memb_phone;
    }
}
