package com.benmei.weike.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Micoo on 2017/5/8.
 */
public class DateUtil {
    private static SimpleDateFormat yyyyMMFormat = new SimpleDateFormat("yyyyMM");

    public static String format(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }

    public static String format(Date date, String format, String clientTimeZoneId) {
        if(StringUtils.isNotBlank(clientTimeZoneId)) {
            TimeZone clientTimeZone = TimeZone.getTimeZone(clientTimeZoneId);
            return DateFormatUtils.format(date,format,clientTimeZone);
        }else{
            return format(date,format);
        }

    }

    public static String getMonth(Date date) {
        return yyyyMMFormat.format(date);
    }

    /**
     * 计算开始时间与结束时间相差的天数
     *
     * @param start
     * @param end
     * @return
     */
    public static Integer getDurationDays(Date start, Date end) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(start);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(end);
        int day1 = cal1.get(Calendar.DAY_OF_YEAR);
        int day2 = cal2.get(Calendar.DAY_OF_YEAR);

        int year1 = cal1.get(Calendar.YEAR);
        int year2 = cal2.get(Calendar.YEAR);
        if (year1 != year2)   //同一年
        {
            int timeDistance = 0;
            for (int i = year1; i < year2; i++) {
                if (i % 4 == 0 && i % 100 != 0 || i % 400 == 0)    //闰年
                {
                    timeDistance += 366;
                } else    //不是闰年
                {
                    timeDistance += 365;
                }
            }

            return timeDistance + (day2 - day1);
        } else    //不同年
        {
            return day2 - day1;
        }
    }

    public static int getHour(Date date) {
        int start_time = 0;
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        start_time = c.get(Calendar.HOUR_OF_DAY);

        return start_time;
    }

    /**
     * 对给定的时间添加一个分钟数，如果minute是负数就是减去minute分钟
     *
     * @param start_time
     * @param minute
     * @return
     */
    public static Date addMinute(Date start_time, int minute) {
        Calendar c = Calendar.getInstance();
        c.setTime(start_time);
        c.add(Calendar.MINUTE, minute);
        return c.getTime();
    }

    public static Long getTime(int set_type, int size) {
        int dateSize = 0;
        switch (set_type) {
            case 1:
                dateSize = 1;
                break;
            case 2:
                dateSize = 7;
                break;
            case 3:
                dateSize = 31;
                break;
            case 4:
                dateSize = 365;
                break;

            default:
                break;
        }
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, dateSize * size);
//        Date date = new Date();
//        date.setDate(dateSize*size);
        return c.getTimeInMillis();
    }

    public static Date addYear(Date start_time, int i) {
        Calendar c = Calendar.getInstance();
        c.setTime(start_time);
        c.add(Calendar.YEAR, i);
        return c.getTime();
    }

    public static Date addMonth(Date start_time, int i) {
        Calendar c = Calendar.getInstance();
        c.setTime(start_time);
        c.add(Calendar.MONTH, i);
        return c.getTime();
    }

    public static Date createDate(String date ) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return sdf.parse(date);
    }
}
