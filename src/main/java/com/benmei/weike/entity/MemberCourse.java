package com.benmei.weike.entity;

import java.util.Date;

/**
 * Created by Peter on 2017/9/11.
 */
public class MemberCourse {
   private Integer memb_cou_id;//	int
   private Integer memb_id;//	int
   private Integer cou_id;//	int
   private Integer study_page;//	int
   private Date start_time;//	timestamp
   private Date end_time;//	timestamp
   private String state;//	varchar

    public Integer getMemb_cou_id() {
        return memb_cou_id;
    }

    public void setMemb_cou_id(Integer memb_cou_id) {
        this.memb_cou_id = memb_cou_id;
    }

    public Integer getMemb_id() {
        return memb_id;
    }

    public void setMemb_id(Integer memb_id) {
        this.memb_id = memb_id;
    }

    public Integer getCou_id() {
        return cou_id;
    }

    public void setCou_id(Integer cou_id) {
        this.cou_id = cou_id;
    }

    public Integer getStudy_page() {
        return study_page;
    }

    public void setStudy_page(Integer study_page) {
        this.study_page = study_page;
    }

    public Date getStart_time() {
        return start_time;
    }

    public void setStart_time(Date start_time) {
        this.start_time = start_time;
    }

    public Date getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Date end_time) {
        this.end_time = end_time;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
