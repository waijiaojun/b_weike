package com.benmei.weike.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Peter on 2017/4/13.
 */
public class Weike implements Serializable{

    private Long id;//                   bigint not null auto_increment,
    private String title;//                varchar(128) comment '标题',
    private String intro;//                varchar(255) comment '介绍',
    private String cover_url;//            varchar(512) comment '封面图片',
    private String intro_video;// 介绍视频

    private String title_student;//                varchar(128) comment '标题', 学生端显示用的
    private String intro_student;//                varchar(255) comment '介绍', 学生端显示用的
    private String cover_url_student;//            varchar(512) comment '封面图片', 学生端显示用的

    @JsonIgnore
    private Double teacher_earn;//         float default 0 comment '老师发布微课可以获取的佣金',
    private BigDecimal student_price;//      float default 0 comment '学生购买价格',
    private Integer approved;//int comment '审核状态：-1 未提交审核 0 未审核（已提交审核）；1 审核不通过；2 通过',
    private String approve_desc;//审核说明
    private Integer published;//int comment '是否发布：0 未发布；1 已发布',
    @JsonIgnore
    private Integer publish_user_id;// int comment '微课发布人id，微课发布人有老师或者后台运营人员两种',
    private Integer buy_number;//int comment '购买人数',//界面上和人数相关的都用此字段：学习人数，收听人数，购买人数等
    @JsonIgnore
    private Integer study_number;// int comment '学习人数',//该字段目前不使用(浏览量)
    @JsonIgnore
    private Integer show_on_student;// int comment '是否显示给学生：0 不显示；1 显示。后台设置，老师不可以设置',
    private Double score;//  float comment '综合评分',

    private Integer boutique;//  int default 0 comment '精品：0 不是精品；1 是精品',
    private Integer today_boutique;// int comment '今日精品：0 不是；1 是',
    private Integer hot;//  int comment '最热微课：0 不是；1 是',

    private Integer category_id;//     int comment '分类ID，引用dict表的livetype',

    public String weike_video;  //微课视频
    public String weike_video_img;  //微课视频封面图

    public String getWeike_video_img() {
        return weike_video_img;
    }

    public void setWeike_video_img(String weike_video_img) {
        this.weike_video_img = weike_video_img;
    }

    public String getWeike_video() {
        return weike_video;
    }

    public void setWeike_video(String weike_video) {
        this.weike_video = weike_video;
    }

    @JsonIgnore
    private Integer create_user;//  int not null,
    @JsonIgnore
    private Date create_date;// datetime not null,
    @JsonIgnore
    private Integer update_user;//  int not null,
    @JsonIgnore
    private Date update_date;//  datetime not null,
    @JsonIgnore
    private Integer deleted;//  int not null default 0 comment '数据删除标记：0使用；1删除',

    private Integer is_lock = 1; //0：未上锁   1：已上锁

    public Integer getIs_lock() {
        return is_lock;
    }

    public void setIs_lock(Integer is_lock) {
        this.is_lock = is_lock;
    }

    public String getIntro_video() {
        return intro_video;
    }

    public void setIntro_video(String intro_video) {
        this.intro_video = intro_video;
    }

    public String getApprove_desc() {
        return approve_desc;
    }

    public void setApprove_desc(String approve_desc) {
        this.approve_desc = approve_desc;
    }

    public String getTitle_student() {
        if(StringUtils.isBlank(this.title_student)){
            this.title_student = title;
        }
        return title_student;
    }

    public void setTitle_student(String title_student) {
        this.title_student = title_student;
    }

    public String getIntro_student() {
        if(StringUtils.isBlank(this.intro_student)){
            this.intro_student = intro;
        }
        return intro_student;
    }

    public void setIntro_student(String intro_student) {
        this.intro_student = intro_student;
    }

    public String getCover_url_student() {
        if(StringUtils.isBlank(this.cover_url_student)){
            this.cover_url_student = cover_url;
        }
        return cover_url_student;
    }

    public void setCover_url_student(String cover_url_student) {
        this.cover_url_student = cover_url_student;
    }

    public Double getTeacher_earn() {
        return teacher_earn;
    }

    public void setTeacher_earn(Double teacher_earn) {
        this.teacher_earn = teacher_earn;
    }

    public BigDecimal getStudent_price() {
        return student_price;
    }

    public void setStudent_price(BigDecimal student_price) {
        this.student_price = student_price;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Integer getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Integer category_id) {
        this.category_id = category_id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getCover_url() {
        return cover_url;
    }

    public void setCover_url(String cover_url) {
        this.cover_url = cover_url;
    }

    public Integer getApproved() {
        return approved;
    }

    public void setApproved(Integer approved) {
        this.approved = approved;
    }

    public Integer getPublished() {
        return published;
    }

    public void setPublished(Integer published) {
        this.published = published;
    }

    public Integer getPublish_user_id() {
        return publish_user_id;
    }

    public void setPublish_user_id(Integer publish_user_id) {
        this.publish_user_id = publish_user_id;
    }

    public Integer getBuy_number() {
        return buy_number;
    }

    public void setBuy_number(Integer buy_number) {
        this.buy_number = buy_number;
    }

    public Integer getStudy_number() {
        return study_number;
    }

    public void setStudy_number(Integer study_number) {
        this.study_number = study_number;
    }

    public Integer getShow_on_student() {
        return show_on_student;
    }

    public void setShow_on_student(Integer show_on_student) {
        this.show_on_student = show_on_student;
    }


    public Integer getBoutique() {
        return boutique;
    }

    public void setBoutique(Integer boutique) {
        this.boutique = boutique;
    }

    public Integer getToday_boutique() {
        return today_boutique;
    }

    public void setToday_boutique(Integer today_boutique) {
        this.today_boutique = today_boutique;
    }

    public Integer getHot() {
        return hot;
    }

    public void setHot(Integer hot) {
        this.hot = hot;
    }

    public Integer getCreate_user() {
        return create_user;
    }

    public void setCreate_user(Integer create_user) {
        this.create_user = create_user;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Integer getUpdate_user() {
        return update_user;
    }

    public void setUpdate_user(Integer update_user) {
        this.update_user = update_user;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
   /* @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }*/
}
