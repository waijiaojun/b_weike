package com.benmei.weike.entity;

/**
 * Created by Peter on 2017/9/6.
 */
public class Teacher {
    private Integer tea_id;
    private String tea_email;//教师Email
    private String equ_type;//1android 2ios
    private String equ_client_id;//设备唯一标示  Android:clientId ios:devicetoken
    private String tea_name;
    private String tea_head_portrait;//老师头像地址
    private String tea_video_intro;//老师介绍视频地址
    private Integer tea_islock;////是否验证，0:未验证，1:已验证

    public Integer getTea_islock() {
        return tea_islock;
    }

    public void setTea_islock(Integer tea_islock) {
        this.tea_islock = tea_islock;
    }

    public String getTea_head_portrait() {
        return tea_head_portrait;
    }

    public void setTea_head_portrait(String tea_head_portrait) {
        this.tea_head_portrait = tea_head_portrait;
    }

    public Integer getTea_id() {
        return tea_id;
    }

    public void setTea_id(Integer tea_id) {
        this.tea_id = tea_id;
    }

    public String getTea_email() {
        return tea_email;
    }

    public void setTea_email(String tea_email) {
        this.tea_email = tea_email;
    }

    public String getEqu_type() {
        return equ_type;
    }

    public void setEqu_type(String equ_type) {
        this.equ_type = equ_type;
    }

    public String getEqu_client_id() {
        return equ_client_id;
    }

    public void setEqu_client_id(String equ_client_id) {
        this.equ_client_id = equ_client_id;
    }

    public String getTea_name() {
        return tea_name;
    }

    public void setTea_name(String tea_name) {
        this.tea_name = tea_name;
    }

    public String getTea_video_intro() {
        return tea_video_intro;
    }

    public void setTea_video_intro(String tea_video_intro) {
        this.tea_video_intro = tea_video_intro;
    }
}
