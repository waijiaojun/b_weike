package com.benmei.weike.entity;

import java.util.Map;

/**
 * Created by Peter on 2017/9/21.
 */
public class ChargeNotifyLog {
    private Integer id;
    private String result;
    private String ping_id;//": "ch_Xsr7u35O3m1Gw4ed2ODmi4Lw",
    private String object;//": "charge",
    private Long created;//": 1427555076,
    private String livemode;//": true,
    private String paid;//": true,
    private String refunded;//": false,
    private String reversed;//": false,
    private String app;//": "app_1Gqj58ynP0mHeX1q",
    private String channel;//": "upacp",
    private String order_no;//": "123456789",
    private String client_ip;//": "127.0.0.1",
    private Long amount;//": 100,
    private Long amount_settle;//": 100,
    private String currency;//": "cny",
    private String subject;//": "Your Subject",
    private String body;//": "Your Body",
    private Long time_paid;//": 1427555101,
    private Long time_expire;//": 1427641476,
    private Long time_settle;//": null,
    private String transaction_no;//": "1224524301201505066067849274",
    private Long amount_refunded;//": 0,
    private String failure_code;//": null,
    private String failure_msg;//": null,
    private String description;//": null
    private String httpPayload;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        if (this.result == null) {
            this.result = "";
        }
        this.result = this.result + ";; " + result;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public String getHttpPayload() {
        return httpPayload;
    }

    public void setHttpPayload(String httpPayload) {
        this.httpPayload = httpPayload;
    }

    public ChargeNotifyLog() {

    }

    public ChargeNotifyLog(Map<String, Object> paymentInfo) {
        this.ping_id = getStringValue(paymentInfo.get("id"));
        this.object = getStringValue(paymentInfo.get("object"));
        this.created = getLongValue(paymentInfo.get("created"));
        this.livemode = getStringValue(paymentInfo.get("livemode"));
        this.paid = getStringValue(paymentInfo.get("paid"));
        this.refunded = getStringValue(paymentInfo.get("refunded"));
        this.reversed = getStringValue(paymentInfo.get("reversed"));
        this.app = getStringValue(paymentInfo.get("app"));
        this.channel = getStringValue(paymentInfo.get("channel"));
        this.order_no = getStringValue(paymentInfo.get("order_no"));
        this.client_ip = getStringValue(paymentInfo.get("client_ip"));
        this.amount = getLongValue(paymentInfo.get("amount"));
        this.amount_settle = getLongValue(paymentInfo.get("amount_settle"));
        this.currency = getStringValue(paymentInfo.get("currency"));
        this.subject = getStringValue(paymentInfo.get("subject"));
        this.body = getStringValue(paymentInfo.get("body"));
        this.time_paid = getLongValue(paymentInfo.get("time_paid"));
        this.time_expire = getLongValue(paymentInfo.get("time_expire"));
        this.time_settle = getLongValue(paymentInfo.get("time_settle"));
        this.transaction_no = getStringValue(paymentInfo.get("transaction_no"));
        this.amount_refunded = getLongValue(paymentInfo.get("amount_refunded"));
        this.failure_code = getStringValue(paymentInfo.get("failure_code"));
        this.failure_msg = getStringValue(paymentInfo.get("failure_msg"));
        this.description = getStringValue(paymentInfo.get("description"));
    }

    private String getStringValue(Object value) {
        String result = "";
        if (value != null) {
            result = value.toString();
        }
        return result;
    }

    private Long getLongValue(Object value) {
        Long result = -1L;
        if (value != null) {
            result = Long.valueOf(value.toString());
        }
        return result;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPing_id() {
        return ping_id;
    }

    public void setPing_id(String ping_id) {
        this.ping_id = ping_id;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public Long getCreated() {
        return created;
    }

    public String getLivemode() {
        return livemode;
    }

    public void setLivemode(String livemode) {
        this.livemode = livemode;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public String getRefunded() {
        return refunded;
    }

    public void setRefunded(String refunded) {
        this.refunded = refunded;
    }

    public String getReversed() {
        return reversed;
    }

    public void setReversed(String reversed) {
        this.reversed = reversed;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getClient_ip() {
        return client_ip;
    }

    public void setClient_ip(String client_ip) {
        this.client_ip = client_ip;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Long getAmount_settle() {
        return amount_settle;
    }

    public void setAmount_settle(Long amount_settle) {
        this.amount_settle = amount_settle;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Long getTime_paid() {
        return time_paid;
    }

    public void setTime_paid(Long time_paid) {
        this.time_paid = time_paid;
    }

    public Long getTime_expire() {
        return time_expire;
    }

    public void setTime_expire(Long time_expire) {
        this.time_expire = time_expire;
    }

    public Long getTime_settle() {
        return time_settle;
    }

    public void setTime_settle(Long time_settle) {
        this.time_settle = time_settle;
    }

    public String getTransaction_no() {
        return transaction_no;
    }

    public void setTransaction_no(String transaction_no) {
        this.transaction_no = transaction_no;
    }

    public Long getAmount_refunded() {
        return amount_refunded;
    }

    public void setAmount_refunded(Long amount_refunded) {
        this.amount_refunded = amount_refunded;
    }

    public String getFailure_code() {
        return failure_code;
    }

    public void setFailure_code(String failure_code) {
        this.failure_code = failure_code;
    }

    public String getFailure_msg() {
        return failure_msg;
    }

    public void setFailure_msg(String failure_msg) {
        this.failure_msg = failure_msg;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
