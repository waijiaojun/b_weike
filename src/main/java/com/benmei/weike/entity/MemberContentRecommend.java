package com.benmei.weike.entity;

import java.util.Date;

/**
 * Created by Peter on 2017/6/10.
 */
public class MemberContentRecommend {
    public static class ContentType{
        public static final String weike="weike";
        public static final String live="live";
        public static final String course="course";
    }
    private Integer id;//	int	11	0	0	-1	0	0	0		0					-1	0
    private Integer memb_id;//	int	11	0	0	0	0	0	0		0					0	0
    private Integer content_id;//	int	11	0	0	0	0	0	0		0	根据content_type，分别对应weike_id，live_id，course_id				0	0
    private String content_type;//	varchar	255	0	0	0	0	0	0	'weike'	0	weike:微课，live:直播，course:课程	utf8mb4	utf8mb4_general_ci		0	0
    private Integer send_count;//	int	11	0	0	0	0	0	0	0	0	推荐次数，展示次数				0	0
    private Integer click_count;//	int	11	0	0	0	0	0	0	0	0	点击次数				0	0
    private Date create_date;//	timestamp	0	0	0	0	0	0	0	CURRENT_TIMESTAMP	0	create_date				0	0
    private Date update_date;//	timestamp	0	0	0	0	0	0	0	CURRENT_TIMESTAMP	-1	update_date				0	0


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMemb_id() {
        return memb_id;
    }

    public void setMemb_id(Integer memb_id) {
        this.memb_id = memb_id;
    }

    public Integer getContent_id() {
        return content_id;
    }

    public void setContent_id(Integer content_id) {
        this.content_id = content_id;
    }

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }

    public Integer getSend_count() {
        return send_count;
    }

    public void setSend_count(Integer send_count) {
        this.send_count = send_count;
    }

    public Integer getClick_count() {
        return click_count;
    }

    public void setClick_count(Integer click_count) {
        this.click_count = click_count;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }
}
