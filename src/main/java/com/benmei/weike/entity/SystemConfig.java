package com.benmei.weike.entity;

import java.io.Serializable;

/**
 * Created by Peter on 2017/7/8.
 * 系统配置信息
 */
public class SystemConfig implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private String category;
    private String name;
    private String code;
    private String value;
    private String desc;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
