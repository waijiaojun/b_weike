package com.benmei.weike.entity;

import java.util.Date;

/**
 * 学生购买的微课
 * Created by Peter on 2017/4/13.
 */
public class WeikeStudentBuy {
    private Long id;//            bigint not null auto_increment,
    private Integer memb_id;//           int,
    private Long weike_id;//         bigint,
    private Long study_number;//       bigint comment '学习次数',
    private Integer payment_status;// 支付状态 0 未付款 1 转账中 2 付款成功 3 付款失败
    private Integer is_vip_view; //0:非会员购买    1:会员查看
    private Integer create_user;//        int not null,
    private Date create_date;//     datetime not null,
    private Integer update_user;//        int not null,
    private Date update_date;//     datetime not null,
    private Integer teacher_charged;// 是否给老师提成：0否，1是
    private Integer deleted;//        int not null default 0 comment '数据删除标记：0使用；1删除',

    public Integer getTeacher_charged() {
        return teacher_charged;
    }

    public void setTeacher_charged(Integer teacher_charged) {
        this.teacher_charged = teacher_charged;
    }

    public Integer getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(Integer payment_status) {
        this.payment_status = payment_status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMemb_id() {
        return memb_id;
    }

    public void setMemb_id(Integer memb_id) {
        this.memb_id = memb_id;
    }

    public Long getWeike_id() {
        return weike_id;
    }

    public void setWeike_id(Long weike_id) {
        this.weike_id = weike_id;
    }

    public Long getStudy_number() {
        return study_number;
    }

    public void setStudy_number(Long study_number) {
        this.study_number = study_number;
    }

    public Integer getIs_vip_view() {
        return is_vip_view;
    }

    public void setIs_vip_view(Integer is_vip_view) {
        this.is_vip_view = is_vip_view;
    }

    public Integer getCreate_user() {
        return create_user;
    }

    public void setCreate_user(Integer create_user) {
        this.create_user = create_user;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Integer getUpdate_user() {
        return update_user;
    }

    public void setUpdate_user(Integer update_user) {
        this.update_user = update_user;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }


    /**
     * 支付状态 0 未付款 1 转账中 2 付款成功 3 付款失败
     */
    public static class PaymentStatus {
        /**
         * 0 未付款
         */
        public static final Integer UN_PAY = 0;

        /**
         * 1 转账中
         */
        public static final Integer PAYING = 1;

        /**
         * 2 付款成功
         */
        public static final Integer PAY_SUCCESS = 2;

        /**
         * 3 付款失败
         */
        public static final Integer PAY_FAIL = 3;
    }

}
