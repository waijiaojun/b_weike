package com.benmei.weike.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Transient;
import java.util.Date;

/**
 * Created by Peter on 2017/4/13.
 */
public class WeikeImage {
    private Long id;//          bigint not null auto_increment,
    private Long weike_id;//          bigint,
    private Integer weike_part_sort;//      int,
    private String url;//        varchar(512),
    private Integer sort;//        int comment '图片顺序',
    private Integer create_user;//         int not null,
    private Date create_date;//      datetime not null,
    private Integer update_user;//         int not null,
    private Date update_date;//         datetime not null,
    private Integer deleted;//         int not null default 0 comment '数据删除标记：0使用；1删除',
    private Integer published; //发布状态 ： 0 ：未发布      1：已发布
    @JsonIgnore
    @Transient
    private MultipartFile file;
    @JsonIgnore
    @Transient
    private String clientFieldName;

    public String getClientFieldName() {
        return clientFieldName;
    }

    public void setClientFieldName(String clientFieldName) {
        this.clientFieldName = clientFieldName;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getWeike_id() {
        return weike_id;
    }

    public void setWeike_id(Long weike_id) {
        this.weike_id = weike_id;
    }

    public Integer getWeike_part_sort() {
        return weike_part_sort;
    }

    public void setWeike_part_sort(Integer weike_part_sort) {
        this.weike_part_sort = weike_part_sort;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getCreate_user() {
        return create_user;
    }

    public void setCreate_user(Integer create_user) {
        this.create_user = create_user;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Integer getUpdate_user() {
        return update_user;
    }

    public void setUpdate_user(Integer update_user) {
        this.update_user = update_user;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Integer getPublished() {
        return published;
    }

    public void setPublished(Integer published) {
        this.published = published;
    }
}
