package com.benmei.weike.entity;

/**
 * Created by Peter on 2017/10/28.
 */
public class WeikeTeacherHourCharge {
    private Integer id;//` int(11) NOT NULL AUTO_INCREMENT,
    private Long weike_id;//` bigint(20) DEFAULT NULL,
    private Integer audio_total_duration;//` int(11) DEFAULT '0' COMMENT '计费时统计的语音总时长(秒)',
    private Integer charge_hour;//` int(11) DEFAULT NULL COMMENT '计费的小时数，如：2，表示老师提成了2个小时的费用',

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getWeike_id() {
        return weike_id;
    }

    public void setWeike_id(Long weike_id) {
        this.weike_id = weike_id;
    }

    public Integer getAudio_total_duration() {
        return audio_total_duration;
    }

    public void setAudio_total_duration(Integer audio_total_duration) {
        this.audio_total_duration = audio_total_duration;
    }

    public Integer getCharge_hour() {
        return charge_hour;
    }

    public void setCharge_hour(Integer charge_hour) {
        this.charge_hour = charge_hour;
    }
}
