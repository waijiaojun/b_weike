package com.benmei.weike.entity;

import java.util.Date;

/**
 * 微课评论
 * Created by Peter on 2017/5/4.
 */
public class WeikeComments {
    private Long id;                   //bigint not null auto_increment,
    private Long weike_id;            //bigint,
    private String content;        //varchar(512),
    private Integer score;        // int,
    private Integer memb_id;        // int not null,
    private String reply;         // varchar(512),
    private Integer tea_id;        //   int,
    private Date create_date;        //timestamp not null,
    private Integer update_user;     //   int not null,
    private Date update_date;        //timestamp not null,
    private Integer deleted;       // int not null default 0 comment '数据删除标记：0使用；1删除'

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getWeike_id() {
        return weike_id;
    }

    public void setWeike_id(Long weike_id) {
        this.weike_id = weike_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getMemb_id() {
        return memb_id;
    }

    public void setMemb_id(Integer memb_id) {
        this.memb_id = memb_id;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public Integer getTea_id() {
        return tea_id;
    }

    public void setTea_id(Integer tea_id) {
        this.tea_id = tea_id;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Integer getUpdate_user() {
        return update_user;
    }

    public void setUpdate_user(Integer update_user) {
        this.update_user = update_user;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
}
