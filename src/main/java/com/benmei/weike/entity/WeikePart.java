package com.benmei.weike.entity;

import java.util.Date;

/**
 * Created by Peter on 2017/4/13.
 */
public class WeikePart {
    private Long id;//                  bigint not null auto_increment,
    private String title;//         varchar(255),
    private Integer sort;//
    private Long weike_id;//         bigint,
    private Integer create_user;//      int not null,
    private Date create_date;//       datetime not null,
    private Integer update_user;//       int not null,
    private Date update_date;//        datetime not null,
    private Integer deleted;//           int not null default 0 comment '数据删除标记：0使用；1删除',
    private Integer published; //发布状态 0：未发布   1：已发布

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getWeike_id() {
        return weike_id;
    }

    public void setWeike_id(Long weike_id) {
        this.weike_id = weike_id;
    }

    public Integer getCreate_user() {
        return create_user;
    }

    public void setCreate_user(Integer create_user) {
        this.create_user = create_user;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Integer getUpdate_user() {
        return update_user;
    }

    public void setUpdate_user(Integer update_user) {
        this.update_user = update_user;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Integer getPublished() {
        return published;
    }

    public void setPublished(Integer published) {
        this.published = published;
    }
}
