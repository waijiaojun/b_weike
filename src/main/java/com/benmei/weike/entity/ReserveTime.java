package com.benmei.weike.entity;

import java.util.Date;

/**
 * Created by Peter on 2018/1/3.
 */
public class ReserveTime {
    public static class TimeStatus{
        public static final int OPEN = 0;//开
        public static final int CLOS = 1;//关
        public static final int BOOKED = 2;//被预约
    }
    private Integer reserve_time_id;//` int(11) NOT NULL AUTO_INCREMENT,
    private Integer tea_id;//` int(11) DEFAULT NULL,
    private Integer reserve_week;//` int(1) DEFAULT NULL,
    private Integer reserve_time;//` int(2) DEFAULT NULL,
    private Integer is_valid;//` int(1) DEFAULT NULL COMMENT '是否可用 0:是,1:否,2:被预约',
    private Date date;//` timestamp NULL DEFAULT NULL,

    public Integer getReserve_time_id() {
        return reserve_time_id;
    }

    public void setReserve_time_id(Integer reserve_time_id) {
        this.reserve_time_id = reserve_time_id;
    }

    public Integer getTea_id() {
        return tea_id;
    }

    public void setTea_id(Integer tea_id) {
        this.tea_id = tea_id;
    }

    public Integer getReserve_week() {
        return reserve_week;
    }

    public void setReserve_week(Integer reserve_week) {
        this.reserve_week = reserve_week;
    }

    public Integer getReserve_time() {
        return reserve_time;
    }

    public void setReserve_time(Integer reserve_time) {
        this.reserve_time = reserve_time;
    }

    public Integer getIs_valid() {
        return is_valid;
    }

    public void setIs_valid(Integer is_valid) {
        this.is_valid = is_valid;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
