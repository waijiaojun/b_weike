package com.benmei.weike.entity;

/**
 * Created by waijiaojun on 2017/9/8.
 */
public class WeikeMemberViewerCount {
    private String id;
    private Long weike_id;   //微课ID
    private Integer memb_id;     //会员ID
    private Integer play_number;// 播放次数

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getWeike_id() {
        return weike_id;
    }

    public void setWeike_id(Long weike_id) {
        this.weike_id = weike_id;
    }

    public Integer getMemb_id() {
        return memb_id;
    }

    public void setMemb_id(Integer memb_id) {
        this.memb_id = memb_id;
    }

    public Integer getPlay_number() {
        return play_number;
    }

    public void setPlay_number(Integer play_number) {
        this.play_number = play_number;
    }

}
