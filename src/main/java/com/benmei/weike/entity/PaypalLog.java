package com.benmei.weike.entity;

import com.benmei.weike.paypal.GetPaymentDetail;

import java.util.Date;

public class PaypalLog {
    private Integer id;
    private String payment_id;
    private String payment_state;
    private String payment_cart;
    private String payer_status;
    private String payer_info_email;
    private String payer_info_id;
    private String payer_info_phone;
    private String payer_info_country_code;
    private String transactions_amount_total;
    private String transactions_amount_currency;
    private String transactions_details_subtotal;
    private String transactions_payee_merchant_id;
    private String transactions_description;
    private String transactions_related_resources_sale_id;
    private String transactions_related_resources_state;
    private String transactions_related_resources_amount_total;
    private String transactions_related_resources_amount_currency;
    private String transactions_related_resources_amount_subtotal;
    private String transactions_related_resources_payment_mode;
    private String transactions_related_resources_protection_eligibility;
    private String transactions_related_resources_protection_eligibility_type;
    private String transactions_related_resources_protection_transaction_fee_value;
    private String transactions_related_resources_protection_transaction_fee_curren;
    private String transactions_related_resources_parent_payment;
    private Date transactions_related_resources_create_time;
    private Date transactions_related_resources_update_time;
    private Date payment_create_time;

    public PaypalLog(GetPaymentDetail getPaymentDetail){
        this.payment_id = getPaymentDetail.getId();
        this.payment_state = getPaymentDetail.getState();
        this.payment_cart = getPaymentDetail.getCart();
        this.payer_status = getPaymentDetail.getPayer().getStatus();
        this.payer_info_email = getPaymentDetail.getPayer().getPayer_info().getEmail();
        this.payer_info_id = getPaymentDetail.getPayer().getPayer_info().getPayer_id();
        this.payer_info_phone = getPaymentDetail.getPayer().getPayer_info().getPhone();
        this.payer_info_country_code = getPaymentDetail.getPayer().getPayer_info().getCountry_code();
        this.transactions_amount_total = getPaymentDetail.getTransactions().get(0).getAmount().getTotal();
        this.transactions_amount_currency = getPaymentDetail.getTransactions().get(0).getAmount().getCurrency();
        this.transactions_details_subtotal = getPaymentDetail.getTransactions().get(0).getAmount().getDetails().getSubtotal();
        this.transactions_payee_merchant_id = getPaymentDetail.getTransactions().get(0).getPayee().getMerchant_id();
        this.transactions_description = getPaymentDetail.getTransactions().get(0).getDescription();
        this.transactions_related_resources_sale_id = getPaymentDetail.getTransactions().get(0).getRelated_resources().get(0).getSale().getId();
        this.transactions_related_resources_state = getPaymentDetail.getTransactions().get(0).getRelated_resources().get(0).getSale().getState();
        this.transactions_related_resources_amount_total = getPaymentDetail.getTransactions().get(0).getRelated_resources().get(0).getSale().getAmount().getTotal();

        this.transactions_related_resources_amount_currency = getPaymentDetail.getTransactions().get(0).getRelated_resources().get(0).getSale().getAmount().getCurrency();
        this.transactions_related_resources_amount_subtotal = getPaymentDetail.getTransactions().get(0).getRelated_resources().get(0).getSale().getAmount().getDetails().getSubtotal();
        this.transactions_related_resources_payment_mode = getPaymentDetail.getTransactions().get(0).getRelated_resources().get(0).getSale().getPayment_mode();
        this.transactions_related_resources_protection_eligibility = getPaymentDetail.getTransactions().get(0).getRelated_resources().get(0).getSale().getProtection_eligibility();
        this.transactions_related_resources_protection_eligibility_type = getPaymentDetail.getTransactions().get(0).getRelated_resources().get(0).getSale().getProtection_eligibility_type();
        this.transactions_related_resources_protection_transaction_fee_value = getPaymentDetail.getTransactions().get(0).getRelated_resources().get(0).getSale().getTransaction_fee().getValue();
        this.transactions_related_resources_protection_transaction_fee_curren = getPaymentDetail.getTransactions().get(0).getRelated_resources().get(0).getSale().getTransaction_fee().getCurrency();
        this.transactions_related_resources_parent_payment = getPaymentDetail.getTransactions().get(0).getRelated_resources().get(0).getSale().getParent_payment();
        this.transactions_related_resources_create_time = getPaymentDetail.getTransactions().get(0).getRelated_resources().get(0).getSale().getCreate_time();
        this.transactions_related_resources_update_time = getPaymentDetail.getTransactions().get(0).getRelated_resources().get(0).getSale().getUpdate_time();
        this.payment_create_time = getPaymentDetail.getCreate_time();

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(String payment_id) {
        this.payment_id = payment_id;
    }

    public String getPayment_state() {
        return payment_state;
    }

    public void setPayment_state(String payment_state) {
        this.payment_state = payment_state;
    }

    public String getPayment_cart() {
        return payment_cart;
    }

    public void setPayment_cart(String payment_cart) {
        this.payment_cart = payment_cart;
    }

    public String getPayer_status() {
        return payer_status;
    }

    public void setPayer_status(String payer_status) {
        this.payer_status = payer_status;
    }

    public String getPayer_info_email() {
        return payer_info_email;
    }

    public void setPayer_info_email(String payer_info_email) {
        this.payer_info_email = payer_info_email;
    }

    public String getPayer_info_id() {
        return payer_info_id;
    }

    public void setPayer_info_id(String payer_info_id) {
        this.payer_info_id = payer_info_id;
    }

    public String getPayer_info_phone() {
        return payer_info_phone;
    }

    public void setPayer_info_phone(String payer_info_phone) {
        this.payer_info_phone = payer_info_phone;
    }

    public String getPayer_info_country_code() {
        return payer_info_country_code;
    }

    public void setPayer_info_country_code(String payer_info_country_code) {
        this.payer_info_country_code = payer_info_country_code;
    }

    public String getTransactions_amount_total() {
        return transactions_amount_total;
    }

    public void setTransactions_amount_total(String transactions_amount_total) {
        this.transactions_amount_total = transactions_amount_total;
    }

    public String getTransactions_amount_currency() {
        return transactions_amount_currency;
    }

    public void setTransactions_amount_currency(String transactions_amount_currency) {
        this.transactions_amount_currency = transactions_amount_currency;
    }

    public String getTransactions_details_subtotal() {
        return transactions_details_subtotal;
    }

    public void setTransactions_details_subtotal(String transactions_details_subtotal) {
        this.transactions_details_subtotal = transactions_details_subtotal;
    }

    public String getTransactions_payee_merchant_id() {
        return transactions_payee_merchant_id;
    }

    public void setTransactions_payee_merchant_id(String transactions_payee_merchant_id) {
        this.transactions_payee_merchant_id = transactions_payee_merchant_id;
    }

    public String getTransactions_description() {
        return transactions_description;
    }

    public void setTransactions_description(String transactions_description) {
        this.transactions_description = transactions_description;
    }

    public String getTransactions_related_resources_sale_id() {
        return transactions_related_resources_sale_id;
    }

    public void setTransactions_related_resources_sale_id(String transactions_related_resources_sale_id) {
        this.transactions_related_resources_sale_id = transactions_related_resources_sale_id;
    }

    public String getTransactions_related_resources_state() {
        return transactions_related_resources_state;
    }

    public void setTransactions_related_resources_state(String transactions_related_resources_state) {
        this.transactions_related_resources_state = transactions_related_resources_state;
    }

    public String getTransactions_related_resources_amount_total() {
        return transactions_related_resources_amount_total;
    }

    public void setTransactions_related_resources_amount_total(String transactions_related_resources_amount_total) {
        this.transactions_related_resources_amount_total = transactions_related_resources_amount_total;
    }

    public String getTransactions_related_resources_amount_currency() {
        return transactions_related_resources_amount_currency;
    }

    public void setTransactions_related_resources_amount_currency(String transactions_related_resources_amount_currency) {
        this.transactions_related_resources_amount_currency = transactions_related_resources_amount_currency;
    }

    public String getTransactions_related_resources_amount_subtotal() {
        return transactions_related_resources_amount_subtotal;
    }

    public void setTransactions_related_resources_amount_subtotal(String transactions_related_resources_amount_subtotal) {
        this.transactions_related_resources_amount_subtotal = transactions_related_resources_amount_subtotal;
    }

    public String getTransactions_related_resources_payment_mode() {
        return transactions_related_resources_payment_mode;
    }

    public void setTransactions_related_resources_payment_mode(String transactions_related_resources_payment_mode) {
        this.transactions_related_resources_payment_mode = transactions_related_resources_payment_mode;
    }

    public String getTransactions_related_resources_protection_eligibility() {
        return transactions_related_resources_protection_eligibility;
    }

    public void setTransactions_related_resources_protection_eligibility(String transactions_related_resources_protection_eligibility) {
        this.transactions_related_resources_protection_eligibility = transactions_related_resources_protection_eligibility;
    }

    public String getTransactions_related_resources_protection_eligibility_type() {
        return transactions_related_resources_protection_eligibility_type;
    }

    public void setTransactions_related_resources_protection_eligibility_type(String transactions_related_resources_protection_eligibility_type) {
        this.transactions_related_resources_protection_eligibility_type = transactions_related_resources_protection_eligibility_type;
    }

    public String getTransactions_related_resources_protection_transaction_fee_value() {
        return transactions_related_resources_protection_transaction_fee_value;
    }

    public void setTransactions_related_resources_protection_transaction_fee_value(String transactions_related_resources_protection_transaction_fee_value) {
        this.transactions_related_resources_protection_transaction_fee_value = transactions_related_resources_protection_transaction_fee_value;
    }

    public String getTransactions_related_resources_protection_transaction_fee_curren() {
        return transactions_related_resources_protection_transaction_fee_curren;
    }

    public void setTransactions_related_resources_protection_transaction_fee_curren(String transactions_related_resources_protection_transaction_fee_curren) {
        this.transactions_related_resources_protection_transaction_fee_curren = transactions_related_resources_protection_transaction_fee_curren;
    }

    public String getTransactions_related_resources_parent_payment() {
        return transactions_related_resources_parent_payment;
    }

    public void setTransactions_related_resources_parent_payment(String transactions_related_resources_parent_payment) {
        this.transactions_related_resources_parent_payment = transactions_related_resources_parent_payment;
    }

    public Date getTransactions_related_resources_create_time() {
        return transactions_related_resources_create_time;
    }

    public void setTransactions_related_resources_create_time(Date transactions_related_resources_create_time) {
        this.transactions_related_resources_create_time = transactions_related_resources_create_time;
    }

    public Date getTransactions_related_resources_update_time() {
        return transactions_related_resources_update_time;
    }

    public void setTransactions_related_resources_update_time(Date transactions_related_resources_update_time) {
        this.transactions_related_resources_update_time = transactions_related_resources_update_time;
    }

    public Date getPayment_create_time() {
        return payment_create_time;
    }

    public void setPayment_create_time(Date payment_create_time) {
        this.payment_create_time = payment_create_time;
    }
}
