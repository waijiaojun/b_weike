package com.benmei.weike.entity;

import java.sql.Timestamp;

public class TsNtSendHis {

    private static final long serialVersionUID = 1L;

    private Integer sms_his_id;
    private Integer sms_provider_id;
    private String phoneno;
    private String sms_type;
    private String yyyymmdd;
    private Integer template_id;
    private Timestamp send_time;

    public Integer getSms_his_id() {
        return sms_his_id;
    }

    public void setSms_his_id(Integer sms_his_id) {
        this.sms_his_id = sms_his_id;
    }

    public Integer getSms_provider_id() {
        return sms_provider_id;
    }

    public void setSms_provider_id(Integer sms_provider_id) {
        this.sms_provider_id = sms_provider_id;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public String getSms_type() {
        return sms_type;
    }

    public void setSms_type(String sms_type) {
        this.sms_type = sms_type;
    }

    public Integer getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(Integer template_id) {
        this.template_id = template_id;
    }

    public Timestamp getSend_time() {
        return send_time;
    }

    public void setSend_time(Timestamp send_time) {
        this.send_time = send_time;
    }

    public String getYyyymmdd() {
        return yyyymmdd;
    }

    public void setYyyymmdd(String yyyymmdd) {
        this.yyyymmdd = yyyymmdd;
    }

    public static long getSerialVersionUID() {

        return serialVersionUID;
    }
}
