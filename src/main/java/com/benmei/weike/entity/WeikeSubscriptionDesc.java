package com.benmei.weike.entity;

/**
 * Created by Peter on 2017/9/22.
 */
public class WeikeSubscriptionDesc {

    private Integer id;
    private String detail;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
