package com.benmei.weike.entity;

import java.util.Date;

/**
 * Created by Peter on 2017/9/8.
 */
public class ClassRoomEntryLog {
    private Long id;//                  bigint not null,
    private Integer room_id;//         int,
    private Integer user_role;//         int,1 学生；2 老师
    private Integer user_id;//         int,
    private Date event_date;//        datetime,
    private String event_type;//  inter 进入教室，leave离开教室
    private String leave_reason;

    public String getLeave_reason() {
        return leave_reason;
    }

    public void setLeave_reason(String leave_reason) {
        this.leave_reason = leave_reason;
    }

    public Date getEvent_date() {
        return event_date;
    }

    public void setEvent_date(Date event_date) {
        this.event_date = event_date;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getUser_role() {
        return user_role;
    }

    public void setUser_role(Integer user_role) {
        this.user_role = user_role;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getRoom_id() {
        return room_id;
    }

    public void setRoom_id(Integer room_id) {
        this.room_id = room_id;
    }
}
