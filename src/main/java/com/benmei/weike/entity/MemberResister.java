package com.benmei.weike.entity;

import java.math.BigDecimal;

/**
 * Created by waijiaojun on 2017/11/8.
 */
public class MemberResister {
    private String memb_id;
    private String memb_phone;   //用户手机号
    private String memb_password;   //用户密码
    private String memb_phone_area; //手机号区域
    private String memb_register_month; //注册月份
    private String memb_islock;         //用户锁定
    private String token;               //用户token
    private String memb_head_portrait;      //用户头像
    private BigDecimal memb_balance;        //用户账户
    private String  memb_sex;        //用户性别
    private String memb_name;   //用户昵称
    private String small_program_open_id;   //用户openID

    public String getMemb_id() {
        return memb_id;
    }

    public void setMemb_id(String memb_id) {
        this.memb_id = memb_id;
    }

    public String getMemb_phone() {

        return memb_phone;
    }

    public void setMemb_phone(String memb_phone) {
        this.memb_phone = memb_phone;
    }

    public String getMemb_password() {
        return memb_password;
    }

    public void setMemb_password(String memb_password) {
        this.memb_password = memb_password;
    }

    public String getMemb_phone_area() {
        return memb_phone_area;
    }

    public void setMemb_phone_area(String memb_phone_area) {
        this.memb_phone_area = memb_phone_area;
    }

    public String getMemb_register_month() {
        return memb_register_month;
    }

    public void setMemb_register_month(String memb_register_month) {
        this.memb_register_month = memb_register_month;
    }

    public String getMemb_islock() {
        return memb_islock;
    }

    public void setMemb_islock(String memb_islock) {
        this.memb_islock = memb_islock;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMemb_head_portrait() {
        return memb_head_portrait;
    }

    public void setMemb_head_portrait(String memb_head_portrait) {
        this.memb_head_portrait = memb_head_portrait;
    }

    public BigDecimal getMemb_balance() {
        return memb_balance;
    }

    public void setMemb_balance(BigDecimal memb_balance) {
        this.memb_balance = memb_balance;
    }

    public String getMemb_sex() {
        return memb_sex;
    }

    public void setMemb_sex(String memb_sex) {
        this.memb_sex = memb_sex;
    }

    public String getMemb_name() {
        return memb_name;
    }

    public void setMemb_name(String memb_name) {
        this.memb_name = memb_name;
    }

    public String getSmall_program_open_id() {
        return small_program_open_id;
    }

    public void setSmall_program_open_id(String small_program_open_id) {
        this.small_program_open_id = small_program_open_id;
    }
}
