package com.benmei.weike.entity;

import com.pingplusplus.model.Charge;

import java.util.Date;

/**
 * Created by Peter on 2017/9/19.
 */
public class Order {
    private Integer id;
    private String ping_id;// Ping++ 的订单id， "ch_Sir10K0OuH0KqjD0WH1aLqXD",
    private String ping_object;//": "charge",
    private Date ping_created;//": 1505810247,
    private String ping_livemode;//": true,
    private String ping_paid;//": false,是否已付款。
    private String ping_refunded;//": false,是否存在退款信息，无论退款是否成功。
    private String ping_app;//": "app_OiXzDS9evH0KifPa",
    private String ping_channel;//": "wx",
    private String ping_orderNo;//": "170919163725413",
    private String ping_clientIp;//": "192.168.15.42",
    private Integer ping_amount;//": 1,订单总金额（必须大于 0），单位为对应币种的最小货币单位，人民币为分。如订单总金额为 1 元， amount 为 100
    private Integer ping_amountSettle;//": 1,清算金额，单位为对应币种的最小货币单位，人民币为分。
    private String ping_currency;//": "cny",三位 ISO 货币代码，目前仅支持人民币  cny 。
    private String ping_subject;//": "外教君充值" ,商品标题，该参数最长为 32 个 Unicode 字符。
    private String ping_body;//": "外教君充值", 商品描述信息，该参数最长为 128 个 Unicode 字符。
    private Date ping_timePaid;//": null,订单支付完成时的 Unix 时间戳。（银联支付成功时间为接收异步通知的时间）
    private Date ping_timeExpire;//": 1505817447,订单失效时间，用 Unix 时间戳表示。时间范围在订单创建后的 1 分钟到 15 天，默认为 1 天，创建时间以 Ping++ 服务器时间为准。 微信对该参数的有效值限制为 2 小时内；银联对该参数的有效值限制为 1 小时内。
    private Date ping_timeSettle;//": null,订单清算时间，用 Unix 时间戳表示。（暂不生效）
    private String ping_transactionNo;//": null,支付渠道返回的交易流水号。

    public Order() {
    }

    public Order(Charge charge) {
        this.ping_id = charge.getId();
        this.ping_object = charge.getObject();
        if (charge.getCreated() == null) {
            this.ping_created = new Date();
        } else {
            this.ping_created = new Date(charge.getCreated());
        }

        this.ping_livemode = charge.getLivemode().toString();
        this.ping_paid = charge.getPaid().toString();
        this.ping_refunded = charge.getRefunded().toString();
        this.ping_app = charge.getApp().toString();
        this.ping_channel = charge.getChannel();
        this.ping_orderNo = charge.getOrderNo();
        this.ping_clientIp = charge.getClientIp();
        this.ping_amount = charge.getAmount();
        this.ping_amountSettle = charge.getAmountSettle();
        this.ping_currency = charge.getCurrency();
        this.ping_subject = charge.getSubject();
        this.ping_body = charge.getBody();

        if (charge.getTimePaid() == null) {
            this.ping_timePaid = new Date();
        } else {
            this.ping_timePaid = new Date(charge.getTimePaid());
        }

        if (charge.getTimeExpire() == null) {
            this.ping_timeExpire = new Date();
        } else {
            this.ping_timeExpire = new Date(charge.getTimeExpire());
        }

        if (charge.getTimeSettle() == null) {
            this.ping_timeSettle = new Date();
        } else {
            this.ping_timeSettle = new Date(charge.getTimeSettle());
        }
        this.ping_transactionNo = charge.getTransactionNo();
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPing_id() {
        return ping_id;
    }

    public void setPing_id(String ping_id) {
        this.ping_id = ping_id;
    }

    public String getPing_object() {
        return ping_object;
    }

    public void setPing_object(String ping_object) {
        this.ping_object = ping_object;
    }

    public String getPing_livemode() {
        return ping_livemode;
    }

    public void setPing_livemode(String ping_livemode) {
        this.ping_livemode = ping_livemode;
    }

    public String getPing_paid() {
        return ping_paid;
    }

    public void setPing_paid(String ping_paid) {
        this.ping_paid = ping_paid;
    }

    public String getPing_refunded() {
        return ping_refunded;
    }

    public void setPing_refunded(String ping_refunded) {
        this.ping_refunded = ping_refunded;
    }

    public String getPing_app() {
        return ping_app;
    }

    public void setPing_app(String ping_app) {
        this.ping_app = ping_app;
    }

    public String getPing_channel() {
        return ping_channel;
    }

    public void setPing_channel(String ping_channel) {
        this.ping_channel = ping_channel;
    }

    public String getPing_orderNo() {
        return ping_orderNo;
    }

    public void setPing_orderNo(String ping_orderNo) {
        this.ping_orderNo = ping_orderNo;
    }

    public String getPing_clientIp() {
        return ping_clientIp;
    }

    public void setPing_clientIp(String ping_clientIp) {
        this.ping_clientIp = ping_clientIp;
    }

    public Integer getPing_amount() {
        return ping_amount;
    }

    public void setPing_amount(Integer ping_amount) {
        this.ping_amount = ping_amount;
    }

    public Integer getPing_amountSettle() {
        return ping_amountSettle;
    }

    public void setPing_amountSettle(Integer ping_amountSettle) {
        this.ping_amountSettle = ping_amountSettle;
    }

    public String getPing_currency() {
        return ping_currency;
    }

    public void setPing_currency(String ping_currency) {
        this.ping_currency = ping_currency;
    }

    public String getPing_subject() {
        return ping_subject;
    }

    public void setPing_subject(String ping_subject) {
        this.ping_subject = ping_subject;
    }

    public String getPing_body() {
        return ping_body;
    }

    public void setPing_body(String ping_body) {
        this.ping_body = ping_body;
    }

    public Date getPing_created() {
        return ping_created;
    }

    public void setPing_created(Date ping_created) {
        this.ping_created = ping_created;
    }

    public Date getPing_timePaid() {
        return ping_timePaid;
    }

    public void setPing_timePaid(Date ping_timePaid) {
        this.ping_timePaid = ping_timePaid;
    }

    public Date getPing_timeExpire() {
        return ping_timeExpire;
    }

    public void setPing_timeExpire(Date ping_timeExpire) {
        this.ping_timeExpire = ping_timeExpire;
    }

    public Date getPing_timeSettle() {
        return ping_timeSettle;
    }

    public void setPing_timeSettle(Date ping_timeSettle) {
        this.ping_timeSettle = ping_timeSettle;
    }

    public String getPing_transactionNo() {
        return ping_transactionNo;
    }

    public void setPing_transactionNo(String ping_transactionNo) {
        this.ping_transactionNo = ping_transactionNo;
    }
}
