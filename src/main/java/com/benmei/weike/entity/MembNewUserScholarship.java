package com.benmei.weike.entity;

/**
 * Created by Peter on 2017/10/27.
 */
public class MembNewUserScholarship {
    private Integer id;
    private Integer memb_id;

    public MembNewUserScholarship() {
    }
    public MembNewUserScholarship(Integer memb_id) {
        this.memb_id = memb_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMemb_id() {
        return memb_id;
    }

    public void setMemb_id(Integer memb_id) {
        this.memb_id = memb_id;
    }
}
