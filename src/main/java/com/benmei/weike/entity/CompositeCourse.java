package com.benmei.weike.entity;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Micoo on 2017/5/5.
 */
public class CompositeCourse {
    private Long id;//  bigint(20) NOT NULL AUTO_INCREMENT ,
    private Integer ref_id;//  根据type的不同，分别对应其表的id     bigint(20) NULL DEFAULT NULL ,
    private String course_type;//  课程类型,course、live、wieke
    private String title;//  课程标题，直播课名称，微课标题     varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
    private String course_cover_url;//课程封面图片URL
    private Integer today_boutique;//  今日精选：0:不是，1:是  int(11)  NULL DEFAULT NULL ,
    private Integer boutique;//  精选：0:不是，1:是    int(11) NULL DEFAULT NULL ,
    private Date create_date;// 创建日期   timestamp NULL DEFAULT NULL ,
    private Integer category_id;//  分类id    int(11) NULL DEFAULT NULL ,
    private String category_name;// 分类名称
    private Integer tea_id;//  老师id int(11) NULL DEFAULT NULL ,
    private Integer viewer_count;//  收听人数 int(9) NULL DEFAULT NULL COMMENT '收听人数' ,
    private Integer update_lesson_count; //更新节数

    public Integer getUpdate_lesson_count() {
        return update_lesson_count;
    }

    public void setUpdate_lesson_count(Integer update_lesson_count) {
        this.update_lesson_count = update_lesson_count;
    }

    private Double price;// 价格
    private Double weike_score; // 微课综合评分
    private Integer likes_size; // 直播点赞数
    private Integer is_lock = 1; //锁定状态   0：解锁   1：锁定


    public Integer getIs_lock() {
        return is_lock;
    }

    public void setIs_lock(Integer is_lock) {
        this.is_lock = is_lock;
    }

    public Integer getLikes_size() {
        return likes_size;
    }

    public void setLikes_size(Integer likes_size) {
        this.likes_size = likes_size;
    }

    public Double getWeike_score() {
        return weike_score;
    }

    public void setWeike_score(Double weike_score) {
        this.weike_score = weike_score;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    @Transient
    private String teacher_name;
    @Transient
    private String teacher_avatar_url;
    @Transient
    private Integer teacher_valid;////是否验证，0:未验证，1:已验证
    @Transient
    public List<String> teacher_tags = new ArrayList<>();

    public String getCourse_cover_url() {
        return course_cover_url;
    }

    public void setCourse_cover_url(String course_cover_url) {
        this.course_cover_url = course_cover_url;
    }

    public Integer getTeacher_valid() {
        return teacher_valid;
    }

    public void setTeacher_valid(Integer teacher_valid) {
        this.teacher_valid = teacher_valid;
    }

    public List<String> getTeacher_tags() {
        return teacher_tags;
    }

    public void setTeacher_tags(List<String> teacher_tags) {
        this.teacher_tags = teacher_tags;
    }

    public String getTeacher_name() {
        return teacher_name;
    }

    public void setTeacher_name(String teacher_name) {
        this.teacher_name = teacher_name;
    }

    public String getTeacher_avatar_url() {
        return teacher_avatar_url;
    }

    public void setTeacher_avatar_url(String teacher_avatar_url) {
        this.teacher_avatar_url = teacher_avatar_url;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRef_id() {
        return ref_id;
    }

    public void setRef_id(Integer ref_id) {
        this.ref_id = ref_id;
    }

    public String getCourse_type() {
        return course_type;
    }

    public void setCourse_type(String course_type) {
        this.course_type = course_type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getToday_boutique() {
        return today_boutique;
    }

    public void setToday_boutique(Integer today_boutique) {
        this.today_boutique = today_boutique;
    }

    public Integer getBoutique() {
        return boutique;
    }

    public void setBoutique(Integer boutique) {
        this.boutique = boutique;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Integer getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Integer category_id) {
        this.category_id = category_id;
    }

    public Integer getTea_id() {
        return tea_id;
    }

    public void setTea_id(Integer tea_id) {
        this.tea_id = tea_id;
    }

    public Integer getViewer_count() {
        return viewer_count;
    }

    public void setViewer_count(Integer viewer_count) {
        this.viewer_count = viewer_count;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CompositeCourse that = (CompositeCourse) o;

        return course_type != null ? course_type.equals(that.course_type) : that.course_type == null;
    }

    @Override
    public int hashCode() {
        return course_type != null ? course_type.hashCode() : 0;
    }
}
