package com.benmei.weike.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by Peter on 2017/9/6.
 */
public class Appointment {
    private Integer reserve_id;//预约id
    private Integer tea_id;//教师id
    private Integer memb_id;//用户id
    private Date reserve_start_time;//预约时间
    private Date reserve_end_time;//预约结束时间
    private Integer reserve_time;//预约时间单位分钟
    private Integer cou_id;//课程id
    private Integer memb_set_id;//用户购买套餐id
    private Integer state = 1;//状态 1待上课 2已完成 3已取消
    private Double amout;//价格
    private Integer type = 1;//1试听预约 2购买课程预约
    private Date room_open_time;// 房间开启时间
    private Date room_close_time;//房间关闭时间

    // 非数据库字段
    private String clientTimeZoneId;

    public String getClientTimeZoneId() {
        return clientTimeZoneId;
    }

    public void setClientTimeZoneId(String clientTimeZoneId) {
        this.clientTimeZoneId = clientTimeZoneId;
    }

    public Date getRoom_open_time() {
        return room_open_time;
    }

    public void setRoom_open_time(Date room_open_time) {
        this.room_open_time = room_open_time;
    }

    public Date getRoom_close_time() {
        return room_close_time;
    }

    public void setRoom_close_time(Date room_close_time) {
        this.room_close_time = room_close_time;
    }

    public Integer getReserve_id() {
        return reserve_id;
    }

    public void setReserve_id(Integer reserve_id) {
        this.reserve_id = reserve_id;
    }

    public Integer getTea_id() {
        return tea_id;
    }

    public void setTea_id(Integer tea_id) {
        this.tea_id = tea_id;
    }

    public Integer getMemb_id() {
        return memb_id;
    }

    public void setMemb_id(Integer memb_id) {
        this.memb_id = memb_id;
    }

    public Date getReserve_start_time() {
        return reserve_start_time;
    }

    public void setReserve_start_time(Date reserve_start_time) {
        this.reserve_start_time = reserve_start_time;
    }

    public Date getReserve_end_time() {
        return reserve_end_time;
    }

    public void setReserve_end_time(Date reserve_end_time) {
        this.reserve_end_time = reserve_end_time;
    }

    public Integer getReserve_time() {
        return reserve_time;
    }

    public void setReserve_time(Integer reserve_time) {
        this.reserve_time = reserve_time;
    }

    public Integer getCou_id() {
        return cou_id;
    }

    public void setCou_id(Integer cou_id) {
        this.cou_id = cou_id;
    }

    public Integer getMemb_set_id() {
        return memb_set_id;
    }

    public void setMemb_set_id(Integer memb_set_id) {
        this.memb_set_id = memb_set_id;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Double getAmout() {
        return amout;
    }

    public void setAmout(Double amout) {
        this.amout = amout;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
