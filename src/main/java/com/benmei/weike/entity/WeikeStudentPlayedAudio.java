package com.benmei.weike.entity;

import java.util.Date;

/**
 * 播放的录音
 * Created by Peter on 2017/4/13.
 */
public class WeikeStudentPlayedAudio {
    private Long id;//          bigint not null auto_increment,
    private Integer memb_id;//         int,
    private Long weike_id;//   bigint,
    private Long weike_content_id;//   bigint,
    private Integer play_number;//       int not null default 0 comment '录音播放次数',
    private Date first_play_date;//   datetime not null,
    private Date last_play_date;//    datetime not null,

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMemb_id() {
        return memb_id;
    }

    public void setMemb_id(Integer memb_id) {
        this.memb_id = memb_id;
    }

    public Long getWeike_id() {
        return weike_id;
    }

    public void setWeike_id(Long weike_id) {
        this.weike_id = weike_id;
    }

    public Long getWeike_content_id() {
        return weike_content_id;
    }

    public void setWeike_content_id(Long weike_content_id) {
        this.weike_content_id = weike_content_id;
    }

    public Integer getPlay_number() {
        return play_number;
    }

    public void setPlay_number(Integer play_number) {
        this.play_number = play_number;
    }

    public Date getFirst_play_date() {
        return first_play_date;
    }

    public void setFirst_play_date(Date first_play_date) {
        this.first_play_date = first_play_date;
    }

    public Date getLast_play_date() {
        return last_play_date;
    }

    public void setLast_play_date(Date last_play_date) {
        this.last_play_date = last_play_date;
    }
}
