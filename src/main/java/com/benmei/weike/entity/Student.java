package com.benmei.weike.entity;

import java.math.BigDecimal;

/**
 * Created by Peter on 2017/9/8.
 */
public class Student {
    private Integer memb_id;
    private String memb_name;
    private String token;
    private String memb_head_portrait;
    private String equ_type;
    private String equ_client_id;
    private Integer total_study_times;
    private BigDecimal memb_balance;//余额

    public BigDecimal getMemb_balance() {
        return memb_balance;
    }

    public void setMemb_balance(BigDecimal memb_balance) {
        this.memb_balance = memb_balance;
    }

    public Integer getMemb_id() {
        return memb_id;
    }

    public void setMemb_id(Integer memb_id) {
        this.memb_id = memb_id;
    }

    public String getMemb_name() {
        return memb_name;
    }

    public void setMemb_name(String memb_name) {
        this.memb_name = memb_name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMemb_head_portrait() {
        return memb_head_portrait;
    }

    public void setMemb_head_portrait(String memb_head_portrait) {
        this.memb_head_portrait = memb_head_portrait;
    }

    public String getEqu_type() {
        return equ_type;
    }

    public void setEqu_type(String equ_type) {
        this.equ_type = equ_type;
    }

    public String getEqu_client_id() {
        return equ_client_id;
    }

    public void setEqu_client_id(String equ_client_id) {
        this.equ_client_id = equ_client_id;
    }

    public Integer getTotal_study_times() {
        return total_study_times;
    }

    public void setTotal_study_times(Integer total_study_times) {
        this.total_study_times = total_study_times;
    }
}
