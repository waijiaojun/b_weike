package com.benmei.weike.entity;

/**
 * Created by Peter on 2017/9/22.
 */
public class MemberSalesPromotion {
    private Integer id;
    private String desc;//促销活动描述，促销说明

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
