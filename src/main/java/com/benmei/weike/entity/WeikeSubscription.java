package com.benmei.weike.entity;

import java.util.Date;

/**
 * Created by Peter on 2017/7/25.
 */
public class WeikeSubscription {
    private Long id;
    private Integer memb_id;
    private Date subscription_start_time;
    private Date subscription_end_time;
    private Integer pay_status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMemb_id() {
        return memb_id;
    }

    public void setMemb_id(Integer memb_id) {
        this.memb_id = memb_id;
    }

    public Date getSubscription_start_time() {
        return subscription_start_time;
    }

    public void setSubscription_start_time(Date subscription_start_time) {
        this.subscription_start_time = subscription_start_time;
    }

    public Date getSubscription_end_time() {
        return subscription_end_time;
    }

    public void setSubscription_end_time(Date subscription_end_time) {
        this.subscription_end_time = subscription_end_time;
    }

    public Integer getPay_status() {
        return pay_status;
    }

    public void setPay_status(Integer pay_status) {
        this.pay_status = pay_status;
    }
}
