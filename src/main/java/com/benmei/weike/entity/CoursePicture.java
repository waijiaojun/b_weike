package com.benmei.weike.entity;

/**
 * Created by Peter on 2017/9/8.
 * td_nt_course_picture
 */
public class CoursePicture {
   private Integer  pic_id;//	int
   private String  img_name;//	varchar
   private String  img_url;//	varchar
   private Integer  img_sort;//	int
   private Integer  is_valid;//	int
   private Integer  cou_id;//	int
   private String  img_type;//教材类别 1轮播图 2教材

    public Integer getPic_id() {
        return pic_id;
    }

    public void setPic_id(Integer pic_id) {
        this.pic_id = pic_id;
    }

    public String getImg_name() {
        return img_name;
    }

    public void setImg_name(String img_name) {
        this.img_name = img_name;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public Integer getImg_sort() {
        return img_sort;
    }

    public void setImg_sort(Integer img_sort) {
        this.img_sort = img_sort;
    }

    public Integer getIs_valid() {
        return is_valid;
    }

    public void setIs_valid(Integer is_valid) {
        this.is_valid = is_valid;
    }

    public Integer getCou_id() {
        return cou_id;
    }

    public void setCou_id(Integer cou_id) {
        this.cou_id = cou_id;
    }

    public String getImg_type() {
        return img_type;
    }

    public void setImg_type(String img_type) {
        this.img_type = img_type;
    }
}
