package com.benmei.weike.entity;

/**
 * Created by Peter on 2017/9/22.
 */
public class TdNtMembCourseSize {
    private Integer memb_cou_size_id;
    private Integer memb_id;
    private Integer cou_id;
    private Integer cou_size;//课程剩余次数

    /**
     * 防止添加了有参数的构造函数后，spring映射无参数的会报错
     */
    public TdNtMembCourseSize() {

    }

    public TdNtMembCourseSize(Integer memb_id, Integer cou_id, Integer cou_size) {
        this.memb_id = memb_id;
        this.cou_id = cou_id;
        this.cou_size = cou_size;
    }

    public Integer getMemb_cou_size_id() {
        return memb_cou_size_id;
    }

    public void setMemb_cou_size_id(Integer memb_cou_size_id) {
        this.memb_cou_size_id = memb_cou_size_id;
    }

    public Integer getMemb_id() {
        return memb_id;
    }

    public void setMemb_id(Integer memb_id) {
        this.memb_id = memb_id;
    }

    public Integer getCou_id() {
        return cou_id;
    }

    public void setCou_id(Integer cou_id) {
        this.cou_id = cou_id;
    }

    public Integer getCou_size() {
        return cou_size;
    }

    public void setCou_size(Integer cou_size) {
        this.cou_size = cou_size;
    }
}
