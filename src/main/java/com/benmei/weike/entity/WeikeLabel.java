package com.benmei.weike.entity;

import java.util.Date;

/**
 * Created by Peter on 2017/4/13.
 */
public class WeikeLabel {
    private Long id;//               bigint not null auto_increment,
    private Long weike_id;//        bigint,
    private String label_zh_name;//     varchar(128) comment '标签中文名称',
    private String label_en_name;//       varchar(128) comment '标签英文名称',
    private Integer label_type;//       int comment '1 系统标签；2 用户自定义标签；3 管理员自定义标签',
    private Integer create_user;//        int not null,
    private Date create_date;//    datetime not null,
    private Integer update_user;//       int not null,
    private Date update_date;//     datetime not null,
    private Integer deleted;//        int not null default 0 comment '数据删除标记：0使用；1删除',

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getWeike_id() {
        return weike_id;
    }

    public void setWeike_id(Long weike_id) {
        this.weike_id = weike_id;
    }

    public String getLabel_zh_name() {
        return label_zh_name;
    }

    public void setLabel_zh_name(String label_zh_name) {
        this.label_zh_name = label_zh_name;
    }

    public String getLabel_en_name() {
        return label_en_name;
    }

    public void setLabel_en_name(String label_en_name) {
        this.label_en_name = label_en_name;
    }

    public Integer getLabel_type() {
        return label_type;
    }

    public void setLabel_type(Integer label_type) {
        this.label_type = label_type;
    }

    public Integer getCreate_user() {
        return create_user;
    }

    public void setCreate_user(Integer create_user) {
        this.create_user = create_user;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Integer getUpdate_user() {
        return update_user;
    }

    public void setUpdate_user(Integer update_user) {
        this.update_user = update_user;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
}
