package com.benmei.weike.entity;

import com.benmei.weike.common.PayChannel;
import com.benmei.weike.common.PayType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Peter on 2017/9/19.
 */
public class Bill {
    private Integer id;
    private Integer membId;

    // app界面显示字段
    private Integer actionType;       // GoodsCategory.inOut 交易类型：-1 付款，+1 充值 (对应账单中的‘正’、‘负’符号)
    private String actionName;        // GoodsCategory.name 交易描述（Ping++ Charge中的subject，最长为 32 个 Unicode 字符）(“购买课程”、“余额充值”等)
    private String actionIconUrl;     // GoodsCategory.icon 图标Url （账单中显示图标的url）
    private BigDecimal amount;         // 交易总金额 （= 支付金额 + 余额变动金额）

    // 商品相关字段
    private String goodsCategoryCode;          // 商品种类 course课程,sys_course系统课,sys_course_vip_year系统课VIP,recharge充值,appointment_cancel取消预约扣费,other其他
    private Integer goodsId;           // 商品id
    private String goods_desc;         // 商品描述，一般使用商品的名称 （Ping++ Charge中的body，最长为 128 个 Unicode 字符）

    //支付和订单相关字段
    private String payType;           // 支付类型：wallet余额支付；multiple余额 + P++ 组合支付；thirdpart 第三方支付，P++支付
    private String channel;            // 支付渠道 wallet余额，wx微信，alipay支付宝
    private BigDecimal payMoney;       // 支付金额
    private String orderNo;            // 订单号（交易单号）
    private Date createOrderTime;     // 订单创建时间（交易时间）
    private Date notifyTime;           // 订单状态通知时间（Ping++ notify时间）
    private Integer paymentStatus;     // 订单支付状态：1待付款，2用户取消支付，3支付失败，4支付成功，5已退款
    private String paymentDesc;        // 付款说明：“余额付款：0 元，支付宝付款：19 元”

    //余额相关字段
    private BigDecimal balanceChange; // 余额变动金额
    private BigDecimal balanceBefore; // 变动前的余额
    private BigDecimal balanceAfter;  // 变动后的余额

    private String thirdPart;

    @JsonIgnore
    private String clientIp;

    public String getThirdPart() {
        return thirdPart;
    }

    public void setThirdPart(String thirdPart) {
        this.thirdPart = thirdPart;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public String getPaymentDesc() {
        if(StringUtils.isNotEmpty(paymentDesc)){
            return paymentDesc;
        }

        if (PayType.WALLET.equalsIgnoreCase(payType)) {
            paymentDesc = "余额支付，" + balanceChange + " 元";
        } else if (PayType.MULTIPLE.equalsIgnoreCase(payType)) {
            paymentDesc = "组合支付，余额付款：" + balanceChange + " 元，" + this.channel + "付款：" + payMoney + " 元";
        } else if (PayType.THIRDPART.equalsIgnoreCase(payType)) {
            paymentDesc = "第三方支付，" +this.channel + "付款：" + payMoney + " 元";
        } else if (PayType.SYSTEM.equalsIgnoreCase(payType)) {
            String InOutFlag = "+";
            if (actionType < 0) {
                InOutFlag = "-";
            }
            paymentDesc = "外教君平台支付，余额变动: " + InOutFlag + "" + balanceChange + " 元";
        }else{
            paymentDesc="无效的支付类型,PayType:" + payType;
        }

        return paymentDesc;
    }


    public Integer getMembId() {
        return membId;
    }

    public void setMembId(Integer membId) {
        this.membId = membId;
    }

    public void setPaymentDesc(String paymentDesc) {
        this.paymentDesc = paymentDesc;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getActionType() {
        return actionType;
    }

    public void setActionType(Integer actionType) {
        this.actionType = actionType;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getActionIconUrl() {
        return actionIconUrl;
    }

    public void setActionIconUrl(String actionIconUrl) {
        this.actionIconUrl = actionIconUrl;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * 交易总金额 （= 支付金额 + 余额变动金额）
     *
     * @param amount
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getGoodsCategoryCode() {
        return goodsCategoryCode;
    }

    public void setGoodsCategoryCode(String goodsCategoryCode) {
        this.goodsCategoryCode = goodsCategoryCode;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoods_desc() {
        return goods_desc;
    }

    public void setGoods_desc(String goods_desc) {
        this.goods_desc = goods_desc;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public BigDecimal getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(BigDecimal payMoney) {
        this.payMoney = payMoney;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Date getCreateOrderTime() {
        return createOrderTime;
    }

    public void setCreateOrderTime(Date createOrderTime) {
        this.createOrderTime = createOrderTime;
    }

    public Date getNotifyTime() {
        return notifyTime;
    }

    public void setNotifyTime(Date notifyTime) {
        this.notifyTime = notifyTime;
    }

    public Integer getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(Integer paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public BigDecimal getBalanceChange() {
        return balanceChange;
    }

    public void setBalanceChange(BigDecimal balanceChange) {
        this.balanceChange = balanceChange;
    }

    public BigDecimal getBalanceBefore() {
        return balanceBefore;
    }

    public void setBalanceBefore(BigDecimal balanceBefore) {
        this.balanceBefore = balanceBefore;
    }

    public BigDecimal getBalanceAfter() {
        return balanceAfter;
    }

    public void setBalanceAfter(BigDecimal balanceAfter) {
        this.balanceAfter = balanceAfter;
    }
}
