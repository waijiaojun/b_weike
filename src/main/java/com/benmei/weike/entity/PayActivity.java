package com.benmei.weike.entity;

import java.math.BigDecimal;

/**
 * Created by Peter on 2017/9/25.
 */
public class PayActivity implements Comparable<PayActivity> {
    private Integer pact_id;//id
    private BigDecimal recharge_amount;//充值金额
    private BigDecimal give_amount;//赠送金额
    private Integer is_valid;//是否有效 0是1否

    public Integer getPact_id() {
        return pact_id;
    }

    public void setPact_id(Integer pact_id) {
        this.pact_id = pact_id;
    }

    public BigDecimal getRecharge_amount() {
        return recharge_amount;
    }

    public void setRecharge_amount(BigDecimal recharge_amount) {
        this.recharge_amount = recharge_amount;
    }

    public BigDecimal getGive_amount() {
        return give_amount;
    }

    public void setGive_amount(BigDecimal give_amount) {
        this.give_amount = give_amount;
    }

    public Integer getIs_valid() {
        return is_valid;
    }

    public void setIs_valid(Integer is_valid) {
        this.is_valid = is_valid;
    }

    @Override
    public int compareTo(PayActivity o) {
        return this.getRecharge_amount().compareTo(o.getRecharge_amount());
    }
}
