package com.benmei.weike.entity;

import freemarker.template.utility.StringUtil;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by waijiaojun on 2017/9/23.
 */
public class WeikeTodayBoutique {

    private Long id;//  bigint(20) NOT NULL AUTO_INCREMENT ,
    private String title;//  课程标题，直播课名称，微课标题     varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
    private String title_student; //学生端展示的课程标题
    private Integer today_boutique;//  今日精选：0:不是，1:是  int(11)  NULL DEFAULT NULL ,
    private Integer boutique;//  精选：0:不是，1:是    int(11) NULL DEFAULT NULL ,
    private Date create_date;// 创建日期   timestamp NULL DEFAULT NULL ,
    private Integer tea_id;//  老师id int(11) NULL DEFAULT NULL ,
    private Integer viewer_count;//  收听人数 int(9) NULL DEFAULT NULL COMMENT '收听人数' ,
    private Integer update_lesson_count; //更新节数
    private Double price;// 价格
    private String 	cover_url;//封面图片
    private String cover_url_student;//学生端展示封面图片
    private Integer is_lock = 1; // 锁定状态  0：解锁   1：锁定

    public Integer getIs_lock() {
        return is_lock;
    }

    public void setIs_lock(Integer is_lock) {
        this.is_lock = is_lock;
    }

    @Transient
    private String teacher_name;
    @Transient
    private String teacher_avatar_url;
    @Transient
    private String teacher_valid;// 0无 1new 2V
    @Transient
    public List<String> teacher_tags = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCover_url_student() {
        if(StringUtils.isNotBlank(cover_url)){
            return cover_url;
        }else{
        return cover_url_student;
        }
    }

    public void setCover_url_student(String cover_url_student) {
        this.cover_url_student = cover_url_student;
    }

    public String getTitle() {
        if(StringUtils.isNotBlank(this.title_student)){
            this.title = title_student;
        }
        return title;
    }

    public void setTitle_student(String title_student) {
        this.title_student = title_student;
    }

    public String getTitle_student() {
        return title_student;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getToday_boutique() {
        return today_boutique;
    }

    public void setToday_boutique(Integer today_boutique) {
        this.today_boutique = today_boutique;
    }

    public Integer getBoutique() {
        return boutique;
    }

    public void setBoutique(Integer boutique) {
        this.boutique = boutique;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Integer getTea_id() {
        return tea_id;
    }

    public void setTea_id(Integer tea_id) {
        this.tea_id = tea_id;
    }

    public Integer getViewer_count() {
        return viewer_count;
    }

    public void setViewer_count(Integer viewer_count) {
        this.viewer_count = viewer_count;
    }

    public Integer getUpdate_lesson_count() {
        return update_lesson_count;
    }

    public void setUpdate_lesson_count(Integer update_lesson_count) {
        this.update_lesson_count = update_lesson_count;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getTeacher_name() {
        return teacher_name;
    }

    public void setTeacher_name(String teacher_name) {
        this.teacher_name = teacher_name;
    }

    public String getTeacher_avatar_url() {
        return teacher_avatar_url;
    }

    public void setTeacher_avatar_url(String teacher_avatar_url) {
        this.teacher_avatar_url = teacher_avatar_url;
    }

    public String getTeacher_valid() {
        return teacher_valid;
    }

    public void setTeacher_valid(String teacher_valid) {
        this.teacher_valid = teacher_valid;
    }

    public List<String> getTeacher_tags() {
        return teacher_tags;
    }

    public void setTeacher_tags(List<String> teacher_tags) {
        this.teacher_tags = teacher_tags;
    }

    public String getCover_url() {
        if(StringUtils.isNotBlank(cover_url_student)){
            return cover_url_student;
        }else{
            return cover_url;
        }
    }

    public void setCover_url(String cover_url) {
        this.cover_url = cover_url;
    }
}
