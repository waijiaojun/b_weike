package com.benmei.weike.entity;

import java.math.BigDecimal;

/**
 * Created by waijiaojun on 2017/9/7.
 */
public class CompositeWeike {
    private Long weikeId;
    private String cover_url_student; //varchar(512) comment '封面图片', 学生端显示用的
    private Double score;//  float comment '综合评分',
    private String title;//                varchar(128) comment '标题',
    private BigDecimal student_price;//      float default 0 comment '学生购买价格',
    private Integer buy_number;//int comment '购买人数',//界面上和人数相关的都用此字段：学习人数，收听人数，购买人数等

    private Integer tea_id;
    private Integer update_lesson_count; //更新节数
    private String teacher_name;
    private Integer teacher_valid;////是否验证，0:未验证，1:已验证
    private String tea_head_portrait;//老师头像地址

    private String flag_name; //课程标签
    private Integer hot;// 是否是热门微课：0不是，1是

    private Integer is_lock = 1; //0：未上锁   1：已上锁

    public Integer getIs_lock() {
        return is_lock;
    }

    public void setIs_lock(Integer is_lock) {
        this.is_lock = is_lock;
    }

    public Integer getHot() {
        return hot;
    }

    public void setHot(Integer hot) {
        this.hot = hot;
    }

    public Long getWeikeId() {
        return weikeId;
    }

    public void setWeikeId(Long weikeId) {
        this.weikeId = weikeId;
    }

    public String getFlag_name() {
        return flag_name;
    }

    public void setFlag_name(String flag_name) {
        this.flag_name = flag_name;
    }

    public Integer getTea_id() {
        return tea_id;
    }

    public void setTea_id(Integer tea_id) {
        this.tea_id = tea_id;
    }

    public String getTea_head_portrait() {
        return tea_head_portrait;
    }

    public void setTea_head_portrait(String tea_head_portrait) {
        this.tea_head_portrait = tea_head_portrait;
    }

    public Integer getUpdate_lesson_count() {
        return update_lesson_count;
    }

    public void setUpdate_lesson_count(Integer update_lesson_count) {
        this.update_lesson_count = update_lesson_count;
    }

    public String getTeacher_name() {
        return teacher_name;
    }

    public void setTeacher_name(String teacher_name) {
        this.teacher_name = teacher_name;
    }

    public Integer getTeacher_valid() {
        return teacher_valid;
    }

    public void setTeacher_valid(Integer teacher_valid) {
        this.teacher_valid = teacher_valid;
    }


    public String getCover_url_student() {
        return cover_url_student;
    }

    public void setCover_url_student(String cover_url_student) {
        this.cover_url_student = cover_url_student;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BigDecimal getStudent_price() {
        return student_price;
    }

    public void setStudent_price(BigDecimal student_price) {
        this.student_price = student_price;
    }

    public Integer getBuy_number() {
        return buy_number;
    }

    public void setBuy_number(Integer buy_number) {
        this.buy_number = buy_number;
    }
}
