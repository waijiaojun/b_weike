package com.benmei.weike.entity;

/**
 * Created by Peter on 2017/9/6.
 */
public class CourseInfo {
    private Integer cou_id;
    private String cou_name;
    private String cou_synopsis;
    private Integer study_size;
    private String cou_grade;
    private Integer study_time;
    private String cou_english_name;
    private Integer cate_id;
    private String stand;//课程封面

    public Integer getCou_id() {
        return cou_id;
    }

    public void setCou_id(Integer cou_id) {
        this.cou_id = cou_id;
    }

    public String getCou_name() {
        return cou_name;
    }

    public void setCou_name(String cou_name) {
        this.cou_name = cou_name;
    }

    public String getCou_synopsis() {
        return cou_synopsis;
    }

    public void setCou_synopsis(String cou_synopsis) {
        this.cou_synopsis = cou_synopsis;
    }

    public Integer getStudy_size() {
        return study_size;
    }

    public void setStudy_size(Integer study_size) {
        this.study_size = study_size;
    }

    public String getCou_grade() {
        return cou_grade;
    }

    public void setCou_grade(String cou_grade) {
        this.cou_grade = cou_grade;
    }

    public Integer getStudy_time() {
        return study_time;
    }

    public void setStudy_time(Integer study_time) {
        this.study_time = study_time;
    }

    public String getCou_english_name() {
        return cou_english_name;
    }

    public void setCou_english_name(String cou_english_name) {
        this.cou_english_name = cou_english_name;
    }

    public Integer getCate_id() {
        return cate_id;
    }

    public void setCate_id(Integer cate_id) {
        this.cate_id = cate_id;
    }

    public String getStand() {
        return stand;
    }

    public void setStand(String stand) {
        this.stand = stand;
    }
}
