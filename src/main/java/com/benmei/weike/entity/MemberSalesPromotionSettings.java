package com.benmei.weike.entity;

/**
 * Created by Peter on 2017/9/22.
 */
public class MemberSalesPromotionSettings {

    private Integer id;
    private Integer memb_id;
    private Integer salesPromotionId;
    private Integer closed;// 是否关闭这个促销活动的显示；1 关闭，0 显示；默认1


    public Integer getMemb_id() {
        return memb_id;
    }

    public void setMemb_id(Integer memb_id) {
        this.memb_id = memb_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSalesPromotionId() {
        return salesPromotionId;
    }

    public void setSalesPromotionId(Integer salesPromotionId) {
        this.salesPromotionId = salesPromotionId;
    }

    public Integer getClosed() {
        return closed;
    }

    public void setClosed(Integer closed) {
        this.closed = closed;
    }
}
