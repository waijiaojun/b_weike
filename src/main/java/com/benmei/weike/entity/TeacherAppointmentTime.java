package com.benmei.weike.entity;

import java.util.Date;

/**
 * Created by Peter on 2017/9/6.
 */
public class TeacherAppointmentTime {
    private Integer reserve_time_id;//设置id
    private Integer tea_id;//教师id
    private Integer reserve_week;//可预约周 1：周1
    private Integer reserve_time;////0代表0点到0:30 1代表0:30到1:00
    private Date reserve_date;//日期(yyyy-MM-dd)
    private Integer is_valid;//是否可用 0是 1否 2被预约
    private Date date;

    public Integer getReserve_time_id() {
        return reserve_time_id;
    }

    public void setReserve_time_id(Integer reserve_time_id) {
        this.reserve_time_id = reserve_time_id;
    }

    public Integer getTea_id() {
        return tea_id;
    }

    public void setTea_id(Integer tea_id) {
        this.tea_id = tea_id;
    }

    public Integer getReserve_week() {
        return reserve_week;
    }

    public void setReserve_week(Integer reserve_week) {
        this.reserve_week = reserve_week;
    }

    public Integer getReserve_time() {
        return reserve_time;
    }

    public void setReserve_time(Integer reserve_time) {
        this.reserve_time = reserve_time;
    }

    public Date getReserve_date() {
        return reserve_date;
    }

    public void setReserve_date(Date reserve_date) {
        this.reserve_date = reserve_date;
    }

    public Integer getIs_valid() {
        return is_valid;
    }

    public void setIs_valid(Integer is_valid) {
        this.is_valid = is_valid;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
