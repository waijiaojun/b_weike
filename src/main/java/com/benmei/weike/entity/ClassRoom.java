package com.benmei.weike.entity;

import com.benmei.weike.common.Constants;
import com.benmei.weike.util.DateUtil;

import java.util.Date;

/**
 * Created by Peter on 2017/9/6.
 */
public class ClassRoom {
    private Integer room_id;//              int(11) not null auto_increment comment '自增长',
    private Integer reserve_id;//          int(11) default NULL,

    private Integer tea_id;//    int(11) default NULL,
    private Integer tea_is_in_room;//老师是否在教室里：1在，0不在
    private Integer tea_into_number;//老师进入教室的次数

    private Integer memb_id;// int(11) default 0,
    private Integer memb_is_in_room;//学生是否在教室里：1在，0不在
    private Integer memb_into_number;//学生进入教室的次数

    private Integer cou_id;//int(11) default NULL,
    private Integer memb_set_id;//int(11),
    private Integer room_state;//       int(11) default 0 comment '1关闭，2开放，3结束',
    private Date room_create_date;//    datetime default NULL,
    private Date room_open_date;//   datetime default NULL,
    private Date room_close_date;//   datetime default NULL,

    public Integer getTea_is_in_room() {
        return tea_is_in_room;
    }

    public void setTea_is_in_room(Integer tea_is_in_room) {
        this.tea_is_in_room = tea_is_in_room;
    }

    public Integer getTea_into_number() {
        return tea_into_number;
    }

    public void setTea_into_number(Integer tea_into_number) {
        this.tea_into_number = tea_into_number;
    }

    public Integer getMemb_is_in_room() {
        return memb_is_in_room;
    }

    public void setMemb_is_in_room(Integer memb_is_in_room) {
        this.memb_is_in_room = memb_is_in_room;
    }

    public Integer getMemb_into_number() {
        return memb_into_number;
    }

    public void setMemb_into_number(Integer memb_into_number) {
        this.memb_into_number = memb_into_number;
    }

    public Integer getRoom_id() {
        return room_id;
    }

    public void setRoom_id(Integer room_id) {
        this.room_id = room_id;
    }

    public Integer getReserve_id() {
        return reserve_id;
    }

    public void setReserve_id(Integer reserve_id) {
        this.reserve_id = reserve_id;
    }

    public Integer getTea_id() {
        return tea_id;
    }

    public void setTea_id(Integer tea_id) {
        this.tea_id = tea_id;
    }

    public Integer getMemb_id() {
        return memb_id;
    }

    public void setMemb_id(Integer memb_id) {
        this.memb_id = memb_id;
    }

    public Integer getCou_id() {
        return cou_id;
    }

    public void setCou_id(Integer cou_id) {
        this.cou_id = cou_id;
    }

    public Integer getMemb_set_id() {
        return memb_set_id;
    }

    public void setMemb_set_id(Integer memb_set_id) {
        this.memb_set_id = memb_set_id;
    }

    public Integer getRoom_state() {
        Date now = new Date();

        long cosur = now.getTime();
        long start = this.getRoom_open_date().getTime();
        long end = this.getRoom_close_date().getTime();
        if (cosur < start) {
            room_state = Constants.ClassRoom.RoomStatus.UN_START;
        } else if (cosur > end) {
            room_state = Constants.ClassRoom.RoomStatus.END;
        } else {
            room_state = Constants.ClassRoom.RoomStatus.OPEN;
        }
        return room_state;
    }

    public void setRoom_state(Integer room_state) {
        this.room_state = room_state;
    }

    public Date getRoom_create_date() {
        return room_create_date;
    }

    public void setRoom_create_date(Date room_create_date) {
        this.room_create_date = room_create_date;
    }

    public Date getRoom_open_date() {
        return room_open_date;
    }

    public void setRoom_open_date(Date room_open_date) {
        this.room_open_date = room_open_date;
    }

    public Date getRoom_close_date() {
        return room_close_date;
    }

    public void setRoom_close_date(Date room_close_date) {
        this.room_close_date = room_close_date;
    }
}
