package com.benmei.weike.entity;

/**
 * Created by Peter on 2017/9/23.
 */
public class CourseChapter {
    private int cou_id;
    private int level_sort;//章节数
    private int start_page;//章节开始页数
    private int end_page;//章节结束页数
    private String level_english_name;//章节英文名称

    public int getCou_id() {
        return cou_id;
    }

    public void setCou_id(int cou_id) {
        this.cou_id = cou_id;
    }

    public int getLevel_sort() {
        return level_sort;
    }

    public void setLevel_sort(int level_sort) {
        this.level_sort = level_sort;
    }

    public int getStart_page() {
        return start_page;
    }

    public void setStart_page(int start_page) {
        this.start_page = start_page;
    }

    public int getEnd_page() {
        return end_page;
    }

    public void setEnd_page(int end_page) {
        this.end_page = end_page;
    }

    public String getLevel_english_name() {
        return level_english_name;
    }

    public void setLevel_english_name(String level_english_name) {
        this.level_english_name = level_english_name;
    }
}
