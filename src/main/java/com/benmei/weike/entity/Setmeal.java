package com.benmei.weike.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Created by Peter on 2017/9/6.
 */
public class Setmeal {
    private Integer memb_set_id;
    private Integer set_id;//套餐id
    private Integer cou_id;//课程id
    private String set_name;//套餐名称
    private Integer set_type;// 套餐类别 1天 2周 3 月 4 年 (如果是年的就按照365天 月都是31天)
    private Integer set_limit;// 0限制每天时长1不限制
    private Integer set_time;//每天时长  如果不限制为总时间
    private BigDecimal set_amout;//套餐价格
    private String set_synopis;//课程简介
    private Integer memb_id;//用户id
    private Timestamp buy_time;//购买时间
    private Timestamp end_time;//截止时间
    private Integer surplus_time;//剩余时间,有限制的为当天剩余时间
    private Integer state;//状态 1 待付款2付款待结束 3付款已完成    --该字段已经用不到
    private BigDecimal minute_amout;//每分钟价格
    private BigDecimal original_price;//原价
    private Integer mfchg_id;
    private String set_picture;//图片
    private Integer size;//
    private Integer set_reserve_size;//规格套餐剩余预约次数
    private Integer cou_size;
    private Integer cou_time;// 1节课的时长
    private Integer additional;//是否是赠送的套餐：1是；0否

    public Integer getCou_time() {
        return cou_time;
    }

    public void setCou_time(Integer cou_time) {
        this.cou_time = cou_time;
    }

    public Integer getAdditional() {
        return additional;
    }

    public void setAdditional(Integer additional) {
        this.additional = additional;
    }

    public Integer getCou_size() {
        return cou_size;
    }

    public void setCou_size(Integer cou_size) {
        this.cou_size = cou_size;
    }

    public Integer getMemb_set_id() {
        return memb_set_id;
    }

    public void setMemb_set_id(Integer memb_set_id) {
        this.memb_set_id = memb_set_id;
    }

    public Integer getSet_id() {
        return set_id;
    }

    public void setSet_id(Integer set_id) {
        this.set_id = set_id;
    }

    public Integer getCou_id() {
        return cou_id;
    }

    public void setCou_id(Integer cou_id) {
        this.cou_id = cou_id;
    }

    public String getSet_name() {
        return set_name;
    }

    public void setSet_name(String set_name) {
        this.set_name = set_name;
    }

    public Integer getSet_type() {
        return set_type;
    }

    public void setSet_type(Integer set_type) {
        this.set_type = set_type;
    }

    public Integer getSet_limit() {
        return set_limit;
    }

    public void setSet_limit(Integer set_limit) {
        this.set_limit = set_limit;
    }

    public Integer getSet_time() {
        return set_time;
    }

    public void setSet_time(Integer set_time) {
        this.set_time = set_time;
    }

    public BigDecimal getSet_amout() {
        return set_amout;
    }

    public void setSet_amout(BigDecimal set_amout) {
        this.set_amout = set_amout;
    }

    public String getSet_synopis() {
        return set_synopis;
    }

    public void setSet_synopis(String set_synopis) {
        this.set_synopis = set_synopis;
    }

    public Integer getMemb_id() {
        return memb_id;
    }

    public void setMemb_id(Integer memb_id) {
        this.memb_id = memb_id;
    }

    public Timestamp getBuy_time() {
        return buy_time;
    }

    public void setBuy_time(Timestamp buy_time) {
        this.buy_time = buy_time;
    }

    public Timestamp getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Timestamp end_time) {
        this.end_time = end_time;
    }

    public Integer getSurplus_time() {
        return surplus_time;
    }

    public void setSurplus_time(Integer surplus_time) {
        this.surplus_time = surplus_time;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public BigDecimal getMinute_amout() {
        return minute_amout;
    }

    public void setMinute_amout(BigDecimal minute_amout) {
        this.minute_amout = minute_amout;
    }

    public BigDecimal getOriginal_price() {
        return original_price;
    }

    public void setOriginal_price(BigDecimal original_price) {
        this.original_price = original_price;
    }

    public Integer getMfchg_id() {
        return mfchg_id;
    }

    public void setMfchg_id(Integer mfchg_id) {
        this.mfchg_id = mfchg_id;
    }

    public String getSet_picture() {
        return set_picture;
    }

    public void setSet_picture(String set_picture) {
        this.set_picture = set_picture;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getSet_reserve_size() {
        return set_reserve_size;
    }

    public void setSet_reserve_size(Integer set_reserve_size) {
        this.set_reserve_size = set_reserve_size;
    }
}
