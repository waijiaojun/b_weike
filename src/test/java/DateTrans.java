import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Peter on 2017/11/9.
 */
public class DateTrans {
    public static void main(String[] args) throws ParseException {
        Date d = new Date(1512543600000L);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd H");
        System.out.println(sdf.format(d));

        System.out.println(getTimestamp("2017-12-02 16"));
    }


    public static Long getTimestamp(String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd H");
        Date d = sdf.parse(date);

        return d.getTime();
    }
}
