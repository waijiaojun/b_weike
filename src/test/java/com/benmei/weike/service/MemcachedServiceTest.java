package com.benmei.weike.service;

import com.benmei.weike.entity.Weike;
import com.benmei.weike.service.common.MemcachedService;
import com.nativetalk.base.course.TwoTsNtCourseInfoDetail;
import com.nativetalk.bean.member.TdNtMember;
import net.sf.json.util.JSONUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

/**
 * Created by Micoo on 2017/4/13.
 */
@SpringBootTest
@ContextConfiguration
@RunWith(SpringRunner.class)
public class MemcachedServiceTest {

    public static final String TEST_WEIKE_KEY = "testWeikeKey";
    @Autowired
    MemcachedService memcachedService;

    @Test
    public void TestAdd() {
        Weike weike = new Weike();
        weike.setId(1L);
        weike.setTitle("test weike title");
        weike.setIntro("微课介绍");
        memcachedService.add("TEST_WEIKE_KEY",weike,60*60*24*30);

    }

    @Test
    public void TestGet(){

        Map<String, TwoTsNtCourseInfoDetail> tsNtCourseInfoDetailMap = (Map<String, TwoTsNtCourseInfoDetail>) memcachedService.get("tsNtCourseInfoDetailMapTwo");
        System.out.println(tsNtCourseInfoDetailMap);

    }


    @Test
    public void TestAddStudent() {
        TdNtMember member = new TdNtMember();
        member.setToken("b1aeb1bb-155f-4556-81d5-5d346854c691");
        member.setMemb_id(2265);
        member.setMemb_name("Peter");
        memcachedService.add("b1aeb1bb-155f-4556-81d5-5d346854c691",member,60*60*24*30);

    }

    @Test
    public void TestGetStudent() {
        TdNtMember member = new TdNtMember();
        Object o = memcachedService.get("b1aeb1bb-155f-4556-81d5-5d346854c691");
        if (o == null) {
            System.out.println("null");
        }else if(!TdNtMember.class.isInstance(o)){
            System.out.println("not member");
        }else{
            System.out.println(JSONUtils.valueToString(member));
        }


    }


}
