package com.benmei.weike.service;

import com.benmei.weike.config.SmallProgramParameter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * Created by waijiaojun on 2017/11/3.
 */
@SpringBootTest
@ContextConfiguration
@RunWith(SpringRunner.class)
public class AppIdTest {
    @Resource
    private SmallProgramParameter smallProgramParameter;
    @Test
    public void getAppID() {
        System.out.println(smallProgramParameter.getAppId());
    }
}
