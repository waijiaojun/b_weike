import com.benmei.weike.common.filter.SignFilter;
import com.benmei.weike.util.MD5Util;

/**
 * Created by Micoo on 2017/4/18.
 */
public class SignUtil {
    public static void main(String[] args) {
        Long timestamp = System.currentTimeMillis()/1000;

        String plaintext = timestamp + SignFilter.KEY; //明文
        String ciphertext = MD5Util.code(plaintext);  //密文

        System.out.println("Timestamp:"+timestamp);
        System.out.println("SignInfo:"+ciphertext);
    }
}
