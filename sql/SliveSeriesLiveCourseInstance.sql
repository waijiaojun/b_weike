-- auto Generated on 2017-08-28 19:11:03 
-- DROP TABLE IF EXISTS `slive_series_live_course_instance`; 
CREATE TABLE `slive_series_live_course_instance`(
    `id` BIGINT (15) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `series_live_id` BIGINT (15) NOT NULL DEFAULT -1 COMMENT 'series_live_id',
    `series_live_course_id` BIGINT (15) NOT NULL DEFAULT -1 COMMENT 'series_live_course_id',
    `series_live_instance_id` BIGINT (15) NOT NULL DEFAULT -1 COMMENT 'series_live_instance_id',
    `title_en` VARCHAR (512) NOT NULL DEFAULT '' COMMENT 'title_en',
    `title_cn` VARCHAR (512) NOT NULL DEFAULT '' COMMENT 'title_cn',
    `duration` INT (11) NOT NULL DEFAULT -1 COMMENT 'duration',
    `tea_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'tea_id',
    `create_date` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'create_date',
    `update_date` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'update_date',
    `deleted` INT (11) NOT NULL DEFAULT -1 COMMENT 'deleted',
    `plan_start_time` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'plan_start_time',
    `plan_end_time` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'plan_end_time',
    `actual_start_time` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'actual_start_time',
    `actual_end_time` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'actual_end_time',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`slive_series_live_course_instance`';
