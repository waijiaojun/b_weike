-- auto Generated on 2017-04-24 16:51:23 
-- DROP TABLE IF EXISTS `wk_weike_content`;
CREATE TABLE `wk_weike_content`(
    `id` BIGINT (15) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `weike_id` BIGINT (15) NOT NULL DEFAULT -1 COMMENT 'weike_id',
    `weike_part_sort` INT (11) NOT NULL DEFAULT -1 COMMENT 'weike_part_sort',
    `type` VARCHAR (20) NOT NULL DEFAULT '' COMMENT 'type',
    `content` VARCHAR (1024) NOT NULL DEFAULT '' COMMENT 'content',
    `duration` BIGINT (15) NOT NULL DEFAULT -1 COMMENT 'duration',
    `sort` INT (11) NOT NULL DEFAULT -1 COMMENT 'sort',
    `create_user` INT (11) NOT NULL DEFAULT -1 COMMENT 'create_user',
    `create_date`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create_date' ,
    `update_user` INT (11) NOT NULL DEFAULT -1 COMMENT 'update_user',
    `update_date` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'update_date',
    `update_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'update_date',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`weike_content`';
