-- auto Generated on 2017-05-05 17:44:07 
-- DROP TABLE IF EXISTS `t_composite_course`; 
CREATE TABLE `t_composite_course`(
    `id` BIGINT (15) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `ref_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'ref_id',
    `course_type` VARCHAR (20) NOT NULL DEFAULT '' COMMENT 'course_type',
    `title` VARCHAR (255) NOT NULL DEFAULT '' COMMENT 'title',
    `today_boutique` INT (11) NOT NULL DEFAULT -1 COMMENT 'today_boutique',
    `boutique` INT (11) NOT NULL DEFAULT -1 COMMENT 'boutique',
    `create_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'create_date',
    `category_id` INT (11)  DEFAULT -1 COMMENT 'category_id',
    `tea_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'tea_id',
    `viewer_count` INT (11) NOT NULL DEFAULT -1 COMMENT 'viewer_count',
    `price` DOUBLE   NULL DEFAULT NULL COMMENT '价格',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`t_composite_course`';
