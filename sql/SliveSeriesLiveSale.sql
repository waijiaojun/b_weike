-- auto Generated on 2017-08-29 09:23:06 
-- DROP TABLE IF EXISTS `slive_series_live_sale`; 
CREATE TABLE `slive_series_live_sale`(
    `id` BIGINT (15) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `series_live_instance_id` BIGINT (15) NOT NULL DEFAULT -1 COMMENT 'series_live_instance_id',
    `title_en` VARCHAR (512) NOT NULL DEFAULT '' COMMENT 'title_en',
    `title_cn` VARCHAR (512) NOT NULL DEFAULT '' COMMENT 'title_cn',
    `intro_en` VARCHAR (512) NOT NULL DEFAULT '' COMMENT 'intro_en',
    `intro_cn` VARCHAR (512) NOT NULL DEFAULT '' COMMENT 'intro_cn',
    `play_status` INT (11) NOT NULL DEFAULT -1 COMMENT 'play_status',
    `cover` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'cover',
    `is_show` INT (11) NOT NULL DEFAULT -1 COMMENT 'is_show',
    `price` DOUBLE (16,4) NOT NULL DEFAULT -1.0 COMMENT 'price',
    `sale_start_time` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'sale_start_time',
    `sale_end_time` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'sale_end_time',
    `plan_sale_number` INT (11) NOT NULL DEFAULT -1 COMMENT 'plan_sale_number',
    `actual_sale_number` INT (11) NOT NULL DEFAULT -1 COMMENT 'actual_sale_number',
    `tea_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'tea_id',
    `create_date` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'create_date',
    `update_date` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'update_date',
    `deleted` INT (11) NOT NULL DEFAULT -1 COMMENT 'deleted',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`slive_series_live_sale`';
