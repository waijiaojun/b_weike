-- auto Generated on 2017-09-20 16:21:54 
-- DROP TABLE IF EXISTS `payment_student_bill`; 
CREATE TABLE `payment_student_bill`(
    `id` INT (11) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `memb_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'membId',
    `action_type` INT (11) NOT NULL DEFAULT -1 COMMENT 'actionType',
    `action_name` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'actionName',
    `action_icon_url` VARCHAR (512) NOT NULL DEFAULT '' COMMENT 'actionIconUrl',
    `amount` DECIMAL (13,4) NOT NULL DEFAULT -1 COMMENT 'amount',
    `goods_category_code` VARCHAR (128) NOT NULL DEFAULT '' COMMENT 'goodsCategoryCode',
    `goods_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'goodsId',
    `goods_desc` VARCHAR (255) NOT NULL DEFAULT '' COMMENT 'goods_desc',
    `pay_type` INT (11) NOT NULL DEFAULT -1 COMMENT 'payType',
    `channel` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'channel',
    `pay_money` DECIMAL (13,4) NOT NULL DEFAULT -1 COMMENT 'payMoney',
    `order_no` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'orderNo',
    `create_order_time` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'createOrderTime',
    `notify_time` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'notifyTime',
    `payment_status` INT (11) NOT NULL DEFAULT -1 COMMENT 'paymentStatus',
    `payment_desc` VARCHAR (128) NOT NULL DEFAULT '' COMMENT 'paymentDesc',
    `balance_change` DECIMAL (13,4) NOT NULL DEFAULT -1 COMMENT 'balanceChange',
    `balance_before` DECIMAL (13,4) NOT NULL DEFAULT -1 COMMENT 'balanceBefore',
    `balance_after` DECIMAL (13,4) NOT NULL DEFAULT -1 COMMENT 'balanceAfter',
    `client_ip` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'clientIp',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`payment_student_bill`';
