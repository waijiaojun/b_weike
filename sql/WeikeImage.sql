-- auto Generated on 2017-04-24 17:49:21 
-- DROP TABLE IF EXISTS `wk_weike_image`; 
CREATE TABLE `wk_weike_image`(
    `id` BIGINT (15) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `weike_id` BIGINT (15) NOT NULL DEFAULT -1 COMMENT 'weike_id',
    `weike_part_sort` INT (11) NOT NULL DEFAULT -1 COMMENT 'weike_part_sort',
    `url` VARCHAR (1024) NOT NULL DEFAULT '' COMMENT 'url',
    `sort` INT (11) NOT NULL DEFAULT -1 COMMENT 'sort',
    `create_user` INT (11) NOT NULL DEFAULT -1 COMMENT 'create_user',
    `create_date`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create_date' ,
    `update_user` INT (11) NOT NULL DEFAULT -1 COMMENT 'update_user',
    `update_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'update_date',
    `deleted` INT (11) NOT NULL DEFAULT -1 COMMENT 'deleted',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`wk_weike_image`';
