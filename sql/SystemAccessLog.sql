-- auto Generated on 2017-05-12 13:39:23 
-- DROP TABLE IF EXISTS `system_access_log`; 
CREATE TABLE `system_access_log`(
    `id` BIGINT (15) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `cip` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'cip',
    `url` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'url',
    `header` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'header',
    `create_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'create_date',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`system_access_log`';
