-- auto Generated on 2017-05-04 15:59:46 
-- DROP TABLE IF EXISTS `wk_weike_comments`; 
CREATE TABLE `wk_weike_comments`(
    `id` BIGINT (15) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `weike_id` BIGINT (15) NOT NULL DEFAULT -1 COMMENT 'weike_id',
    `content` VARCHAR (500) NOT NULL DEFAULT '' COMMENT 'content',
    `score` INT (11) NOT NULL DEFAULT -1 COMMENT 'score',
    `memb_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'memb_id',
    `reply` VARCHAR (500) NOT NULL DEFAULT '' COMMENT 'reply',
    `tea_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'tea_id',
    `create_date`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create_date' ,
    `update_user` INT (11) NOT NULL DEFAULT -1 COMMENT 'update_user',
    `update_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'update_date',
    `deleted` INT (11) NOT NULL DEFAULT 0 COMMENT 'deleted',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`wk_weike_comments`';
