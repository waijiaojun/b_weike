-- auto Generated on 2017-09-22 15:38:04 
-- DROP TABLE IF EXISTS `member_sales_promotion`; 
CREATE TABLE `member_sales_promotion`(
    `id` INT (11) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `desc` VARCHAR (512) NOT NULL DEFAULT '' COMMENT 'desc',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`member_sales_promotion`';
