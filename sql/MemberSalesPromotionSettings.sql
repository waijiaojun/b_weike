-- auto Generated on 2017-09-22 15:38:44 
-- DROP TABLE IF EXISTS `member_sales_promotion_settings`; 
CREATE TABLE `member_sales_promotion_settings`(
    `id` INT (11) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `sales_promotion_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'salesPromotionId',
    `closed` INT (11) NOT NULL DEFAULT 1 COMMENT 'closed',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`member_sales_promotion_settings`';
