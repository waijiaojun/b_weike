-- auto Generated on 2017-09-06 19:51:17 
-- DROP TABLE IF EXISTS `class_room`; 
CREATE TABLE `class_room`(
    `room_id` INT (11) NOT NULL AUTO_INCREMENT COMMENT 'room_id',
    `reserve_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'reserve_id',
    `stt_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'stt_id',
    `tea_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'tea_id',
    `memb_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'memb_id',
    `cou_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'cou_id',
    `set_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'set_id',
    `room_state` INT (11) NOT NULL DEFAULT -1 COMMENT 'room_state',
    `room_create_date` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'room_create_date',
    `room_open_date` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'room_open_date',
    `room_close_date` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'room_close_date',
    PRIMARY KEY (`room_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`class_room`';
