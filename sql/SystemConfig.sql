-- auto Generated on 2017-09-27 15:00:09 
-- DROP TABLE IF EXISTS `system_config`; 
CREATE TABLE `system_config`(
    `id` INT (11) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `category` VARCHAR (255) NOT NULL DEFAULT '' COMMENT 'category',
    `name` VARCHAR (255) NOT NULL DEFAULT '' COMMENT 'name',
    `code` VARCHAR (255) NOT NULL DEFAULT '' COMMENT 'code',
    `value` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'value',
    `desc` VARCHAR (255) NOT NULL DEFAULT '' COMMENT 'desc',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`system_config`';
