-- auto Generated on 2017-08-28 17:24:47 
-- DROP TABLE IF EXISTS `slive_series_live_course`; 
CREATE TABLE `slive_series_live_course`(
    `id` BIGINT (15) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `series_live_id` BIGINT (15) NOT NULL DEFAULT -1 COMMENT 'series_live_id',
    `title_en` VARCHAR (512) NOT NULL DEFAULT '' COMMENT 'title_en',
    `title_cn` VARCHAR (512) NOT NULL DEFAULT '' COMMENT 'title_cn',
    `duration` INT (11) NOT NULL DEFAULT -1 COMMENT 'duration',
    `tea_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'tea_id',
    `tea_earn` DOUBLE (16,4) NOT NULL DEFAULT -1.0 COMMENT 'tea_earn',
    `create_date` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'create_date',
    `update_date` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'update_date',
    `deleted` INT (11) NOT NULL DEFAULT -1 COMMENT 'deleted',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`slive_series_live_course`';
