-- auto Generated on 2017-06-10 17:44:34 
-- DROP TABLE IF EXISTS `td_nt_member_content_recomend`;
CREATE TABLE `td_nt_member_content_recommend`(
    `id` INT (11) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `memb_id` INT (11) NOT NULL COMMENT 'memb_id',
    `content_id` INT (11) NOT NULL COMMENT 'content_id',
    `content_type` VARCHAR (50) NOT NULL COMMENT 'content_type',
    `send_count` INT (11) NOT NULL COMMENT 'send_count',
    `click_count` INT (11) NOT NULL COMMENT 'click_count',
    `create_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create_date',
    `update_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'update_date',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`td_nt_member_content_recommend`';
