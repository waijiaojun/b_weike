-- auto Generated on 2017-09-21 15:13:12 
-- DROP TABLE IF EXISTS `payment_student_order`; 
CREATE TABLE `payment_student_order`(
    `id` INT (11) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `ping_id` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'ping_id',
    `ping_object` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'ping_object',
    `ping_created` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'ping_created',
    `ping_livemode` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'ping_livemode',
    `ping_paid` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'ping_paid',
    `ping_refunded` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'ping_refunded',
    `ping_app` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'ping_app',
    `ping_channel` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'ping_channel',
    `ping_order_no` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'ping_orderNo',
    `ping_client_ip` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'ping_clientIp',
    `ping_amount` INT (11) NOT NULL DEFAULT -1 COMMENT 'ping_amount',
    `ping_amount_settle` INT (11) NOT NULL DEFAULT -1 COMMENT 'ping_amountSettle',
    `ping_currency` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'ping_currency',
    `ping_subject` VARCHAR (128) NOT NULL DEFAULT '' COMMENT 'ping_subject',
    `ping_body` VARCHAR (256) NOT NULL DEFAULT '' COMMENT 'ping_body',
    `ping_time_paid` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'ping_timePaid',
    `ping_time_expire` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'ping_timeExpire',
    `ping_time_settle` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'ping_timeSettle',
    `ping_transaction_no` VARCHAR (128) NOT NULL DEFAULT '' COMMENT 'ping_transactionNo',
    `detail` text (128) NOT NULL DEFAULT '' COMMENT 'detail',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`payment_student_order`';
