-- auto Generated on 2017-07-25 13:35:46 
-- DROP TABLE IF EXISTS `wk_wieke_subscription`; 
CREATE TABLE `wk_wieke_subscription`(
    `id` BIGINT (15) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `memb_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'memb_id',
    `subscription_start_time` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'subscription_start_time',
    `subscription_end_time` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'subscription_end_time',
    `pay_status` INT (11) NOT NULL DEFAULT 1 COMMENT 'pay_status',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`wk_wieke_subscription`';
