-- auto Generated on 2017-08-28 17:17:05 
-- DROP TABLE IF EXISTS `slive_series_live`; 
CREATE TABLE `slive_series_live`(
    `id` BIGINT (15) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `title_en` VARCHAR (512) NOT NULL DEFAULT '' COMMENT 'title_en',
    `title_cn` VARCHAR (512) NOT NULL DEFAULT '' COMMENT 'title_cn',
    `intro_en` VARCHAR (512) NOT NULL DEFAULT '' COMMENT 'intro_en',
    `intro_cn` VARCHAR (512) NOT NULL DEFAULT '' COMMENT 'intro_cn',
    `cover` VARCHAR (512) NOT NULL DEFAULT '' COMMENT 'cover',
    `tea_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'tea_id',
    `create_date` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'create_date',
    `update_date` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'update_date',
    `deleted` INT (11) NOT NULL DEFAULT -1 COMMENT 'deleted',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`slive_series_live`';
