-- auto Generated on 2017-08-28 19:29:16 
-- DROP TABLE IF EXISTS `slive_member_buy_series_live`; 
CREATE TABLE `slive_member_buy_series_live`(
    `id` BIGINT (15) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `series_live_sale_id` BIGINT (15) NOT NULL DEFAULT -1 COMMENT 'series_live_sale_id',
    `series_live_instance_id` BIGINT (15) NOT NULL DEFAULT -1 COMMENT 'series_live_instance_id',
    `pay_status` INT (11) NOT NULL DEFAULT -1 COMMENT 'pay_status',
    `title_en` VARCHAR (512) NOT NULL DEFAULT '' COMMENT 'title_en',
    `title_cn` VARCHAR (512) NOT NULL DEFAULT '' COMMENT 'title_cn',
    `intro_en` VARCHAR (512) NOT NULL DEFAULT '' COMMENT 'intro_en',
    `intro_cn` VARCHAR (512) NOT NULL DEFAULT '' COMMENT 'intro_cn',
    `cover` VARCHAR (512) NOT NULL DEFAULT '' COMMENT 'cover',
    `tea_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'tea_id',
    `create_date` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'create_date',
    `update_date` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'update_date',
    `deleted` INT (11) NOT NULL DEFAULT -1 COMMENT 'deleted',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`slive_member_buy_series_live`';
