-- auto Generated on 2017-09-22 17:15:00 
-- DROP TABLE IF EXISTS `weike_subscription_desc`; 
CREATE TABLE `wk_weike_subscription_desc`(
    `id` INT (11) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `detail` TEXT NOT NULL COMMENT 'detail',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`weike_subscription_desc`';
