-- auto Generated on 2017-04-15 14:08:56 
-- DROP TABLE IF EXISTS `wk_weike_student_buy`; 
CREATE TABLE `wk_weike_student_buy`(
    `id` BIGINT (15) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `memb_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'memb_id',
    `weike_id` BIGINT (15) NOT NULL DEFAULT -1 COMMENT 'weike_id',
    `study_number` BIGINT (15) NOT NULL DEFAULT -1 COMMENT 'study_number',
    `payment_status` INT (11) NOT NULL DEFAULT -1 COMMENT 'payment_status',
    `create_user` INT (11) NOT NULL DEFAULT -1 COMMENT 'create_user',
    `create_date`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create_date' ,
    `update_user` INT (11) NOT NULL DEFAULT -1 COMMENT 'update_user',
    `update_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'update_date',
    `deleted` INT (11) NOT NULL DEFAULT -1 COMMENT 'deleted',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`wk_weike_student_buy`';
