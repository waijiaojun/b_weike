-- auto Generated on 2017-04-15 14:04:58 
-- DROP TABLE IF EXISTS `wk_weike_label`; 
CREATE TABLE `wk_weike_label`(
    `id` BIGINT (15) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `weike_id` BIGINT (15) NOT NULL DEFAULT -1 COMMENT 'weike_id',
    `label_zh_name` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'label_zh_name',
    `label_en_name` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'label_en_name',
    `label_type` INT (11) NOT NULL DEFAULT -1 COMMENT 'label_type',
    `create_user` INT (11) NOT NULL DEFAULT -1 COMMENT 'create_user',
    `create_date`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create_date' ,
    `update_user` INT (11) NOT NULL DEFAULT -1 COMMENT 'update_user',
    `update_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'update_date',
    `deleted` INT (11) NOT NULL DEFAULT -1 COMMENT 'deleted',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`wk_weike_label`';
