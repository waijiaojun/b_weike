-- auto Generated on 2017-08-28 18:51:15 
-- DROP TABLE IF EXISTS `slive_series_live_course_picture`; 
CREATE TABLE `slive_series_live_course_picture`(
    `id` BIGINT (15) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `series_live_course_id` BIGINT (15) NOT NULL DEFAULT -1 COMMENT 'series_live_course_id',
    `url` VARCHAR (512) NOT NULL DEFAULT '' COMMENT 'url',
    `sort` INT (11) NOT NULL DEFAULT -1 COMMENT 'sort',
    `deleted` INT (11) NOT NULL DEFAULT -1 COMMENT 'deleted',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`slive_series_live_course_picture`';
