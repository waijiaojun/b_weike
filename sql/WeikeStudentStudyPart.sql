-- auto Generated on 2017-09-22 17:27:34 
-- DROP TABLE IF EXISTS `wk_weike_student_study_part`; 
CREATE TABLE `wk_weike_student_study_part`(
    `id` BIGINT (15) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `memb_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'memb_id',
    `weike_id` BIGINT (15) NOT NULL DEFAULT -1 COMMENT 'weike_id',
    `weike_part_id` BIGINT (15) NOT NULL DEFAULT -1 COMMENT 'weike_part_id',
    `play_number` INT (11) NOT NULL DEFAULT 0 COMMENT 'play_number',
    `first_play_date` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'first_play_date',
    `last_play_date` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'last_play_date',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`wk_weike_student_study_part`';
